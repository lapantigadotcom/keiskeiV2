<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TypeAttribute;
use App\DetailAttribute;

class Attribute extends Model {

	protected $table = 'ms_attributes';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function typeAttribute()
    {
    	return $this->belongsTo('App\TypeAttribute', 'ms_type_attribute_id', 'id');
    }

    public function detailAttributes()
    {
    	return $this->hasMany('App\DetailAttribute', 'ms_attribute_id', 'id');
    }

}
