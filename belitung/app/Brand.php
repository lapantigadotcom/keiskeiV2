<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Brand extends Model {

	protected $table = 'ms_brands';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('App\Product', 'ms_brand_id', 'id');
    }

}
