<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class CategoryProduct extends Model {

	protected $table = 'ms_category_products';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('App\Product', 'ms_category_product_id', 'id');
    }

    public function parent()
    {
    	return $this->belongsTo('App\CategoryProduct','parent_id');
    }
    public function subcategory()
    {
    	return $this->hasMany('App\CategoryProduct','parent_id');
    }
}
