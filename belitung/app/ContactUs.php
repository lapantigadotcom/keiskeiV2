<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model {

	protected $table = 'tr_contactus';
	protected $guarded = ['id'];
	protected $fillable = [
		'name',
		'email',
		'subject',
		'message'
	];

}
