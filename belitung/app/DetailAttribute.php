<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attribute;
use App\DetailAttributeProduct;

class DetailAttribute extends Model {

	protected $table = 'ms_detail_attributes';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function attribute()
    {
    	return $this->belongsTo('App\Attribute', 'ms_attribute_id', 'id');
    }

    public function detailAttributeProducts()
    {
    	return $this->hasMany('App\DetailAttributeProduct', 'ms_detail_attribute_id', 'id');
    }

}
