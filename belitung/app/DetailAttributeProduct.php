<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailAttribute;
use App\Product;
use App\DetailAttributeProductDetailOrder;

class DetailAttributeProduct extends Model {

	protected $table = 'ms_detail_attribute_product';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function detailAttribute()
    {
    	return $this->belongsTo('\App\DetailAttribute', 'ms_detail_attribute_id', 'id');
    }

    public function product()
    {
    	return $this->belongsTo('\App\Product', 'ms_product_id', 'id');
    }

    public function detailAttributeProductDetailOrders()
    {
        return $this->hasMany('\App\DetailAttributeProductDetailOrder', 'ms_detail_attribute_product_id', 'id');
    }

}
