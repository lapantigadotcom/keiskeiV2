<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailAttributeProductDetailOrder;
use App\Order;
use App\Product;

class DetailOrder extends Model {

	protected $table = 'tr_detail_orders';
	protected $guarded = ['id'];

	public function detailAttributeProductDetailOrders()
	{
		return $this->hasMany('\App\DetailAttributeProductDetailOrder', 'tr_detail_order_id', 'id');
	}

	public function order()
	{
		return $this->belongsTo('\App\Order', 'tr_order_id', 'id');
	}

	public function product()
	{
		return $this->belongsTo('\App\Product', 'ms_product_id', 'id');
	}

}
