<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\User;
use DB;
use Carbon\Carbon;
use App\Order;
use App\DetailOrder;
use App\Withdraw;
use App\Transfer;
use App\DetailAttributeProductDetailOrder;
use App\LogActivity;
use App\NotifSMS;
use App\NotifEmail;
use App\NotifMessage;
use App\RegistrationFee;
use Session;
use Input;
use Hash;
  

class AdminController extends BasicController {


 


	public function getLogin()
	{
		return view('pages.login.login');
	}
	public function postLogin(Request $request)
	{

		$username = $request->input('username');
		$password = $request->input('password');
		$remember_me=false;
		$tmp = $request->input('remember_me');
		if(!empty($tmp))
			$remember_me=true;
		if (Auth::attempt(['username' => $username, 'password' => $password],$remember_me))
        {
        	if(Auth::user()->roles->first()->locked != '1')
        	{
        		Auth::logout();
        		return redirect()->route('home');
        	}
            return redirect()->intended('admin/dashboard');
        }else
        {
        	return redirect()->back();
        }
	}

	public function getDashboard()
	{
		$data['sale_total'] = DetailOrder::with('order')->sum('total');



		$data['sale_today'] = DetailOrder::whereHas('order', function($q){
			$q->where(DB::raw('DAY(created_at)'), '=', date('d'))
			->where(DB::raw('MONTH(created_at)'), '=', date('m'))
			->where(DB::raw('YEAR(created_at)'), '=', date('Y'));
		})->sum('total');

		$data['sale_week'] = DetailOrder::whereHas('order', function($q){
			$q->whereBetween('created_at', array(Carbon::now()->subWeek(), Carbon::now()));
		})->sum('total');

		$data['sale_month'] = DetailOrder::whereHas('order', function($q){
			$q->where(DB::raw('MONTH(created_at)'), '=', date('m'))
			->where(DB::raw('YEAR(created_at)'), '=', date('Y'));
		})->sum('total');

		$data['member_active'] = User::whereHas('roles', function($q) {
			$q->where('enabled', '=', '1');
		})->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['member_today'] = User::where(DB::raw('DAY(created_at)'), '=', date('d'))
		->where(DB::raw('MONTH(created_at)'), '=', date('m'))
		->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['member_week'] = User::whereBetween('created_at', array(Carbon::now()->subWeek(), Carbon::now()))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$data['member_month'] = User::where(DB::raw('MONTH(created_at)'), '=', date('m'))
		->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
		->whereHas('roles', function($q){
		    $q->where('locked', '<>', '1');
		})->count();

		$total_referral_value = Withdraw::sum('referral_value');
		$total_monthly_value = Withdraw::sum('monthly_value');
		$data['total_bonus'] = $total_referral_value + $total_monthly_value;
		
		$total_bonus_dibayarkan_referral = Withdraw::where('status', '=', '1')->sum('referral_value');
		$total_bonus_dibayarkan_monthly = Withdraw::where('status', '=', '1')->sum('monthly_value');
		$data['total_bonus_dibayarkan'] = $total_bonus_dibayarkan_referral + $total_bonus_dibayarkan_monthly;
		
		$total_bonus_kredit_referral = Withdraw::where('status', '<>', '1')->sum('referral_value');
		$total_bonus_kredit_monthly = Withdraw::where('status', '<>', '1')->sum('monthly_value');
		$data['total_bonus_kredit'] = $total_bonus_kredit_referral + $total_bonus_kredit_monthly;
		
		$user = Withdraw::whereHas('user.roleUsers.role', function($q){
			$q->where('locked', '<>', '1');
		})->orderBy(DB::raw('sum(referral_value+monthly_value)'), 'desc')->first()->user;
		
		$data['top_member_bonus'] = isset($user->username) ? $user->username : 'Kosong';

		$data['top_user_transaction'] = DB::select(DB::raw("SELECT sum(shipping_price + total_harga) as total_biaya, h.user_name, h.role_name, h.tanggal_aktif, h.user_id
FROM(
	SELECT ms_user_id, order_id, user_id, urp.tanggal_aktif, role_id, role_name, user_name, shipping_price, sum(total_product * product_price) as total_harga
	FROM(
		SELECT o.ms_user_id, o.id as order_id, do.id as detail_order_id, do.ms_product_id, do.total as total_product, o.shipping_price
		FROM tr_orders as o
		JOIN tr_detail_orders as do ON o.id = do.tr_order_id
	) as odo
	JOIN(
		SELECT ur.user_name, ur.user_id, ur.role_id, ur.role_name, ur.tanggal_aktif, pp.product_name, pp.product_price, pp.product_id
		FROM (
			SELECT u.id as user_id, u.username as user_name, u.created_at as tanggal_aktif,ru.role_id, ru.role_name
			FROM ms_users as u
			JOIN (
				SELECT ms_user_id, max(ms_role_id) as role_id, ms_roles.name as role_name 
				FROM ms_role_user, ms_roles
				WHERE ms_roles.id = ms_role_user.ms_role_id
				GROUP BY ms_user_id
			) as ru
			ON u.id = ru.ms_user_id
		) as ur
		JOIN (
			SELECT	p.id as product_id, p.title as product_name, pr.price as product_price, pr.ms_role_id, c.name as currecy_name
			FROM ms_products as p
			JOIN ms_prices as pr ON p.id = pr.ms_product_id
			JOIN ms_currencies as c ON c.id = pr.ms_currency_id
			WHERE c.id = 1
		) as pp
		on ur.role_id = pp.ms_role_id
	) urp
	ON (odo.ms_user_id = urp.user_id AND odo.ms_product_id = urp.product_id)
	GROUP BY order_id
) as h
GROUP BY user_id
ORDER BY total_biaya DESC
LIMIT 5"));
		
		return view('pages.dashboard.index', compact('data'));
	}

	public function getLogout()
	{
		Auth::logout();
		return redirect()->route('ki-admin.auth');
	}
	
	public function getChangePassword()
	{
		return view('pages.admin.changepassword');
	}
	
	public function postChangePassword(Request $request)
	{
		$user = Auth::user();
		$oldPassword = Input::get('old_password');
		$newPassword = Input::get('new_password');
		$retypePassword = Input::get('retype_password');
		if($newPassword != $retypePassword) {
			Session::flash('errorMessage', 'Re-type Password tidak sama');
			return redirect()->route('ki-admin.getChangePassword');
		}
		if(Hash::check($newPassword, $user->password)) {
			Session::flash('errorMessage', 'Password salah');
			return redirect()->route('ki-admin.getChangePassword');
		}
		$user->password = Hash::make($newPassword);
		$user->save();
		Session::flash('successMessage', 'Ganti password berhasil');
		return redirect()->route('ki-admin.getChangePassword');
	}
	
	public function getTruncateDatabase()
	{
		return view('pages.admin.truncatedatabase');
	}
	
	public function postTruncateDatabase(Request $request)
	{
		$order = $request->input('order');
		if( $order == '1') {
			Order::truncate();
			DetailOrder::truncate();
			DetailAttributeProductDetailOrder::truncate();
			Transfer::truncate();
		}
		$detail_order = $request->input('detail_order');
		if( $detail_order == '1') {
			DetailOrder::truncate();
			DetailAttributeProductDetailOrder::truncate();
			Transfer::truncate();
		}
		$detail_attribute_product_order = $request->input('detail_attribute_product_order');
		if($detail_attribute_product_order == '1') {
			DetailAttributeProductDetailOrder::truncate();
		}
		$withdraw = $request->input('withdraw');
		if($withdraw == '1') {
			Withdraw::truncate();
		}
		$transfer = $request->input('transfer');
		if( $transfer == '1') {
			Transfer::truncate();
		}
		$registration_fee = $request->input('registration_fee');
		if($registration_fee == '1') {
			RegistrationFee::truncate();
		}
		$log_activity = $request->input('log_activity');
		if( $request->input('log_activity') == '1') {
			LogActivity::truncate();
		}
		$notification_email = $request->input('notification_email');
		if( $notification_email == '1') {
			NotifEmail::truncate();
		}
		$notification_sms = $request->input('notification_sms');
		if($notification_sms == '1') {
			NotifSMS::truncate();
		}
		$notification_message = $request->input('notification_message');
		if($notification_message == '1') {
			NotifMessage::truncate();
		}
		Session::flash('success', true);
		return redirect()->back();
	}

	public function refreshBonus(){
		$user = \App\User::whereHas('roles', function($q){
			$q->where('require_file','1');
		})->get();
		foreach ($user as $row) {
			$this->refreshWithdraw($row->id);
		}
		return redirect()->route('ki-admin.dashboard');
	}
}
