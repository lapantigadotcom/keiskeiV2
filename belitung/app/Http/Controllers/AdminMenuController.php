<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\AdminMenuRequest;
use Illuminate\Http\Request;

class AdminMenuController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = \App\Adminmenu::with('roles','child')->get();
		return view('pages.adminmenu.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['adminmenu'] = \App\Adminmenu::where('parent_id','0')->lists('name','id');
		return view('pages.adminmenu.form',compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(AdminMenuRequest $request)
	{
		$data = \App\Adminmenu::create($request->all());
		$urutan = \App\Adminmenu::where('parent_id', $request->input('parent_id'))->count();
		$data->order_item = $urutan+1;
		$data->save();
		return redirect()->route('ki-admin.adminmenu.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['adminmenu'] = \App\Adminmenu::where('parent_id','0')->lists('name','id');
		$data['content'] = \App\Adminmenu::find($id);
		return view('pages.adminmenu.form',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(AdminMenuRequest $request, $id)
	{
		$data = \App\Adminmenu::find($id);
		$data->update($request->all());
		$urutan = \App\Adminmenu::where('parent_id', $request->input('parent_id'))->count();
		$data->order_item = $urutan+1;
		$data->save();
		return redirect()->route('ki-admin.adminmenu.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data=\App\Adminmenu::find($id);
		$data->roles()->detach();
		$data->delete();
		return redirect()->route('ki-admin.adminmenu.index');
	}


}
