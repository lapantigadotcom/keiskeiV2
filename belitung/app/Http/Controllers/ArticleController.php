<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Article;
use App\CategoryArticle;
use App\Http\Requests\ArticleRequest;
use Auth;

class ArticleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Article::with('categoryArticle')->where('ms_user_id', '=', Auth::id())->get();
		return view('pages.article.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['categoryarticle'] = CategoryArticle::all();
		return view('pages.article.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ArticleRequest $request)
	{
		$article = Article::create($request->all());
		$article->ms_user_id = Auth::id();
		$article->save();
		return redirect()->route('ki-admin.article.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['categoryarticle'] = CategoryArticle::all();
		$data['content'] = Article::find($id);
		return view('pages.article.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ArticleRequest $request, $id)
	{
		$article = Article::find($id);
		$article->update($request->all());
		return redirect()->route('ki-admin.article.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Article::destroy($id);
		return redirect()->route('ki-admin.article.index');
	}

}
