<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Theme;
use App\General;
use Illuminate\Http\Request;
use Session;
use Mail;
use URL;
use DB;

class BasicController extends Controller {
	protected $data;
	protected $theme;
	protected $general;
	protected $origin;
	protected $api_shipping;
	public function __construct() {
		$this->theme = Theme::where('active','1')->first();
		$this->general = General::first();
		$this->data['theme'] = $this->theme;
		$this->data['general'] = $this->general;
		$this->origin = '444';
		$this->api_shipping = 'b1f9f32ff1963055f55bc1e96493c175';
	}

	protected function sendEmail($type = '1', $to, $m = '')
	{
		$data['to'] = $to;
		switch ($type) {
			// User Registrasi
			case '1':

				$data['url'] = URL::to('/login');
				$data['username'] = $m->username;
				$data['password'] = $m['password'];
				Mail::send('theme.'.$this->theme->code.'.include.register',$data, function($message) use($data)
				{
					$message->subject('Aktivasi Registrasi KeisKei');
					$message->to($data['to']);
				});
			break;
			// Reset Password by Admin
			case '2':
				$data['url'] = URL::to('/login');
				$data['username'] = $m->username;
				$data['password'] = $m['password'];
				Mail::send('theme.'.$this->theme->code.'.include.reset',$data, function($message) use($data)
				{
					$message->subject('Reset Password Akun KeisKei');
					$message->to($data['to']);
				});
			break;
			// User terverifikasi
			case '3':
				$data['name'] = $m->name;
				$data['username'] = $m->username;
				$data['roles'] = $m->roles->first()->name;
				$data['url'] = URL::to('/login');
				Mail::send('theme.'.$this->theme->code.'.include.verified',$data, function($message) use($data)
				{
					$message->subject('Verifikasi Akun KeisKei Berhasil');
					$message->to($data['to']);
				});
				break;
			// Contackt us masuk
			case '4':
				$data['name'] = $m->name;
				$data['email'] = $m->email;
				$data['message'] = $m->message;
				$data['subject'] = $m->subject;
				Mail::send('theme.'.$this->theme->code.'.include.contact-us',$data, function($message) use($data)
				{
					$message->subject('Contact KeisKei : '.$data['subject']);
					$message->to($data['to']);
				});
				break;
			// Ada pemesanan oleh end user ke perwakilan
			case '5':
				$data['content'] = $m;
				$data['subject'] = "KeisKei Indonesia - Pemesanan Produk";
				Mail::send('theme.'.$this->theme->code.'.include.order-reseller',$data, function($message) use($data)
				{
					$message->subject('Office KeisKei : '.$data['subject']);
					$message->to($data['to']);
				});
				break;
			// Penyetujuan invoice
			case '6':
				$data['content'] = "Administrator telah menyetujui konfirmasi pembayaran anda untuk invoice ".$m;
				$data['subject'] = "KeisKei Indonesia - Invoice Berhasil";
				Mail::send('theme.'.$this->theme->code.'.include.blank-email',$data, function($message) use($data)
				{
					$message->subject('Order Status: '.$data['subject']);
					$message->to($data['to']);
				});
				break;
			case '7':
				$data['content'] = "Checkout invoice ".$m." telah dilakukan. Silakan melakukan pembayaran.";
				$data['subject'] = "KeisKei Indonesia - Invoice NO:".$m;
				Mail::send('theme.'.$this->theme->code.'.include.blank-email',$data, function($message) use($data)
				{
					$message->subject('Order Status : '.$data['subject']);
					$message->to($data['to']);
				});
				break;
			default:
				# code...
				break;
		}
	}
	protected function log($data = null){
		$log = new Log;
		$log->ms_user_id = Auth::id();
		$log->name = $data['name'];
		$log->date = date('Y-m-d H:i:s');
		$data = Log::all();
		if($data->count() > 49)
		{
			$lebih = $data->count()-49;
			$hapus = Log::orderBy('id','desc')->get()->take($lebih);
			foreach ($hapus as $row) {
				$row->delete();
			}

		}
		return $log;
	}

	function refreshWithdraw($id)
	{
		$user_t = \App\User::find($id);
		$month = date('m',strtotime($user_t->created_at));
		$year = date('Y',strtotime($user_t->created_at));
		$current_year = date('Y');
		$current_month = date('m');
		$loop_year = $current_year-$year;
		$temp_year = $year;

		while ($temp_year<= $current_year) {
			for ($i=1; $i < 12 ; $i++) { 
				if($temp_year < $current_year or ($temp_year = $current_year and $i<= $current_month))
				{
					
					$this->calculalteMonthPoint($user_t->id,$temp_year,$i);
					$this->calculateReferallPoint($user_t->id,$temp_year,$i);
				}
			}
			$temp_year++;
		}
		return;
	}
	function calculalteMonthPoint($user_id, $year, $month)
	{
		$temp = \App\Withdraw::where('ms_user_id',$user_id)->where('year',$year)->where('month', $month)->first();
		if(empty($temp))
		{
			$monthly_value = $this->calculateMonthlyValue($user_id, $year, $month);
			if(intval($monthly_value) >= 1)
			{

				$user = \App\User::with('roles','roles.point')->find($user_id);
				$point = intval($user->roles->first()->point->point);
				$result = intval($monthly_value) * intval($point)/100;
				\App\Withdraw::create([
					'ms_user_id' => $user_id,
					'month' => $month,
					'year' => $year,
					'monthly_value' => $result
				]);
			}
		}
		else if($temp->status!='1')
		{
			$monthly_value = $this->calculateMonthlyValue($user_id, $year, $month);
			if(intval($monthly_value) >= 1)
			{
				$user = \App\User::with('roles','roles.point')->find($user_id);
				$point = intval($user->roles->first()->point->point);
				$result = intval($monthly_value) * intval($point)/100;
				$temp->monthly_value = $result;
				$temp->save();
			}else{
				$temp->monthly_value = 0;
				$temp->save();
			}
			
		}
		return;
	}
	function calculateReferallPoint($user_id, $year, $month)
	{
		$temp = \App\Withdraw::where('ms_user_id',$user_id)->where('year',$year)->where('month', $month)->first();
		if(empty($temp))
		{
			$monthly_value = $this->calculateReferralValue($user_id, $year, $month);
			if(intval($monthly_value) > 0)
			{
				\App\Withdraw::create([
					'ms_user_id' => $user_id,
					'month' => $month,
					'year' => $year,
					'referral_value' => $monthly_value
				]);
			}
		}
		else if($temp->status!='1')
		{
			$monthly_value = $this->calculateReferralValue($user_id, $year, $month);
			$temp->referral_value = $monthly_value;
			$temp->save();
		}
		return;
	}
	
	function calculateReferralValue($user_id, $year, $month)
	{
		$user_referraled = \App\User::with('roles')->where('referraled_by',$user_id)->where('approved','1')->where(DB::raw('MONTH(created_at)'),$month)->where(DB::raw('YEAR(created_at)'), $year)->get();
		$user = \App\User::with('roles','roles.referralpoint')->find($user_id);
		$referraled_point = \App\ReferralPoint::where('ms_role_id',$user->roles->first()->id)->get();
		$result = 0;
		foreach ($user_referraled as $row) {
			foreach ($referraled_point as $val) {
				if($val->target_ms_role_id == $row->roles->first()->id)
				{
					$result += intval($val->value);
				}
			}
		}
		return $result;
	}

	function calculateMonthlyValue($user_id, $year, $month)
	{
		$user = \App\User::find($user_id);
		
		$order = \App\Order::with('detailOrders','detailOrders.product','detailOrders.product.prices')->where('ms_user_id', $user_id)->where('ms_status_order_id','4')->where(DB::raw('MONTH(created_at)'),$month)->where(DB::raw('YEAR(created_at)'), $year)->get();
		$total = 0;
		foreach ($order as $row) {
			$price = 0;
			foreach ($row->detailOrders as $val) {
				$price_t = \App\Price::where('ms_product_id',$val->ms_product_id)->where('ms_role_id',$user->roles->first()->id)->where('ms_currency_id','1')->first()->price;
				
				if(!empty($val->product->discount))
				{
					$discount = $price_t*$val->product->discount/100;
					$price_t = $price_t-$discount;
				}
				$price += $price_t*$val->total;
			}
			// $price += intval($row->shipping_price);
			$total += $price;
		}
		return $total;
	}
	protected function sendNotif($data)
	{
		$url = "http://www.freesms4us.com/kirimsms.php?";
		$userpass = "user=keiskei&pass=ZXCasd123&";
		$content="isi=".$data['content'];
		$no="no=".$data['receiver']."&";
		$smsUrl = $url.$userpass.$no.$content;
		$smsUrl = str_replace(" ", "%20", $smsUrl);
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $smsUrl);
		curl_setopt($ch, CURLOPT_HEADER, false);

		// grab URL and pass it to the browser
		$response = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		$firstWord = explode(" ", $response)[0];
		if($firstWord == "MAAF") {
			return false;
		}
		else {
			return true;
		}
	}
	protected function sendSMS($type = '1', $to, $m = '')
	{
		$content = '';
		switch ($type) {
			// User Registrasi
			case '1':
				$content="isi=[AKTIVASI KEISKEI] Silakan login di http://keiskei.co.id/login ~ username : ".$m->username." ~ password : ".$m['password'];
				break;
		}
		$url = "http://www.freesms4us.com/kirimsms.php?";
		$userpass = "user=keiskei&pass=ZXCasd123&";
		$no="no=".$to."&";
		$smsUrl = $url.$userpass.$no.$content;
		$smsUrl = str_replace(" ", "%20", $smsUrl);
		$ch = curl_init();

		// set URL and other appropriate options
		curl_setopt($ch, CURLOPT_URL, $smsUrl);
		curl_setopt($ch, CURLOPT_HEADER, false);

		// grab URL and pass it to the browser
		$response = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		$firstWord = explode(" ", $response)[0];
		if($firstWord == "MAAF") {
			return false;
		}
		else {
			return true;
		}
	}
}
