<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Brand;
use App\Http\Requests\BrandRequest;
use Session;

class BrandController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Brand::all();
		return view('pages.brand.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pages.brand.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(BrandRequest $request)
	{
		Brand::create($request->all());
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('ki-admin.brand.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Brand::find($id);
		return view('pages.brand.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(BrandRequest $request, $id)
	{
		$data = Brand::find($id);
		$data->update($request->all());
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('ki-admin.brand.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data_t = Brand::with(
			'products',
			'products.detailAttributeProducts',
			'products.prices',
			'products.detailOrders',
			'products.detailOrders.detailAttributeProductDetailOrders',
			'products.reviews',
			'products.stockProducts'
		)->find($id);
		foreach ($data_t->products as $data) {
			foreach ($data->detailAttributeProducts as $row) {
				$row->delete();
			}
			foreach ($data->prices as $row) {
				$row->delete();
			}
			foreach ($data->reviews as $row) {
				$row->delete();
			}
			foreach ($data->stockProducts as $row) {
				$row->delete();
			}
			foreach ($data->detailOrders as $row) {
				foreach ($row->detailAttributeProductDetailOrders as $val) {
					$val->delete();
				}
				$row->delete();
			}
			foreach ($data->productImages as $row) {
				if(File::exists($row->image))
					File::delete($row->image);
				$row->delete();
			}
		}
		$data_t->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.brand.index');
	}

}
