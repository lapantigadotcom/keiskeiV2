<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ThirdParty\Ongkir;

use Illuminate\Http\Request;
use Auth;
use Input;
use Session;

class CartController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function indexUser()
	{
		$data['content'] = \App\Order::where('ms_user_id',Auth::id())
			->where('ms_status_order_id','1')
			->first();
		
		$data['payment'] = \App\Payment::all();
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['courier'] = \App\Courier::lists('name','id');
		if(Auth::user()->roles->first()->default == '1')
			$data['agen'] = \App\User::whereHas('roles',function($q)
				{
					$q->where('require_file','1');
				})
				->where('ms_city_id',Auth::user()->ms_city_id)
				->where('enabled','1')
				->where('approved','1')
				->get();
		else
			$data['agen'] = null;
		if(!empty($data['content']) and Auth::user()->roles->first()->level!='5')
		{
			if($data['content']->detailOrders->count()  > 0)
			{
				if(empty($data['content']->ms_courier_id))
				{
					$this->calculateShipping(Auth::id(), $data['content']->id);
				}else{
					$this->calculateShipping(Auth::id(), $data['content']->id,$data['content']->ms_courier_id, $data['content']->service_courier);
				}
			}
		}
		$data['content'] = \App\Order::with(
			'detailOrders',
			'detailOrders.product',
			'detailOrders.product.prices',
			'detailOrders.product.productImages',
			'detailOrders.detailAttributeProductDetailOrders',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct.detailAttribute',
			'detailOrders.detailAttributeProductDetailOrders.detailAttributeProduct.detailAttribute.attribute',
			'statusOrder',
			'courier')
			->where('ms_user_id',Auth::id())
			->where('ms_status_order_id','1')
			->first();
		
		return view('theme.'.$this->theme->code.'.pages.user.cart',compact('data'));
	}
	function calculateShipping($user_id, $order_id, $courier_id=null, $service_courier = null){
		$ongkir = new Ongkir($this->api_shipping);
		$user = \App\User::find($user_id);
		$order = \App\Order::with('detailOrders','detailOrders.product')->find($order_id);
		$weight = 0;
		foreach ($order->detailOrders as $row) {
			$weight += $row->total * $row->product->weight;
		}
		$courier = null;
		if(empty($courier_id))
		{
			$courier = \App\Courier::where('code','jne')->first();
			$cost = $ongkir->getCost(intval($this->origin),$user->ms_city_id,intval($weight*1000),'jne');
			$min_obj = array();
			if(empty($cost))
			{
				return redirect()->route('user.cart');
			}
			foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
				foreach ($row->{'costs'} as $val) {
					if(count($min_obj) == 0)
					{
						array_push($min_obj, $val->service);
						array_push($min_obj, intval($val->cost[0]->value));
						array_push($min_obj, intval($val->cost[0]->etd));
					}else{
						if($min_obj[1] > intval($val->cost[0]->value) || ($min_obj[1] == intval($val->cost[0]->value) && $min_obj[2] > intval($val->cost[0]->etd)))
						{
							$min_obj[0] = $val->service;
							$min_obj[1] = $val->cost[0]->value;
							$min_obj[2] = $val->cost[0]->etd;
						}
					}
				}
			}
			// Hasil =
			// array:3 [▼
			//   0 => "Pos Kilat Khusus"
			//   1 => 41000
			//   2 => 4
			// ]
			$order->service_courier = $min_obj[0];
			$order->shipping_price = $min_obj[1];
			$order->ms_courier_id = $courier->id;
			$order->save();
			return $min_obj;
		}
		else if(!empty($courier_id) && !empty($service_courier))
		{
			$courier = \App\Courier::find($courier_id);
			$cost = $ongkir->getCost(intval($this->origin),$user->ms_city_id,intval($weight*1000),$courier->code);
			$min_obj = array();
			$tmp_t = array();
			if(!empty($cost)){
			
				foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
					foreach ($row->{'costs'} as $val) {
						if(count($min_obj) == 0)
						{
							array_push($min_obj, $val->service);
							array_push($min_obj, intval($val->cost[0]->value));
							array_push($min_obj, intval($val->cost[0]->etd));
						}else{
							if($min_obj[1] > $val->cost[0]->value || ($min_obj[1] == $val->cost[0]->value && $min_obj[2] > $val->cost[0]->etd))
							{
								$min_obj[0] = $val->service;
								$min_obj[1] = $val->cost[0]->value;
								$min_obj[2] = $val->cost[0]->etd;
							}
						}
						if($val->service == $service_courier)
						{
							array_push($tmp_t, $val->service);
							array_push($tmp_t, intval($val->cost[0]->value));
							array_push($tmp_t, intval($val->cost[0]->etd));
						}
					}
				}
				// Hasil =
				// array:3 [▼
				//   0 => "Pos Kilat Khusus"
				//   1 => 41000
				//   2 => 4
				// ]
				if(count($tmp_t) > 0)
				{	$order->service_courier = $tmp_t[0];
					$order->shipping_price = $tmp_t[1];
					$order->ms_courier_id = $courier->id;
				}elseif(count($min_obj))
				{
					$order->service_courier = $min_obj[0];
					$order->shipping_price = $min_obj[1];
					$order->ms_courier_id = $courier->id;
				}
				$order->save();
				return $min_obj;
			}else{
				$courier = \App\Courier::find($courier_id);
				$cost = $ongkir->getCost(intval($this->origin),$user->ms_city_id,intval($weight*1000),$courier->code);
				$min_obj = array();
				if(!empty($cost)){
					foreach ($cost->{'rajaongkir'}->{'results'} as $row) {
						foreach ($row->{'costs'} as $val) {
							if(count($min_obj) == 0)
							{
								array_push($min_obj, $val->service);
								array_push($min_obj, intval($val->cost[0]->value));
								array_push($min_obj, intval($val->cost[0]->etd));
							}else{
								if($min_obj[1] > $val->cost[0]->value || ($min_obj[1] == $val->cost[0]->value && $min_obj[2] > $val->cost[0]->etd))
								{
									$min_obj[0] = $val->service;
									$min_obj[1] = $val->cost[0]->value;
									$min_obj[2] = $val->cost[0]->etd;
								}
							}
						}
					}
					// Hasil =
					// array:3 [▼
					//   0 => "Pos Kilat Khusus"
					//   1 => 41000
					//   2 => 4
					// ]
					$order->service_courier = $min_obj[0];
					$order->shipping_price = $min_obj[1];
					$order->ms_courier_id = $courier->id;
					$order->save();
					return $min_obj;
				}
			}
			return null;
		}
	}

	public function checkReseller()
	{
		$order_id = Input::get('order_id');
		$ms_reseller_id = Input::get('ms_reseller_id');
		$data = \App\Order::find($order_id);
		$data->ms_reseller_id = $ms_reseller_id;
		$data->save();
		return null;
	}
	public function store(Request $request,$id)
	{
		if(Auth::user()->approved != '1')
		{
			Session::flash('warning','Maaf akun anda belum terverifikasi. Silakan kontak petugas kami, untuk informasi lebih lanjut.');
			return redirect()->route('user.cart');
		}
		$order = \App\Order::where('ms_status_order_id',1)->where('ms_user_id', Auth::id())->first();
		if(empty($order))
		{
			$order = new \App\Order;
			$order->code = 'INV/'.date('Ymd').'/'.Auth::user()->roles->first()->code.Auth::id().'/'.date('Hi');
			$order->ms_user_id = Auth::id();
			$order->ms_status_order_id = 1;
			$order->save();
		}
		$data_t = \App\DetailOrder::create([
			'tr_order_id' => $order->id,
			'ms_product_id' => $id,
			'total' => 1
		]);
		$att = \App\Attribute::all();
		foreach ($att as $row) {
			$input = $request->input('att'.$row->id);
			if(is_array($input))
			{
				foreach ($input as $value) {
					$tmp_t = \App\DetailAttributeProduct::where('ms_product_id',$id)->where('ms_detail_attribute_id',$value)->first();
					if(!empty($tmp_t))
					\App\DetailAttributeProductDetailOrder::create([
						'tr_detail_order_id' => $data_t->id,
						'ms_detail_attribute_product_id' => $tmp_t->id
					]);
				}
			}else{
				$tmp_t = \App\DetailAttributeProduct::where('ms_product_id',$id)->where('ms_detail_attribute_id',$input)->first();
				if(!empty($tmp_t))
				\App\DetailAttributeProductDetailOrder::create([
					'tr_detail_order_id' => $data_t->id,
					'ms_detail_attribute_product_id' => $tmp_t->id
				]);
			}
		}
		return redirect()->route('user.cart');
	}
	public function changeTotal(Request $request, $id)
	{
		$total = $request->input('total');
		$detailOrder = \App\DetailOrder::find($id);
		$detailOrder->total = $total;
		$detailOrder->save();
		return redirect()->to('/user/cart');
	}
	public function deleteDetail($id)
	{
		$detailOrder = \App\DetailOrder::with('detailAttributeProductDetailOrders')->find($id);
		foreach ($detailOrder->detailAttributeProductDetailOrders as $row) {
			$row->delete();
		}
		$detailOrder->delete();
		return redirect()->route('user.cart');
	}
	public function checkoutAgent()
	{
		$data = \App\Order::where('ms_user_id',Auth::id())
			->where('ms_status_order_id','1')
			->first();
		$data->ms_status_order_id = '8';
		$data->save();
		\App\LogActivity::create([
			'ms_user_id' => Auth::id(),
			'description' => 'Checkout Invoice '. $data->code .' telah dilakukan. Silakan tunggu kontak dari Perwakilan atau anda dapat menghubunginya.'
		]);
		$fak = 'User '. Auth::user()->name .' telah melakukan pemesanan Invoice '. $data->code .' pada Anda. Silakan lanjutkan proses pemesanan.';
		\App\LogActivity::create([
			'ms_user_id' => $data->ms_reseller_id,
			'description' => $fak
		]);
		$user = \App\User::find($data->ms_reseller_id);
		if(!empty($user))
		{
			$this->sendEmail('5',$user->email,$fak);
			$sms['content'] = '[KEISKEI.CO.ID]~'.$fak;
			$sms['receiver'] = $user->mobile;
			$this->sendNotif($sms);
		}
		Session::flash('success','Checkout telah berhasil dilakukan. Silakan tunggu Perwakilan menghubungi anda.');
		return redirect()->route('user.report');
	}
	public function checkoutOverseas()
	{
		$data = \App\Order::where('ms_user_id',Auth::id())
			->where('ms_status_order_id','1')
			->first();
		$data->ms_status_order_id = '7';
		$data->save();
		\App\LogActivity::create([
			'ms_user_id' => Auth::id(),
			'description' => 'Checkout Invoice '. $data->code .' telah dilakukan. Silakan tunggu petugas kami menghubungi anda.'
		]);
		$user = \App\User::find($data->ms_user_id);
		$sms['content'] = '[KEISKEI.CO.ID]~'.'Checkout Invoice '. $data->code .' telah dilakukan. Silakan melakukan pembayaran. Call Center KEISKEI:+62.31.99.00.6002';
		$sms['receiver'] = $user->mobile;
		$this->sendNotif($sms);
		Session::flash('success','Checkout telah berhasil dilakukan. Silakan tunggu petugas kami menghubungi anda.');
		return redirect()->route('user.report');
	}
	public function checkoutNormal()
	{
		$data = \App\Order::where('ms_user_id',Auth::id())
			->where('ms_status_order_id','1')
			->first();
		$data->ms_status_order_id = '2';
		$data->save();
		\App\LogActivity::create([
			'ms_user_id' => Auth::id(),
			'description' => 'Checkout Invoice '. $data->code .' telah dilakukan. Silakan melakukan pembayaran. Call Center KEISKEI:+62.31.99.00.6002 '
		]);
		$user = \App\User::find($data->ms_user_id);
		$this->sendEmail('7', $user->email, $data->code);
		$sms['content'] = '[KEISKEI.CO.ID]~'.'Checkout Invoice '. $data->code .' telah dilakukan. Silakan melakukan pembayaran.';
		$sms['receiver'] = $user->mobile;
		$this->sendNotif($sms);
	


		Session::flash('success','Checkout telah berhasil dilakukan. Silakan lakukan pembayaran.');
		return redirect()->route('user.report');
	}

	public function apiNormal(Request $request)
	{
		$order_id = $request->input('order_id');
		$payment_id = $request->input('payment_id');
		$data = \App\Order::find($order_id);
		$data->ms_payment_id = $payment_id;
		$data->save();
		return response()->json(1);
	}
}
