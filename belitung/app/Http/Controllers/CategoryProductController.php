<?php namespace App\Http\Controllers;

use App\CategoryProduct;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CategoryProductRequest;
use Illuminate\Http\Request;
use Session;

class CategoryProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = CategoryProduct::with('parent')->get();
		return view('pages.categoryproduct.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['categoryproduct'] = CategoryProduct::all();
		return view('pages.categoryproduct.create',compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CategoryProductRequest $request)
	{
		CategoryProduct::create($request->all());
		return redirect()->route('ki-admin.categoryproduct.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['categoryproduct'] = CategoryProduct::all();
		$data['content'] = CategoryProduct::find($id);
		return view('pages.categoryproduct.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CategoryProductRequest $request,$id)
	{
		$data = CategoryProduct::find($id);
		$data->update($request->all());
		return redirect()->route('ki-admin.categoryproduct.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data_t = CategoryProduct::with(
			'products',
			'products.detailAttributeProducts',
			'products.prices',
			'products.detailOrders',
			'products.detailOrders.detailAttributeProductDetailOrders',
			'products.reviews',
			'products.stockProducts',
			'subcategory'
		)->find($id);
		foreach ($data_t->subcategory as $row) {
			$row->parent_id = 0;
			$row->save();
		}
		foreach ($data_t->products as $data) {
			foreach ($data->detailAttributeProducts as $row) {
				$row->delete();
			}
			foreach ($data->prices as $row) {
				$row->delete();
			}
			foreach ($data->reviews as $row) {
				$row->delete();
			}
			foreach ($data->stockProducts as $row) {
				$row->delete();
			}
			foreach ($data->detailOrders as $row) {
				foreach ($row->detailAttributeProductDetailOrders as $val) {
					$val->delete();
				}
				$row->delete();
			}
			foreach ($data->productImages as $row) {
				if(File::exists($row->image))
					File::delete($row->image);
				$row->delete();
			}
		}
		$data_t->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.categoryproduct.index');
	}

}
