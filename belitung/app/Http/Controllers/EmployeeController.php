<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\EmployeeRequest;
use Auth;
use Hash;
use Illuminate\Http\Request;

class EmployeeController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = \App\User::with('roles')->whereHas('roles',function($q)
		{
		    $q->where('locked','1');
		})->get();
		return view('pages.employee.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['role'] = \App\Role::where('level','>',Auth::user()->role_id)->where('locked','1')->lists('name','id');
		$data['city'] = \App\City::lists('name','id');
		return view('pages.employee.form',compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(EmployeeRequest $request)
	{
		$role = \App\Role::find($request->input('ms_role_id'));
		$data = \App\User::create($request->all());
		$data->password = Hash::make($request->input('password'));
		$data->code = $role->code.'00'.$data->id;
		$data->save();
		$data->roles()->attach($role->id);
		return redirect()->route('ki-admin.employee.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['role'] = \App\Role::where('level','>',Auth::user()->role_id)->where('locked','1')->lists('name','id');
		$data['city'] = \App\City::lists('name','id');
		$data['content'] = \App\User::find($id);
		return view('pages.employee.form',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(EmployeeRequest $request, $id)
	{
		$role = \App\Role::find($request->input('ms_role_id'));
		$data = \App\User::find($id);
		$data->update($request->all());
		$data->code = $role->code.'00'.$data->id;
		$data->save();
		$data->roles()->detach();
		$data->roles()->attach($request->input('ms_role_id'));
		return redirect()->route('ki-admin.employee.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data=\App\User::find($id);
		$data->roles()->detach();
		$data->delete();
		return redirect()->route('ki-admin.employee.index');
	}
	public function reset($id)
	{
		$data = User::find($id);
		$password = mt_rand();
		$data->password = Hash::make($password);
		$data->save();
		$data['password'] = $password;
		Session::flash('success', 'Please Check Email');
		$this->sendEmail('2',$data->email,$data);
		return redirect()->route('ki-admin.employee.index');
	}

}
