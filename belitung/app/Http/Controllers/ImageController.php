<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Image as ImageModel;
use Image as ImageCreator;
use App\Http\Requests\ImageRequest;
use File;

class ImageController extends Controller {

	protected $path;

	function __construct() {
		$this->path = 'data/images/';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = ImageModel::all();
		return view('pages.image.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('pages.image.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ImageRequest $request)
	{
		if ($request->hasFile('new-image'))
		{
			$newImage = ImageModel::create($request->all());
		    $imageFile = $request->file('new-image');
			$extension = $imageFile->getClientOriginalExtension();
			$no = 1;
			if (!File::exists($this->path))
			{
				File::makeDirectory($this->path, $mode = 0777, true, true);
			}
			while(File::exists($this->path."image_".sprintf("%05d", $no).'.'.$extension)) {
				$no++;
			}
			$fileName = "image_".sprintf("%05d", $no).'.'.$extension;
			ImageCreator::make($imageFile->getRealPath())->save($this->path.$fileName);
			$newImage->image = $this->path.$fileName;
			$newImage->save();
		}
		return redirect()->route('ki-admin.image.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$image = ImageModel::find($id);
		if (File::exists($image->image)) {
			unlink($image->image);
		}
		$image->delete();
		return redirect()->back();
	}

}
