<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\LogActivity;
use Illuminate\Http\Request;

use Auth;

class LogActivityController extends BasicController {
	protected $modelName;
	protected $path;

	function __construct() {
		$this->modelName = '\App\LogActivity';
		$this->path = 'data/user/';

		parent::__construct();
	}

	public function indexUser()
	{
		$data['content'] = LogActivity::where('ms_user_id', Auth::id())->orderBy('created_at','desc')->paginate(15);
		$data['content']->setPath('status');
		return view('theme.'.$this->theme->code.'.pages.user.status',compact('data'));
	}

}
