<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Input;

class OrderController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['statusorder'] = \App\StatusOrder::where('id','<>','1')->lists('name','id');
		$data['content'] = \App\Order::where('ms_status_order_id','<>','1')->orderBy('created_at','desc')->get();
		return view('pages.order.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['default_role'] = \App\Role::where('default','1')->first();
		$data['content'] = \App\Order::find($id);
		$data['statusorder'] = \App\StatusOrder::where('id','<>','1')->lists('name','id');
		return view('pages.order.show', compact('data'));
	}
	public function change($id = null)
	{
		$status = Input::get('state');
		if(empty($id) || empty($status))
			exit();
		$data = \App\Order::find($id);
		$data->ms_status_order_id = $status;
		$status_obj = \App\StatusOrder::find($status);
		\App\LogActivity::create([
			'ms_user_id' => $data->ms_user_id,
			'description' => 'Administrator telah mengubah status invoice ('.$data->code.') menjadi '.$status_obj->name
		]);			

		$data->save();
		if($status == '4')
		{
			$this->refreshWithdraw($data->ms_user_id);
			$user = \App\User::find($data->ms_user_id);
			$this->sendEmail('6', $user->email, $data->code);
			$user_t = \App\User::find($data->ms_user_id);
			if(!empty($user_t)){
				$sms['content'] = '[INFO KEISKEI]~'.'Administrator telah mengubah status invoice ('.$data->code.') menjadi '.$status_obj->name;
				$sms['receiver'] = $user_t->mobile;
				$this->sendNotif($sms);
			}
		}
		Session::flash('success', 'Status order / invoice telah diperbarui menjadi '.$status_obj->name);
		return redirect()->route('ki-admin.order.show',[$id]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = \App\Order::with('transfers','detailOrders','detailOrders.detailAttributeProductDetailOrders')->find($id);
		foreach ($data->detailOrders as $row) {
			foreach ($row->detailAttributeProductDetailOrders as $val) {
				$val->delete();
			}
			$row->delete();
		}
		foreach ($data->transfers as $row) {
			$row->delete();
		}
		$data->delete();
		Session::flash('success', 'Status order / invoice telah berhasil dihapus');
		return redirect()->route('ki-admin.order.index');
	}

}
