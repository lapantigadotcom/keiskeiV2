<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserRegistrationFeeRequest;
use App\RegistrationFee;
use Illuminate\Http\Request;

use Auth;
use Session;
use Image;
use Input;

class RegistrationFeeController extends BasicController {
	protected $path;
	function __construct() {
		$this->path = 'data/user/registrationfee/';
		parent::__construct();
	}
	public function userCreate()
	{
		$data['payment'] = \App\Payment::all();
		$data['user'] = \App\User::with('roles','roles.point')->find(Auth::id());
		return view('theme.'.$this->theme->code.'.pages.user.registrationfee-form',compact('data'));
	}

	public function userStore(UserRegistrationFeeRequest $request)
	{
		$data = RegistrationFee::create($request->all());
		$data->ms_user_id = Auth::id();
		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
			$extension = $file->getClientOriginalExtension();
			$fileName = Auth::user()->code."_registrationfee_".date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->save($this->path.$fileName, 60);
			$data->file = $fileName;
			
		}
		$data->status = '0';
		$data->save();
		Session::flash('success','Registration Fee telah dikirim. Silahkan Tunggu konfirmasi dari Admin');
		\App\LogActivity::create([
			'ms_user_id' => Auth::id(),
			'description' => 'User '.Auth::user()->username.' telah melakukan konfirmasi pembayaran Registration Fee. Tunggu Admin, melakukan konfirmasi.'
		]);
		return redirect()->route('user.dashboard');
	}

	public function index($type = 0)
	{
		$type = Input::get('state');
		if(empty($type))
		{
			$type = '0';
			$data['content'] = RegistrationFee::with('user','user.roles','payment')->where('status',$type)->orWhere('status',null)->orderBy('created_at','desc')->get();
		}
		else
			$data['content'] = RegistrationFee::with('user','user.roles','payment')->where('status',$type)->orderBy('created_at','desc')->get();
		return view('pages.notif-registrationfee.index', compact('data'));
	}
	public function show($id = '1')
	{
		$data['content'] = RegistrationFee::with('user','user.roles','payment')->find($id);
		return view('pages.notif-registrationfee.show', compact('data'));
	}

	public function change($id = null)
	{
		$status = Input::get('state');
		if(empty($id) || empty($status))
			exit();
		$data = RegistrationFee::find($id);
		$data->status = $status;
		if($status == '1')
		{
			$user = \App\User::with('roles')->find($data->ms_user_id);
			$user->approved = '1';
			$user->save();
			\App\LogActivity::create([
				'ms_user_id' => $data->ms_user_id,
				'description' => 'Administrator telah menyetujui Registration Fee Anda'
			]);
			$this->sendEmail('3', $user->email, $user);
			
		}
		$data->save();
		return redirect()->route('ki-admin.notif-registrationfee.index');
	}
}
