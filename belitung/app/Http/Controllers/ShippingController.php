<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ThirdParty\Ongkir;

use Illuminate\Http\Request;
use Auth;

class ShippingController extends BasicController {

	function getService(Request $request)
	{
		$courier_id = $request->input('courier_id');
		$courier = \App\Courier::find($courier_id);
		$ongkir = new Ongkir($this->api_shipping);
		$user = \App\User::find(Auth::id());
		$order = \App\Order::
			with('detailOrders','detailOrders.product')
			->where('ms_status_order_id','1')
			->where('ms_user_id',Auth::id())
			->first();
		$weight = 0;
		foreach ($order->detailOrders as $row) {
			$weight += $row->total * $row->product->weight;
		}
		$ongkir = new Ongkir($this->api_shipping);
		$service = $ongkir->getCost(intval($this->origin),$user->ms_city_id,intval($weight*1000),$courier->code);
		$min_obj = array();
		foreach ($service->{'rajaongkir'}->{'results'} as $row) {
			foreach ($row->{'costs'} as $val) {
				$tmp = array();
				array_push($tmp, $val->service);
				array_push($tmp, intval($val->cost[0]->value));
				array_push($tmp, intval($val->cost[0]->etd));
				array_push($min_obj, $tmp);
			}
		}
		return response()->json($min_obj);
	}

	public function setService(Request $request)
	{
		$service = $request->input('service');
		$price = $request->input('price');
		$courier_id = $request->input('courier_id');
		$order = \App\Order::with('courier')->where('ms_status_order_id',1)->where('ms_user_id', Auth::id())->first();
		$order->service_courier = $service;
		$order->ms_courier_id = $courier_id;
		$order->shipping_price = $price;
		$order->save();
		return response()->json($order);
	}

}
