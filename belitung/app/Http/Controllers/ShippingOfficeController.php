<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ShippingOfficeRequest;
use Session;
use Input;

class ShippingOfficeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = \App\ShippingOffice::all();
		return view('pages.shippingoffice.index', compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['courier'] = \App\Courier::lists('name','id');
		return view('pages.shippingoffice.create',compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ShippingOfficeRequest $request)
	{
		$data = \App\ShippingOffice::create($request->all());
		$data->status = 0;
		$data->save();
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('ki-admin.shippingoffice.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = \App\ShippingOffice::find($id);
		return view('pages.shippingoffice.edit', compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ShippingOfficeRequest $request, $id)
	{
		$data = \App\ShippingOffice::find($id);
		$data->update($request->all());
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('ki-admin.shippingoffice.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data_t = \App\ShippingOffice::find($id);
		$data_t->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('ki-admin.shippingoffice.index');
	}

	public function change($id)
	{
		$status = Input::get('state');
		if(empty($id) || empty($id))
			exit();
		$data = \App\ShippingOffice::find($id);
		$data->status = $status;
		$data->save();
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('ki-admin.shippingoffice.index');
	}

}
