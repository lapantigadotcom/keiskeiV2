<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use DB;
use Input;
use App\Withdraw;
use Session;

class WithdrawController extends BasicController {
	/*
		1 adalah witdrwaw setuju.
		2 adalah user mengajukan withdraw, menunggu persetujuan
		3 adalah tidak disetujui.

	*/
	

	public function indexUser()
	{
		$this->refreshWithdraw(Auth::id());
		$data['content'] = \App\Withdraw::where('ms_user_id',Auth::id())->orderBy('year','desc')->orderBy('month','desc')->paginate(15);
		$temp_t= \App\Withdraw::where('ms_user_id',Auth::id())->where('status','<>','1')->where('status','<>','3')->sum('referral_value');
		$temp_t2= \App\Withdraw::where('ms_user_id',Auth::id())->where('status','<>','1')->where('status','<>','3')->sum('monthly_value');
		$data['total_active'] = intval($temp_t)+intval($temp_t2);
		$temp_t= \App\Withdraw::where('ms_user_id',Auth::id())->where('status','1')->where('status','3')->sum('referral_value');
		$temp_t2= \App\Withdraw::where('ms_user_id',Auth::id())->where('status','1')->where('status','3')->sum('monthly_value');
		$data['total_inactive'] = intval($temp_t)+intval($temp_t2);
		$data['total'] = $data['total_active'] + $data['total_inactive'] ;
		return view('theme.'.$this->theme->code.'.pages.user.withdraw',compact('data'));
	}
	public function getUser()
	{
		$max=\App\Withdraw::where('ms_user_id',Auth::id())->where('status','1')->max('batch');
		$batch = intval($max)+1;
		if(date('j') === date('t'))
		{
			$date = date('Y-m-d H:i:s');
			\App\Withdraw::where('ms_user_id',Auth::id())->where('status','<>','1')->where('status','<>','3')->update(['status' => '2','batch' => $batch,'withdraw_date' => $date]);
			\App\LogActivity::create([
				'ms_user_id' => Auth::id(),
				'description' => 'User '.Auth::user()->username.' telah melakukan permintaan withdraw bonus.'
			]);
		}

		// disable withdraw per akhir bulan //
		// else{
			// Session::flash('danger','Maaf, Withdraw hanya dapat dilakukan pada akhir bulan');
		// }
		return redirect()->route('user.withdraw');
	}
	public function index()
	{
		$type = Input::get('state');
		if(empty($type))
			$data['content'] = Withdraw::with('user')->where('status',$type)->orWhere('status', null)->orWhere('status', '')->get();
		else
			$data['content'] = Withdraw::with('user')->where('status',$type)->get();
		return view('pages.notif-withdraw.index', compact('data'));
	}
	public function change($id = null)
	{
		$status = Input::get('state');
		if(empty($id) || empty($status))
			exit();
		$data = Withdraw::find($id);
		$data->status = $status;
		if($status == '1')
		{
			// $user = \App\User::find($data->ms_user_id);
			// $user->approved = '1';
			// $user->save();
			
			\App\LogActivity::create([
				'ms_user_id' => $data->ms_user_id,
				'description' => 'Administrator telah menyetujui permintaan withdraw bonus anda.'
			]);
		}
		$data->save();
		return redirect()->route('ki-admin.notif-withdraw.index');
	}
	public function destroy($id)
	{
		Withdraw::destroy($id);
		return redirect()->route('ki-admin.notif-withdraw.index');
	}
}
