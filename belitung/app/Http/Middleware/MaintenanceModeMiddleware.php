<?php namespace App\Http\Middleware;

use App\General;
use Closure;

class MaintenanceModeMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$data = General::first();
		if(empty($data) || $data->active=='0' || empty($data->active))
			return redirect()->route('maintenance');
		return $next($request);	
	}

}
