<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use App\General;
use Input;

class TokenLoginMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// if(Auth::check() && Auth::user()->roles->first()->level=='1')
		$token = General::find(1)->token_admin;
		if($token == Input::get('token'))
		{
			return $next($request);
		}
		return redirect()->route('home');
	}

}
