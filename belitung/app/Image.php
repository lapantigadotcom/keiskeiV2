<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Slider;

class Image extends Model {

	protected $table = 'ms_images';
	protected $fillable = ['caption', 'description'];
    public $timestamps = false;

    public function sliders()
    {
    	return $this->hasMany('App\Slider', 'ms_image_id', 'id');
    }

}
