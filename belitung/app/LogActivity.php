<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class LogActivity extends Model {

	protected $table = 'tr_log_activity';
	protected $guarded = ['id'];
    public $timestamps = true;

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}

}
