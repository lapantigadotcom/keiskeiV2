<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class NotifEmail extends Model {

	protected $table = 'tr_notif_emails';
	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}

	public function recipient()
	{
		return $this->belongsTo('App\User', 'ms_user_recipient_id', 'id');
	}

}
