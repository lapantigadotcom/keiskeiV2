<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailOrder;
use App\StatusOrder;
use App\User;
use App\Courier;
use App\Transfer;

class Order extends Model {

	protected $table = 'tr_orders';
	protected $guarded = ['id'];

	public function detailOrders()
	{
		return $this->hasMany('\App\DetailOrder', 'tr_order_id', 'id');
	}

	public function statusOrder()
	{
		return $this->belongsTo('\App\StatusOrder', 'ms_status_order_id', 'id');
	}

	public function user()
	{
		return $this->belongsTo('\App\User', 'ms_user_id', 'id');
	}
	public function reseller()
	{
		return $this->belongsTo('\App\User', 'ms_reseller_id', 'id');
	}

	public function courier()
	{
		return $this->belongsTo('\App\Courier', 'ms_courier_id', 'id');
	}
	public function payment()
	{
		return $this->belongsTo('\App\Payment', 'ms_payment_id', 'id');
	}

	public function transfers()
	{
		return $this->hasMany('\App\Transfer', 'tr_order_id', 'id');
	}
	public function general()
	{
		return $this->belongsTo('\App\general' , 'tr_mobile', 'id');		
	}

	 

}

