<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transfer;

class Payment extends Model {

	protected $table = 'ms_payments';
	protected $guarded = ['id'];
    public $timestamps = false;

	public function transfers()
	{
		return $this->hasMany('App\Transfer', 'ms_payment_id', 'id');
	}

}
