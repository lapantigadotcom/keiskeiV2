<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model {
    protected $table = 'ms_permissions';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $incrementing = true;
    protected $fillable = array(
        'route',
        'enabled',
    );

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'ms_permission_role', 'ms_permission_id', 'ms_role_id');
    }

    public function listRoles()
    {
        return $this->group();
    }

}
