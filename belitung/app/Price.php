<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Role;
use App\Currency;

class Price extends Model {

	protected $table = 'ms_prices';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function product()
    {
    	return $this->belongsTo('App\Product', 'ms_product_id', 'id');
    }

    public function role()
    {
    	return $this->belongsTo('App\Role', 'ms_role_id', 'id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency', 'ms_currency_id', 'id');
    }

}
