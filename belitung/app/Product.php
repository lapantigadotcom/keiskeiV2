<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetailAttributeProduct;
use App\Price;
use App\Brand;
use App\CategoryProduct;
use App\StatusProduct;
use App\ProductImage;
use App\DetailOrder;
use App\Review;
use App\StockProduct;

class Product extends Model {

	protected $table = 'ms_products';
	protected $fillable = ['title', 'description', 'bahan','specification', 'ms_brand_id', 'ms_category_product_id', 'ms_status_product_id',
                            'code', 'length', 'weight', 'help', 'discount'];

    public function detailAttributeProducts()
    {
    	return $this->hasMany('App\DetailAttributeProduct', 'ms_product_id', 'id');
    }

    public function prices()
    {
    	return $this->hasMany('App\Price', 'ms_product_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'ms_brand_id', 'id');
    }

    public function categoryProduct()
    {
        return $this->belongsTo('App\CategoryProduct', 'ms_category_product_id', 'id');
    }

    public function statusProduct()
    {
        return $this->belongsTo('App\StatusProduct', 'ms_status_product_id', 'id');
    }

    public function productImages()
    {
        return $this->hasMany('App\ProductImage', 'ms_product_id', 'id');
    }

    public function detailOrders()
    {
        return $this->hasMany('App\DetailOrder', 'ms_product_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review', 'ms_product_id', 'id');
    }

    public function stockProducts()
    {
        return $this->hasMany('App\StockProduct', 'ms_product_id', 'id');
    }

}
