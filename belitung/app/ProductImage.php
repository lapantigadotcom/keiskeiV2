<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class ProductImage extends Model {

	protected $table = 'ms_product_images';
	protected $fillable = ['ms_product_id', 'caption', 'featured'];
    public $timestamps = false;

    public function product()
    {
    	return $this->belongsTo('App\Product', 'ms_product_id', 'id');
    }

}
