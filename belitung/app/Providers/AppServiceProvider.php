<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use App\Transfer;
use App\RegistrationFee;
use App\Withdraw;
use App\Review;
use App\ContactUs;
use App\Order;
use App\Article;

use DB;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$data = \App\Theme::where('active','1')->first();
		if(!empty($data))
		{
			view()->composer('theme.'.$data->code.'.app',function($v){
				$data = \App\Theme::with('themeMenus')->where('active','1')->first();
				$menuSupport = array();
				foreach ($data->themeMenus as $row) {
					array_push($menuSupport, $row->code);
				}
				$menu = \App\Menu::with(
					'detailMenus',
					'detailMenus.submenu',
					'detailMenus.submenu.submenu',
					'detailMenus.submenu.submenu.submenu'
					)->whereIn('code',$menuSupport)->get();
				$general = \App\General::first();
				$articles = \App\Article::whereHas('categoryArticle', function($q){
					$q->where('locked','1');
				})->orderBy('created_at','asc')->get()->toArray();
				// dd($articles);
				
				$v->with('menu',$menu)
					->with('general',$general)
					->with('theme', $data)
					->with('articles', $articles)
					;
			});
			view()->composer('theme.'.$data->code.'.partials.best-products', function($v){
				$data = \App\DetailOrder::select(DB::raw('sum(total) as total_t,ms_product_id'))
					->groupBy('ms_product_id')
					->orderBy('total_t','desc')
					->get()->toArray();

				if (count($data) > 0) {
					$arr = array();
					foreach ($data as $key => $value) {
						array_push($arr, $value['ms_product_id'] );
					}
					$hot = \App\Product::with('productImages')->whereIn('id', $arr)->get();
					$v->with('hot_product',$hot);
				}
			});
			view()->composer('theme.'.$data->code.'.include.sidebar-default', function($v){
				$latest = \App\Article::whereHas('categoryArticle', function($q){
					$q->where('locked','<>','1');
				})->orderBy('created_at','desc')->orderBy('created_at','desc')->get()->take(5);
				$v->with('latest_article',$latest);
			});
			view()->composer('theme.'.$data->code.'.include.user-sidebar', function($v){
				$latest = \App\Article::whereHas('categoryArticle', function($q){
					$q->where('locked','<>','1');
				})->orderBy('created_at','desc')->orderBy('created_at','desc')->get()->take(5);
				$v->with('latest_article',$latest);
			});
			
		}
		view()->composer('layouts.app', function($v){
			$registration = User::where(function($query) {
				$query->where('enabled', '=', '0')->orWhere('enabled', '=', null)->orWhere('enabled', '=', '');
			})->count();

			$order = Order::count();
			$kontak = contactus::count();
			$blog = Article::where('ms_category_article_id', '=', '2')->count();
 

			$transfer = Transfer::where('status', '=', '0')->orWhere('status', '=', null)->orWhere('status', '=', '')->count();
			$registrationFee = RegistrationFee::where('status', '=', '0')->orWhere('status', '=', null)->orWhere('status', '=', '')->count();
			$withdraw = Withdraw::where('status', '=', '0')->orWhere('status', '=', null)->orWhere('status', '=', '')->count();
			$review = Review::where('enabled', '=', '0')->orWhere('enabled', '=', null)->orWhere('enabled', '=', '')->count();
			$v->with('notificationRegistration', $registration)->with('notificationTransfer', $transfer)->with('notificationRegistrationFee', 
			$registrationFee)->with('notificationWithdraw', $withdraw)->with('notificationReview', $review)
			->with('notificationOrder',$order)->with('notificationKontak',$kontak)->with('notificationblog', $blog) ;
	
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
