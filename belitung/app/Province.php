<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;

class Province extends Model {

	protected $table = 'ms_provinces';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function cities()
    {
    	return $this->hasMany('App\City', 'ms_province_id', 'id');
    }

}
