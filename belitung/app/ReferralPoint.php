<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Role;

class ReferralPoint extends Model {

	protected $table = 'ms_referral_points';
	protected $guarded = ['id'];
	protected $fillable= ['ms_role_id','target_ms_role_id','value'];
    public $timestamps = true;

    public function role()
    {
        return $this->belongsTo('App\Role', 'ms_role_id');
    }
    public function targetrole()
    {
        return $this->belongsTo('App\Role', 'target_ms_role_id');
    }
}
