<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationFee extends Model {

	protected $table = 'tr_registration_fee';
	protected $guarded = ['id'];
    public $timestamps = true;
    public $fillable = [
    	'ms_user_id',
    	'ms_payment_id',
    	'nominal',
    	'account_name',
    	'account_number',
    	'status',
    	'information',
    	'file',
    	'transfer_date'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'ms_user_id', 'id');
    }
    public function payment()
    {
        return $this->belongsTo('App\Payment', 'ms_payment_id', 'id');
    }

}
