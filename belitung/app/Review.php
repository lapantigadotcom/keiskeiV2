<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Product;

class Review extends Model {

	protected $table = 'tr_reviews';
	protected $guarded = ['id'];

	public function user()
	{
		return $this->belongsTo('App\User', 'ms_user_id', 'id');
	}

	public function product()
	{
		return $this->belongsTo('App\Product', 'ms_product_id', 'id');
	}

}
