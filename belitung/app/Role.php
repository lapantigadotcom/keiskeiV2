<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Price;
use App\RoleUser;
use App\User;

class Role extends Model {

	protected $table = 'ms_roles';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function prices()
    {
        return $this->hasMany('App\Price', 'ms_role_id', 'id');
    }
    public function point()
    {
        return $this->hasOne('App\Point', 'ms_role_id', 'id');
    }
    public function adminmenu()
    {
        return $this->belongsToMany('App\Adminmenu', 'ms_adminmenu_role', 'ms_role_id', 'ms_adminmenu_id');
    }
    public function permission()
    {
        return $this->belongsToMany('App\Permission', 'ms_permission_role', 'ms_role_id', 'ms_permission_id');
    }
    public function roleUsers()
    {
    	return $this->hasMany('App\RoleUser', 'ms_role_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'ms_role_user', 'ms_role_id', 'ms_user_id');
    }

    public function referralpoint()
    {
        return $this->hasMany('\App\ReferralPoint', 'ms_role_id', 'id');
    }
}
