<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingOffice extends Model {

	protected $table = 'tr_shipping_office';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function courier()
    {
        return $this->belongsTo('\App\Courier', 'ms_courier_id');
    }

}
