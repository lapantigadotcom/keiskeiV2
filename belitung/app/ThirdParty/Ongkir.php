<?php namespace App\ThirdParty;


class Ongkir {
	private static $api_key;
    private static $base_url = "http://rajaongkir.com/api/basic/";

    /**
     * Constructor
     * @param string $api_key API Key Anda sebagaimana yang tercantum di akun panel Ongkir
     * @param array $additional_headers Header tambahan seperti android-key, ios-key, dll
     */
    public function __construct($api_key, $additional_headers = array()) {
    	Ongkir::$api_key = $api_key;
    }

    function getCost($origin, $destination, $weight, $courier=null)
    {

    	$curl = curl_init();
    	$url = '';
    	if(empty($courier))
    		$url = 'origin='.$origin.'&destination='.$destination.'&weight='.$weight;
    	else
    		$url = 'origin='.$origin.'&destination='.$destination.'&weight='.$weight.'&courier='.$courier;
    	
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "http://rajaongkir.com/api/basic/cost",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $url,
		  CURLOPT_HTTPHEADER => array(
		    "content-type: application/x-www-form-urlencoded",
		    "key: ".Ongkir::$api_key
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		  return null;
		} else {
		  return json_decode($response);
		}
    }

function gettracking  ()

{

	$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://rajaongkir.com/api/basic/waybill",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "waybill=SOCAG00183235715&courier=jne",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/x-www-form-urlencoded",
    "key: your-api-key"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
}

}
