<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

	protected $table = 'tr_transactions';
	protected $guarded = ['id'];
    public $timestamps = true;

    public function user()
    {
    	return $this->belongsTo('App\User', 'ms_user_id', 'id');
    }

}
