<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Attribute;

class TypeAttribute extends Model {

	protected $table = 'ms_type_attributes';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function attributes()
    {
    	return $this->hasMany('App\Attribute', 'ms_type_attribute_id', 'id');
    }

}
