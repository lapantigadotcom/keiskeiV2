<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Article;
use App\RoleUser;
use App\Role;
use App\City;
use App\UserFile;
use App\Log;
use App\Order;
use App\Review;
use App\Transfer;
use App\NotifEmail;
use App\NotifMessage;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
	protected $table = 'ms_users';
	protected $guarded = ['id'];
    protected $fillable = [
                        'code',
                        'username',
                        'name',
                        'email',
                        'gender','pinbb',
                        'telephone',
                        'mobile',
                        'address',
                        'ms_city_id',
                        'ms_country_id',
                        'photo',
                        'postcode',
                        'fax',
                        'company',
                        'account_bank',
                        'ms_bank_id',
                        'referraled_by',
                        'description',
                        'enabled',
                        'approved',
                        'first_login',
                        'safe',
                        'required_step',
                        'valid_date',
                        'role_id',
                        'last_logtime',
                        'last_ip'
                        ];
    protected $exceptionalRoute = array(
        'user/change_role',
    );
    protected $effectiveMenu = null;
    protected $effectivePermission = null;
    protected $groupRole = null;
	protected $hidden = ['password', 'remember_token'];

	public function articles()
	{
		return $this->hasMany('App\Article', 'ms_category_article_id', 'id');
	}

    public function roleUsers()
    {
    	return $this->hasMany('\App\RoleUser', 'ms_user_id', 'id');
    }
    public function isUser()
    {
        $roles = $this->roles->toArray();
        return !empty($roles);
    }
    public function hasRole($check)
    {
        return in_array($check, array_fetch($this->roles->toArray(),'code'));
    }

    private function getIdInArray($array, $term)
    {
        foreach ($array as $key => $value) {
            if($value == $term)
            {
                return $key;
            }
        }
        throw new UnexpectedValueException;
    }
    public function makeUser($t)
    {
        $assign = array();
        $roles = array_fetch(\App\Role::all()->toArray(),'name');
        $assign[] = $this->getIdInArray($roles,$t);
        $this->roles->attach($assign);
    }

    public function bank()
    {
        return $this->belongsTo('\App\Bank', 'ms_bank_id', 'id');
    }
    public function city()
    {
        return $this->belongsTo('\App\City', 'ms_city_id', 'id');
    }
    public function country()
    {
        return $this->belongsTo('\App\Country', 'ms_country_id', 'id');
    }

    public function userFiles()
    {
    	return $this->hasOne('\App\UserFile', 'ms_user_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany('\App\Log', 'ms_user_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany('\App\Order', 'ms_user_id', 'id');
    }
    public function ordersReport()
    {
        return $this->hasMany('\App\Order', 'ms_user_id', 'id')->where('ms_status_order_id','<>','1');
    }

    public function reviews()
    {
        return $this->hasMany('\App\Review', 'ms_user_id', 'id');
    }

    public function transfers()
    {
        return $this->hasMany('\App\Transfer', 'ms_user_id', 'id');
    }

    public function notifEmails()
    {
        return $this->hasMany('\App\NotifEmail', 'ms_user_id', 'id');
    }  
    public function referraledBy()
    {
        return $this->belongsTo('\App\User', 'referraled_by','id');
    }
    public function referral()
    {
        return $this->hasMany('\App\User', 'referraled_by','id');
    }

    public function roles()
    {
        return $this->belongsToMany('\App\Role', 'ms_role_user', 'ms_user_id', 'ms_role_id');
    }

    public function listRole()
    {
        return $this->roles();
    }

    public function hasAccess($route)
    {
        foreach ($this->exceptionalRoute as $exceptionalRoute)
        {
            if ($exceptionalRoute == $route)
            {
                return true;
            }
        }

        $permissions = $this->getEffectivePermission();
        foreach ($permissions as $permission) 
        {
            if ($permission->route == $route)
            {
                return true;
            }
        }
        return false;
    }

    public function getEffectiveMenu()
    {
        if ($this->effectiveMenu != null)
        {
            return $this->effectiveMenu;
        }
        else
        {
            $role = \App\Role::with(array('adminmenu' => function($query){
                $query->orderBy('parent_id')->orderBy('order_item');
            }))->find($this->getRoleId());
            if ($role != null)
            {
                $effectiveMenu = array();
                foreach ($role->adminmenu as $adminmenu) 
                {
                    if ($adminmenu->parent_id == 0)
                    {
                        array_push($effectiveMenu, $adminmenu);
                        $adminmenu->_child = array();
                        foreach ($role->adminmenu as $child) 
                        {
                            if ($child->parent_id == $adminmenu->id)
                            {
                                array_push($adminmenu->_child, $child);

                                $child->_child = array();
                                foreach ($role->adminmenu as $grandChild) 
                                {
                                    if ($grandChild->parent_id == $child->id)
                                    {
                                        array_push($child->_child, $grandChild);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->effectiveMenu = $effectiveMenu;
            }
            else
            {
                $this->effectiveMenu = array();
            }
            return $this->effectiveMenu;
        }
    }

    public function getEffectivePermission()
    {
        if ($this->effectivePermission != null)
        {
            return $this->effectivePermission;
        }
        else
        {
            $role = \App\Role::with('permission')->find($this->getRoleId());
            if ($role != null)
            {
                $this->effectivePermission = $role->permission;
            }
            else
            {
                $this->effectivePermission = array();
            }
            return $this->effectivePermission;
        }
    }

    public function setRoleId($id)
    {
        $role = \App\Role::find($id);
        if ($role != null)
        {
            $this->role_id = $id;
            $this->save();
        }
    }

    public function getRoleId()
    {
        if ($this->role_id == 0)
        {
            if ($this->roles->count() > 0)
            {
                $id = $this->roles->sortByDesc('level')->first()->id;
                $this->setRoleId($id);
                return $this->role_id;
            }
            else
            {
                return 0;
            }            
        }
        else
        {
            return $this->role_id;
        }
    }

    public function getRole()
    {
        if ($this->groupRole != null)
        {
            return $this->groupRole;
        }
        else
        {
            $this->groupRole = \App\Role::find($this->getRoleId());
            return $this->groupRole;
        }
    }    

    public function addRole($role)
    {
        if (!$this->inRole($role))
        {
            $this->roles()->attach($role);
        }
        return true;
    }

    public function removeGroup($role)
    {
        if ($this->inRole($role))
        {
            $this->roles()->detach($role);
        }
        return true;
    }

    public function inRole($role)
    {
        foreach ($this->roles()->get() as $userRole)
        {
            if ($userRole->id == $role->id)
            {
                return true;
            }
        }
        return false;
    }

    public function setRoleByIds($role_ids)
    {
        if (is_array($role_ids))
        {
            $this->roles()->detach();
            foreach ($role_ids as $group_id) {
                $role = \App\Role::find($group_id);
                if ($role != null)
                {
                    $this->addRole($role);
                }
            }
            return true;
        }
        return false;
    }

    public function registrationfee()
    {
        return $this->hasOne('\App\RegistrationFee','ms_user_id');
    }

    public function logActivity()
    {
        return $this->hasMany('\App\LogActivity','ms_user_id')->orderBy('created_at','desc');
    }
    public function withdraw()
    {
        return $this->hasMany('\App\Withdraw','ms_user_id')->orderBy('created_at','desc');
    }

    public function emailRecipients()
    {
        return $this->belongsTo('App\NotifEmail', 'ms_user_recipient_id', 'id');
    }

    public function smsRecipients()
    {
        return $this->belongsTo('App\NotifSMS', 'ms_user_recipient_id', 'id');
    }

    public function notifMessages()
    {
        return $this->hasMany('App\NotifMessage', 'ms_user_id', 'id');
    }

    public function messageRecipients()
    {
        return $this->hasMany('App\NotifMessage', 'ms_user_recipient_id', 'id');
    }
    public function reseller()
    {
        return $this->hasMany('\App\Order','ms_reseller_id')->orderBy('created_at','desc');
    }
    public function accountBank()
    {
        return $this->hasMany('\App\AccountBank','ms_user_id');
    }
    public function arrProduct($id)
    {
        $flag = false;
        $order = $this->orders;
        foreach ($order as $row) {
            $order_t = \App\Order::with('detailOrders')->find($row->id);
            if($order_t->ms_status_order_id != '4' || $order_t->ms_status_order_id != '7' || $order_t->ms_status_order_id != '8' )
            {
                foreach ($order_t->detailOrders as $value) {
                    if($value->ms_product_id == $id)
                        $flag=true;
                }
            }
        }
        return $flag;
    }
}
