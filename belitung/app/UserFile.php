<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserFile extends Model {

	protected $table = 'ms_user_files';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\User', 'ms_user_id', 'id');
    }

}
