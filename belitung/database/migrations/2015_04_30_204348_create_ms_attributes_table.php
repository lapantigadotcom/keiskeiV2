<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsAttributesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_attributes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_type_attribute_id')->index('fk_ms_attribute_ms_type_attribute1_idx');
			$table->string('name', 64)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_attributes');
	}

}
