<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsDetailAttributeProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_detail_attribute_product', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_detail_attribute_id')->index('fk_ms_detail_attribute_product_ms_detail_attribute1_idx');
			$table->integer('ms_product_id')->index('fk_ms_detail_attribute_product_ms_products1_idx');
			$table->boolean('enabled');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_detail_attribute_product');
	}

}
