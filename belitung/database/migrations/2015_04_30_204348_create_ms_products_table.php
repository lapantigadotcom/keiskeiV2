<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 128)->nullable();
			$table->string('description', 512)->nullable();
			$table->string('specification', 512)->nullable();
			$table->integer('ms_brand_id')->index('fk_ms_products_ms_brands1_idx');
			$table->integer('ms_category_product_id')->index('fk_ms_products_ms_categories1_idx');
			$table->integer('ms_status_product_id')->index('fk_ms_products_ms_status_product1_idx');
			$table->string('code', 64)->nullable();
			$table->float('length', 10, 0)->nullable();
			$table->float('weight', 10, 0)->nullable();
			$table->string('help', 512)->nullable();
			$table->integer('discount')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_products');
	}

}
