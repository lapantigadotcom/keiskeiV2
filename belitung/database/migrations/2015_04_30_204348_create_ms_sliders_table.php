<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsSlidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_sliders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 64)->nullable();
			$table->string('description', 512)->nullable();
			$table->integer('ms_image_id')->index('fk_ms_slider_ms_images1_idx');
			$table->string('url', 128)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_sliders');
	}

}
