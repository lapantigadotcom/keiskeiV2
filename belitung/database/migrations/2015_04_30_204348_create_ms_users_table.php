<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 64)->nullable();
			$table->string('username', 32)->nullable();
			$table->string('name', 64)->nullable();
			$table->string('email', 64)->nullable();
			$table->string('password', 256)->nullable();
			$table->char('gender', 1)->nullable();
			$table->string('telephone', 32)->nullable();
			$table->string('mobile', 32)->nullable();
			$table->string('address', 256)->nullable();
			$table->string('photo', 128)->nullable();
			$table->string('postcode', 32)->nullable();
			$table->string('fax', 32)->nullable();
			$table->string('company', 128)->nullable();
			$table->string('description', 512)->nullable();
			$table->integer('ms_bank_id')->nullable();
			$table->string('account_bank')->nullable();
			$table->integer('referraled_by')->nullable();
			$table->boolean('enabled')->nullable();
			$table->boolean('approved')->nullable();
			$table->boolean('safe')->nullable();
			$table->boolean('required_step')->nullable();
			$table->boolean('overseas')->nullable();
			$table->date('valid_date')->nullable();
			$table->integer('ms_country_id');
			$table->integer('ms_city_id');
			$table->timestamps();
			$table->rememberToken();
			$table->dateTime('last_logtime')->nullable();
            $table->string('last_ip', 64)->nullable();
            $table->integer('role_id')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_users');
	}

}
