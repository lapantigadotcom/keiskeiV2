<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_reviews', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_user_id');
			$table->integer('ms_product_id');
			$table->integer('rate')->nullable();
			$table->string('title', 64)->nullable();
			$table->string('description', 512)->nullable();
			$table->boolean('enabled')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_reviews');
	}

}
