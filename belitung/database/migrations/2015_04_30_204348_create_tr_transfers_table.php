<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrTransfersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_transfers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ms_payment_id');
			$table->integer('ms_user_id');
			$table->integer('tr_order_id');
			$table->string('custom');
			$table->integer('nominal');
			$table->string('account_name', 64)->nullable();
			$table->string('account_number', 64)->nullable();
			$table->boolean('status')->nullable();
			$table->string('information', 512)->nullable();
			$table->string('file', 512)->nullable();
			$table->date('transfer_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_transfers');
	}

}
