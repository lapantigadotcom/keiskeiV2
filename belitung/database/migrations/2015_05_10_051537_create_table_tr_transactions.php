<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTrTransactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('transaction',64);
			$table->string('balance',64);
			$table->integer('ms_transaction_code_id');
			$table->integer('ms_user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tr_transactions');
	}

}
