<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMsPoints extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_points', function($table)
        {
            $table->increments('id');
            $table->integer('ms_role_id');
            $table->string('point', 16)->nullable();
			$table->string('registrationfee', 16)->nullable();
			$table->string('referral', 16)->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('ms_points');
	}

}
