<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrNotifSmsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_notif_sms', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('ms_user_recipient_id');
            $table->integer('ms_user_id');
            $table->string('receiver', 32);
            $table->string('content', 512);
            $table->boolean('sent');
            $table->boolean('draft');
            $table->boolean('group');
            $table->timestamp('sent_timetamp');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_notif_sms');
	}

}