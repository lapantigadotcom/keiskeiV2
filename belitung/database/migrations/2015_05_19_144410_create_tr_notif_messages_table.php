<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrNotifMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_notif_messages', function($table)
        {
            $table->increments('id');
            $table->integer('parent_id');
            $table->integer('ms_user_id');
            $table->integer('ms_user_recipient_id');
            $table->string('subject', 128);
            $table->string('content', 2048);
            $table->boolean('sent');
            $table->boolean('draft');
            $table->boolean('group');
            $table->timestamp('sent_timetamp');
			$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_notif_messages');
	}

}
