<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrContactusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_contactus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 128)->nullable();
			$table->string('email', 128)->nullable();
			$table->string('subject', 128)->nullable();
			$table->string('message', 2048)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_contactus');
	}

}
