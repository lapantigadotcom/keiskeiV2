<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Attribute;

class AttributeTableSeeder extends Seeder {

    public function run()
    {
        Attribute::truncate();

        Attribute::create([
            'ms_type_attribute_id' => '1',
            'name' => 'Size'
        ]);

        Attribute::create([
            'ms_type_attribute_id' => '1',
            'name' => 'Color'
        ]);

    }

}