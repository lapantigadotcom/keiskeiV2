<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Brand;

class BrandTableSeeder extends Seeder {

    public function run()
    {
        Brand::truncate();

        Brand::create([
            'name' => 'Brand1'
        ]);

        Brand::create([
            'name' => 'Brand2'
        ]);

        Brand::create([
            'name' => 'Brand3'
        ]);

        Brand::create([
            'name' => 'Brand4'
        ]);

        Brand::create([
            'name' => 'Brand5'
        ]);

    }

}