<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CategoryProduct;

class CategoryProductTableSeeder extends Seeder {

    public function run()
    {
        CategoryProduct::truncate();

        CategoryProduct::create([
            'name' => 'Category Product 1'
        ]);

        CategoryProduct::create([
            'name' => 'Category Product 2'
        ]);

        CategoryProduct::create([
            'name' => 'Category Product 3'
        ]);

        CategoryProduct::create([
            'name' => 'Category Product 4'
        ]);

        CategoryProduct::create([
            'name' => 'Category Product 5'
        ]);

    }

}