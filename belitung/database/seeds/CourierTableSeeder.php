<?php

// Composer: "fzaninotto/faker": "v1.4.0"
// use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CourierTableSeeder extends Seeder {

	public function run()
	{
		\App\Courier::truncate();
		\App\Courier::create([
			'name' => 'Pos Indonesia',
			'code' => 'pos'
		]);
		\App\Courier::create([
			'name' => 'Jalur Nugraha Ekakurir (JNE)',
			'code' => 'jne'
		]);
	}

}
