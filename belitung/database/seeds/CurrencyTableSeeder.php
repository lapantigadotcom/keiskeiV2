<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Currency;

class CurrencyTableSeeder extends Seeder {

    public function run()
    {
        Currency::truncate();

        Currency::create([
            'name' => 'Rupiah',
            'symbol' => 'Rp'
        ]);

        Currency::create([
            'name' => 'US Dollar',
            'symbol' => 'US$'
        ]);

    }

}