<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('CategoryArticleTableSeeder');
		$this->call('ArticleTableSeeder');
		$this->call('CurrencyTableSeeder');
		$this->call('ThemeTableSeeder');
		$this->call('TypeAttributeTableSeeder');
		$this->call('AttributeTableSeeder');
		$this->call('DetailAttributeTableSeeder');
		$this->call('BrandTableSeeder');
		$this->call('CategoryProductTableSeeder');
		$this->call('RoleTableSeeder');
		$this->call('StatusProductTableSeeder');
		$this->call('ProvinceTableSeeder');
		$this->call('CityTableSeeder');
		$this->call('UserTableSeeder');
		$this->call('ProductTableSeeder');
		$this->call('TransactionCodeTableSeeder');
		$this->call('CountryTableSeeder');
		$this->call('GeneralTableSeeder');
		$this->call('AuthTableSeeder');
		$this->call('PointTableSeeder');
		$this->call('PaymentTableSeeder');
		$this->call('OrderStatusTable');
		$this->call('BankTableSeeder');
		$this->call('CourierTableSeeder');
		
	}

}
