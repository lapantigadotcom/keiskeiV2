<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\StatusOrder;

class OrderStatusTable extends Seeder {

    public function run()
    {
        StatusOrder::truncate();

        StatusOrder::create([
            'name' => 'Cart'
        ]);

        StatusOrder::create([
            'name' => 'Menunggu Pembayaran'
        ]);

        StatusOrder::create([
            'name' => 'Proses'
        ]);
        StatusOrder::create([
            'name' => 'Selesai'
        ]);
        StatusOrder::create([
            'name' => 'Batal'
        ]);
        StatusOrder::create([
            'name' => 'Kadaluarsa'
        ]);
        StatusOrder::create([
            'name' => 'Overseas'
        ]);
        StatusOrder::create([
            'name' => 'Perwakilan'
        ]);
    }

}