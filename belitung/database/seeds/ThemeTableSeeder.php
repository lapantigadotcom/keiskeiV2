<?php

use App\Theme;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ThemeTableSeeder extends Seeder {

    public function run()
    {
        Theme::truncate();
        Theme::create([
            'code' => 'keiskei',
            'name' => 'KeisKei Indonesia',
            'description' => 'KeisKei Indonesia',
            'active' => '1'
        ]);
    }

}