<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder {

    public function run()
    {
        User::truncate();

        $data = User::create([
            'code' => 'AD00001',
            'username' => 'admin',
            'name' => 'Administrator',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
            'ms_city_id' => '2',
            'enabled' => '1',
            'mobile' => '081000000001'
        ]);
        $data->roles()->attach('1');
        $data = User::create([
            'code' => 'MG00001',
            'username' => 'manager',
            'name' => 'Manager',
            'email' => 'manager@manager.com',
            'password' => Hash::make('manager'),
            'ms_city_id' => '2',
            'enabled' => '1',
            'mobile' => '081000000002'
        ]);
        $data->roles()->attach('2');
        $data = User::create([
            'code' => 'OF00001',
            'username' => 'officer',
            'name' => 'officer',
            'email' => 'officer@officer.com',
            'password' => Hash::make('officer'),
            'ms_city_id' => '2',
            'enabled' => '1',
            'mobile' => '081000000003'
        ]);
        $data->roles()->attach('3');

        $data = User::create([
            'code' => 'US00002',
            'username' => 'user',
            'name' => 'User',
            'email' => 'user@user.com',
            'password' => Hash::make('user'),
            'ms_city_id' => '2',
            'ms_city_id' => '2',
            'enabled' => '1',
            'mobile' => '081000000004'
        ]);
        $data->roles()->attach('4');

        $data = User::create([
            'code' => 'AG00003',
            'username' => 'agent',
            'name' => 'Agent',
            'email' => 'agent@agent.com',
            'password' => Hash::make('agent'),
            'ms_city_id' => '2',
            'ms_country_id' => '1',
            'enabled' => '1',
            'mobile' => '081000000005'
        ]);
        $data->roles()->attach('5');

        $data = User::create([
            'code' => 'DT00004',
            'username' => 'distributor',
            'name' => 'Distributor',
            'email' => 'distributor@distributor.com',
            'password' => Hash::make('distributor'),
            'ms_city_id' => '2',
            'ms_country_id' => '1',
            'enabled' => '1',
            'mobile' => '081000000006'
        ]);
        $data->roles()->attach('6');

        $data = User::create([
            'code' => 'PT00005',
            'username' => 'perusahaan',
            'name' => 'Perusahaan',
            'email' => 'perusahaan@perusahaan.com',
            'password' => Hash::make('perusahaan'),
            'ms_city_id' => '1',
            'ms_country_id' => '1',
            'enabled' => '1',
            'mobile' => '081000000007'
        ]);
        $data->roles()->attach('7');
        $data = User::create([
            'code' => 'OV00006',
            'username' => 'overseas',
            'name' => 'overseas',
            'email' => 'overseas@overseas.com',
            'password' => Hash::make('overseas'),
            'ms_city_id' => '1',
            'ms_country_id' => '2',
            'required_step' => '1',
            'overseas' => '1',
            'enabled' => '1',
            'mobile' => '081000000008'
        ]);
        $data->roles()->attach('8');

    }

}