@extends('theme.keiskei.app')
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang">

<title>Keiskei Indonesia | Reset Password Keiskei Smart System</title>
@section('content')
 
<div class="row">
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">	
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
						@endif
						@if (count($errors) > 0)
						<div class="alert alert-danger">
						<strong>Perhatian!</strong> Terdapat kesalahan input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						
					<div class="login-form"><!--login form-->
						<h2>Reset Password <span style="color:green"><b>Smart System</b></span></h2>
						<p>
						Pastikan memasukan email Anda yang terdaftar di <span style="color:green">Keiskei Smart System</span>. 
					</p>
						{!! BootstrapForm::open(array('url' => url('/password/email'),'method' => 'POST')) !!}
					{!! BootstrapForm::text('email','Silahkan Masukkan Email Anda',old('email') ) !!}
					<div class="form-group text-align form-action">
						{!! Form::submit('Reset',array('class' => 'btn btn-primary')) !!}
					</div>
					{!! BootstrapForm::close() !!}

							</span>
  					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
 				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
					<img src="{{ asset('theme/keiskei_V2/images/banner/banner1.jpeg') }}" style="max-height:450px" class="img-lazy" alt="keiskei 17 Agustus">

					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
</div>
@endsection
