@extends('theme.keiskei.app')

@section('content')
@include('theme.keiskei.scripts.price-product')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Hi, {!! $data['content']->name !!}</h2>
			<h5>{!! $data['content']->code!=''?$data['content']->code:'-' !!}&nbsp; | &nbsp; {!! $data['content']->telephone!=''?$data['content']->telephone:'-' !!} </h5>
		</div>
	</div>
</div>
<div class="row">
					<img src="{{ asset('theme/keiskei_V2/images/banner/banner1.jpeg') }}"  class="img-lazy" alt="keiskei 17 Agustus">

</div>


<center><div class="se-pre-con">
  <div style="left: 0px; margin: -100px auto auto; position: absolute; width: 100%; top: 40%;">
			<div class="row">
						@if(File::exists('data/user/photo/'.$data['content']->photo) and $data['content']->photo != '')
							<img src="{!! asset('data/user/photo/'.$data['content']->photo) !!}" style="max-width:7%"   class="img-responsive img-thumbnail" style="padding:10px;">
						@else
							<img src="{!! asset('theme/keiskei/img/user.png') !!}"  style="max-width:7%"   class="img-responsive img-thumbnail">
						@endif
						</div>
    Membuka Halaman Profil Anda..Mohon ditunggu...</div></div></center>


<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<h4 class="text-center">PROFIL ANDA </h4>
					<hr>
					@include('errors.session')
					<div class="col-md-4">
						@if(Auth::user()->roles->first()->require_file == '1')
						<div class="row">
							<h5>Bonus Aktif : {!! money_format('%(#10n', $data['total_active']) !!}</h5>
						</div>
						@endif
						<div class="row">
						@if(File::exists('data/user/photo/'.$data['content']->photo) and $data['content']->photo != '')
							<img src="{!! asset('data/user/photo/'.$data['content']->photo) !!}" class="img-responsive img-thumbnail" style="padding:10px;">
						@else
							<img src="{!! asset('theme/keiskei/img/user.png') !!}" class="img-responsive img-thumbnail">
						@endif
						</div>
					</div>
					<div class="col-md-8 product-detail">
						<div class="product-title">
						<div class="row">
							<div class="col-md-8">
								<h4>
									{!! $data['content']->name !!}
								</h4>
							</div>
							<div class="col-md-4">
								<a href="javascript:void(0)" class="change-password"  data-toggle="modal" data-target="#change-password"> <i class="fa fa-lock"></i> &nbsp;Ganti Password</a>
							</div>
						</div>
						
						</div>
						<div class="product-description">
						@if($data['content']->roles->first()->default!='1')
							<p> Perusahaan : <strong>{!! $data['content']->company !!}</strong> </p>
						@endif
							<p> ID Anggota : {!! $data['content']->code!=''?$data['content']->code:'-' !!}</p>
							<p> Email : {!! $data['content']->email!=''?$data['content']->email:'-' !!} </p>
							<p> Telepon : {!! $data['content']->telephone!=''?$data['content']->telephone:'-' !!} </p>
							<p> Mobile : {!! $data['content']->mobile!=''?$data['content']->mobile:'-' !!} </p>
							<p> Alamat : {!! $data['content']->address!=''?$data['content']->address:'-' !!} </p>
						@if($data['content']->roles->first()->default!='1')
							<h4>Akun Rekening</h4>
							<ul>
							@foreach($data['content']->accountBank as $row)
								<li>
									{!! $row->bank->name !!} - {!! $row->account_name !!}
								</li>
							@endforeach
							</ul>
						@endif
							<h4>Status Keanggotaan</h4>
							<p class="member-status"> Status : 
							@if($data['content']->approved=='1')
								<span class="unverivied">Terverifikasi</span>
							@else
								<span class="unverivied">Belum Terverifikasi</span>
							@endif
							</p>
							<p> Tipe : {!! $data['content']->roles->first()->name !!}</p>
							<span class="divider">&nbsp;</span>
							<p>
								 {!! $data['content']->description!=''?$data['content']->description:'-' !!} 
							</p>
							<span class="divider">&nbsp;</span>
							<div class="row">
								<div class="col-md-12">
									<a href="{!! route('user.profile.edit') !!}" class="btn btn-keiskei-orange">Edit Profil</a>
									@if($data['content']->roles->first()->require_file=='1' &&  $data['content']->approved!='1')
									@if($data['content']->registrationfee()->count() == 0)
									<a href="{!! route('user.registrationfee') !!}" class="btn btn-danger">Registration Fee</a>
									@endif
									@endif

									<a target="_blank" href="{!! route('user.report') !!}"  class="btn btn-keiskei-default"> 
										Transaksi
									</a>
									@if($data['content']->roles->first()->require_file=='1')
									<a target="_blank" href="{!! route('user.withdraw') !!}"  class="btn btn-keiskei-grey"> 
										Bonus
									</a>
									@if($data['content']->approved === '1')
									@if(date('d') === date('t'))
										<a href="{!! route('user.withdraw.get') !!}" class="btn btn-danger">Withdraw Point</a>
									@else
										<a href="javascript:void(0)" class="btn btn-danger" onclick="withdrawModal()">Withdraw Point</a>
									@endif
									@endif
									@endif


								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist" id="myTab">
						  <li role="presentation" class="active"><a href="#inbox" aria-controls="inbox" role="tab" data-toggle="tab">Inbox</a></li>
						  <li role="presentation"><a href="#outbox" aria-controls="outbox" role="tab" data-toggle="tab">Outbox</a></li>
						  <li role="presentation"><a href="#report" aria-controls="report" role="tab" data-toggle="tab">Report</a></li>
						  <li role="presentation"><a href="#status" aria-controls="status" role="tab" data-toggle="tab">Status</a></li>
						</ul>

						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane" id="status">
						  	<ul>
						  		@foreach($data['content']->logActivity as $row)
						  		<li> <em>{!! date('l, d M Y',strtotime($row->created_at)) !!} </em> {!! $row->description !!}</li>
						  		@endforeach
						  	</ul>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="report">
						  	<table class="table">
							<thead>
								<tr>
									<th>Kode Invoice</th>
									<th>Waktu</th>
									<th>Keterangan</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1; ?>
							@foreach($data['report'] as $row)
							<tr>
								<td>
									<a href="{!! route('user.invoice',[$row->id]) !!}?invoice={!! $row->code !!}" target="_blank">
										<b>{!! $row->code !!}</b>
									</a>
								</td>
								<td>{!! date('j M Y',strtotime($row->created_at)) !!}</td>
								<td>
									<b>{!! $row->statusOrder->name !!}</b>
								</td>
								<td class="text-right">
								<?php 
									$total = 0; ?>
									@foreach($row->detailOrders as $val)
									<?php $total_t = priceDisplay($data['default_role'], $val->product->prices); 
									if(!empty($val->product->discount))
									{
										$discount_t = ($total_t/100)*intval($val->product->discount);
										$total_t = $total_t - $discount_t;
									}
									$total_t = $total_t*$val->total;
									$total += $total_t;
									?>
									@endforeach
									<?php 
									$total = $total + $row->shipping_price;
									?>
									{!! money_format('%(#10n', $total) !!}
								</td>
							</tr>
							@endforeach
							</tbody>
						</table>
						  </div>
						  <div role="tabpanel" class="tab-pane active" id="inbox">
						  	<h4>Pesan Masuk</h4>
						  	<hr>
						  	@foreach($data['message_inbox'] as $row)
						  	<div class="review-detail">
						  		<div class="review-item">
						  			<div class="review-item-head">
						  				<div class="row">
						  					<div class="col-md-12 text-left review-item-inline" style="color:green">
						  						<span style="font-size:12px">Pengirim: &nbsp;</span><span>{!! $row->user->name !!} </span>
						  						<i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($row->created_at)) !!}  <i class="fa fa-bell-o"></i> {!! date('l', strtotime($row->created_at)) !!} 
						  						<i class="fa fa-calendar-o"></i> {!! date('j F', strtotime($row->created_at)) !!} 
						  					</div>
						  				</div>
						  				
						  			</div>
						  			<div class="review-item-body">
						  				<p>
						  					{!! $row->content !!}
						  				</p>
						  			</div>
						  		</div>
						  	</div>
						  	@endforeach
						  	@if($data['message_inbox']->count() == 0)
						  	<div class="review-detail">
						  		<h5>Tidak ada pesan.</h5>
						  	</div>
						  	@endif
						  </div>
						  <div role="tabpanel" class="tab-pane" id="outbox">
						  	<h4>Pesan Keluar</h4>
						  	<hr>
						  	@foreach($data['message_outbox'] as $row)
						  	<div class="review-detail">
						  		<div class="review-item">
						  			<div class="review-item-head">
						  				<div class="row">
						  					<div class="col-md-12 text-left review-item-inline" style="color:green">
						  						<span style="font-size:12px">Tujuan: &nbsp;</span><span>{!! $row->recipient()->count()>0?$row->recipient->name:'N.A' !!} </span>
						  						<i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($row->created_at)) !!}  <i class="fa fa-bell-o"></i> {!! date('l', strtotime($row->created_at)) !!} 
						  						<i class="fa fa-calendar-o"></i> {!! date('j F', strtotime($row->created_at)) !!} 
						  					</div>
						  				</div>
						  				
						  			</div>
						  			<div class="review-item-body">
						  				<p>
						  					{!! $row->content !!}
						  				</p>
						  			</div>
						  		</div>
						  	</div>
						  	@endforeach
						  	@if($data['message_outbox']->count() == 0)
						  	<div class="review-detail">
						  		<h5>Tidak ada pesan.</h5>
						  	</div>
						  	@endif
						  </div>
						</div>

						<script>
						  $(function () {
						    $('#myTab a:first').tab('show')
						  })
						</script>
					</div>
				</div>
			</div>
<div class="col-md-4" style="max-width:26%;margin-left:20px">
				@include('theme.keiskei.include.user-sidebar')
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="withdrawPoint" tabindex="-1" role="dialog" aria-labelledby="withdrawPoint1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="withdrawPoint1">Withdraw Point</h4>
      </div>
      <div class="modal-body">
        <h4>Maaf, Withdraw hanya dapat dilakukan pada akhir bulan.</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@include('theme.keiskei.partials.best-products')
@include('theme.keiskei.include.password')
@endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>

	<script type="text/javascript">

		//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});

	</script>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(../img/preloader.gif) center no-repeat #fff;
}
</style>
  
<script type="text/javascript">
  function withdrawModal () {
    $('#withdrawPoint').modal({ backdrop: 'static', keyboard: false });
    
  }
</script>
@endsection

@section('custom-head')
	
@endsection