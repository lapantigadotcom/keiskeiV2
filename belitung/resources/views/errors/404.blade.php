@extends('theme.keiskei.app')
 
@section('content')
	<title>KeisKei Indonesia | Opssssss.....Error 404. File/Halaman tidak ditemukan</title>

 

 


 <div class="container text-center">
		<div class="logo-404">
		</div>
		<div class="content-404"><i class="fa fa-exclamation-triangle fa-5x text-danger" ></i> <h1><b>Opssssss.....!</b> Error 404. File/Halaman tidak ditemukan</h1>
			<p>Silahkan kembali ke halaman sebelumnya.</p>
 		</div>
	</div>

<br><br>

 @endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
 

@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection