@extends('theme.keiskei.app')
 
@section('content')

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center" style="padding-top:20%;border-radius:0;height:600px;background:url(./theme/keiskei/img/bg-maintenance.jpg) center 20%">
			<h2>ERROR...500</h2>
			<h5>Maaf...terjadi kesalahan. Silahkan reload kembali.
</h5>

		</div>
	</div>
</div>
<div class="row">

 
 @endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
 

@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection