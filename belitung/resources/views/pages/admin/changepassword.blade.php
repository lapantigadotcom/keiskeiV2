@extends('layouts.app')

@section('content')

            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
			<div class="box">
				<div class="box-header">
                                    <h3 class="box-title">Change Password &nbsp;&nbsp;&nbsp;</h3>
                                </div>
		<div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                        @if(Session::has('errorMessage'))
                        	{!! Session::get('errorMessage') !!}
                        @elseif(Session::has('successMessage'))
                        	{!! Session::get('successMessage') !!}
                        @endif
                        {!! BootstrapForm::openStandard(array('route' => 'ki-admin.postChangePassword', 'method' => 'POST')); !!}
                            <div class="row">
                                <div class="col-md-12">
                               	  {!! BootstrapForm::password('old_password') !!}
                               	  {!! BootstrapForm::password('new_password') !!}
                               	  {!! BootstrapForm::password('retype_password') !!}                               	                                 
                              	  <div class="form-group">
                                      {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
            	                  </div>
                                </div><!-- /.col -->
                            </div> <!-- /.row -->
                        {!! BootstrapForm::close() !!}
                        </div>
                    </div>
                    </div>
                    </div>
                </section>
@endsection