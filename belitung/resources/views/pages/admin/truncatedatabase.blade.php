@extends('layouts.app')

@section('content')
           <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
<div class="box">
<div class="box-header">
                                    <h3 class="box-title">Reset Database &nbsp;&nbsp;&nbsp;</h3>
                                </div>
<div class="box-body table-responsive">
                    <div class="row">
                        <div class="col-xs-12">
                        {!! BootstrapForm::openStandard(array('route' => 'ki-admin.postResetDatabase', 'method' => 'POST', 'id' => 'resetDatabaseForm')); !!}
                            <div class="row">
                                <div class="col-md-12">
                                                <div class="box-body">
                                                    <h4>Choose table :</h4>
                                                    <div class="form-group">
                                                        {!! Form::checkbox('order', true, false) !!}
                                                        {!! Form::label('order','Order', array('style' => 'color:red')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::checkbox('detail_order',true, false) !!}
                                                        {!! Form::label('detail_order','Detail Order', array('style' => 'color:red')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::checkbox('detail_attribute_product_order',true, false) !!}
                                                        {!! Form::label('detail_attribute_product_order','Detail Attribute Product Order', array('style' => 'color:red')) !!}
                                                    </div>  
                                                    <div class="form-group">
                                                        {!! Form::checkbox('withdraw', true, false) !!}
                                                        {!! Form::label('withdraw','Withdraw', array('style' => 'color:red')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::checkbox('transfer',true, false) !!}
                                                        {!! Form::label('transfer','Detail Order', array('style' => 'color:red')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::checkbox('registration_fee',true, false) !!}
                                                        {!! Form::label('registration_fee','Registration Fee', array('style' => 'color:red')) !!}
                                                    </div>  
                                                    <div class="form-group">
                                                        {!! Form::checkbox('log_activity',true, false) !!}
                                                        {!! Form::label('log_activity','Log Activity', array('style' => 'color:red')) !!}
                                                    </div>   
                                                    <div class="form-group">
                                                        {!! Form::checkbox('notification_email',true, false) !!}
                                                        {!! Form::label('notification_email','Notification Email', array('style' => 'color:red')) !!}
                                                    </div>    
                                                    <div class="form-group">
                                                        {!! Form::checkbox('notification_sms',true, false) !!}
                                                        {!! Form::label('notification_sms','Notification SMS', array('style' => 'color:red')) !!}
                                                    </div>  
                                                    <div class="form-group">
                                                        {!! Form::checkbox('notification_message',true, false) !!}
                                                        {!! Form::label('notification_message','Notification Message', array('style' => 'color:red')) !!}
                                                    </div>                                            
                                                    <div class="form-group">
                                                        <a class="btn btn-danger" onclick="deleteModal(this)">Reset data</a>
                                                    </div>
                                      	      </div>
                                      	 </div>
                                    </div>
                        {!! BootstrapForm::close(); !!}
           		</div>                                               
                      </div>                                         
                      </div>
                                            </div>
                      </section>
@endsection

@section('custom-footer')
  <div class="modal modal-default" id="delete-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Are you sure want to reset data in database ? All data on checked table will be erase !</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" id="delete-modal-confirm">Reset database now !</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  
  <div class="modal modal-default" id="success-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Reset database success !</h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<script type="text/javascript">
  @if(Session::has('success'))
  	successModal();
  @endif
  function successModal () {
    $('#success-modal').modal({ backdrop: 'static', keyboard: false });
  }
  function deleteModal (x) {
    $('#delete-modal').modal({ backdrop: 'static', keyboard: false })
    .one('click', '#delete-modal-confirm', function (e) {
      $('#resetDatabaseForm').submit();
    });
  }
</script>
@endsection