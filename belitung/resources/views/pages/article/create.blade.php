@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        {!! Form::open(array('route' => 'ki-admin.article.store', 'method' => 'POST')) !!}
                            <?php 
                                $arrCategory = array();
                                $arrCategory[0] = 'Tidak ada';
                                foreach ($data['categoryarticle'] as $row) {
                                    $arrCategory[$row->id] = $row->title;
                                }
                            ?>
                            @include('pages.article.form',array('submit' => 'Simpan','arrCategory' => $arrCategory))
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    <script type="text/javascript" src="{!! asset('plugins/tinymce/tinymce.min.js') !!}"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: "#description"
     ,
        theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ]
});
</script>

@endsection

@section('custom-footer')
    
@endsection