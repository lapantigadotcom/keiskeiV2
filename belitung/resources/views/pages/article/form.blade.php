<div class="box-body">
	<div class="form-group">
		{!! Form::label('title','Title') !!}
		{!! Form::text('title',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('description','Description') !!}
		{!! Form::textarea('description',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('ms_category_article_id','Category Article') !!}
		{!! Form::select('ms_category_article_id',$arrCategory,null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('meta_description','Meta Description') !!}
		{!! Form::textarea('meta_description',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('meta_keyword','Meta Keyword') !!}
		{!! Form::textarea('meta_keyword',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>