<div class="box-body">
	<div class="form-group">
		{!! Form::label('name','Name') !!}
		{!! Form::text('name',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('ms_type_attribute_id','Type Attribute') !!}
		{!! Form::select('ms_type_attribute_id', $typeAttribute,null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>