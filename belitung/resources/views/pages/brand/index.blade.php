@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Brand Lists &nbsp;&nbsp;&nbsp;
                                    @if(Auth::user()->hasAccess('ki-admin.brand.create'))
                                        <a href="{!! route('ki-admin.brand.create') !!}" class="btn btn-primary">Tambah Data </a>
                                    @endif
                                    </h3>                                    
                                </div>
                                <div class="box-body table-responsive">
                                    @include('errors.session')
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{ $row->name }}</td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.brand.edit'))
                                                    <a href="{!! route('ki-admin.brand.edit',[$row->id]) !!}" class="fa fa-pencil-square-o"></a>
                                                @endif
                                                @if(Auth::user()->hasAccess('ki-admin.brand.delete'))
                                                    <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.brand.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                                                @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            @include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection