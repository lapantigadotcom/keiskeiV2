<div class="box-body">
	<div class="form-group">
		{!! Form::label('title','Title') !!}
		{!! Form::text('title',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('description','Description') !!}
		{!! Form::textarea('description',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>