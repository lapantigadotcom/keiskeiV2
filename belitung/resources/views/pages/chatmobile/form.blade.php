
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('description', 'Description') !!}
				{!! Form::text('description', null, array('class' => 'form-control', 'placeholder' => 'Reply ...')) !!}
				{!! Form::hidden('ms_gcm_id', $data['gcm_id']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('photo', 'Image') !!}
				{!! Form::file('photo') !!}
			</div>
			
		</div><!-- /.box-body -->
		<div class="box-footer">
			<div class="pull-right">
				<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
			</div>
			<a href="{!! route('ki-admin.chatmobile.index') !!}" class="btn btn-default"><i class="fa fa-times"></i> Discard</a>
		</div><!-- /.box-footer -->