@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Chat Mobile
            <small>Mobile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>
              @if(Auth::user()->hasAccess('ki-admin.chatmobile.index'))
              &nbsp;&nbsp;&nbsp;<a href="{!! route('ki-admin.chatmobile.index') !!}" class="btn btn-primary btn-sm">Refresh</a>
              @endif
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Timestamps</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                      <td>{!! $row->is_admin == '1'?$row->admin->name:$row->name !!}</td>
                      <td><a href="{!! route('ki-admin.chatmobile.show', [$row->ms_gcm_id]) !!}">
                      @if(in_array($row->ms_gcm_id, $data['list_unread']))
                      <b>  {!! substr($row->description,0,80)."..." !!}</b>
                      @else
                      {!! substr($row->description,0,80)."..." !!}
                      @endif
                      </a></td>
                      <td>{!! $row->created_at !!}</td>
                      <td>
                          <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.chatmobile.delete',[$row->ms_gcm_id]) !!}" class="fa fa-trash-o"></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Timestamps</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection