@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Contact Us
            <small>Message</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>Subject : {!! $data['content']->subject !!}</h3>
                    <h5>Name: {!! $data['content']->name !!}</h5>
                  <h5>Email: {!! $data['content']->email !!} <span class="mailbox-read-time pull-right">{!! $data['content']->created_at !!}</span></h5>
                  </div><!-- /.mailbox-read-info -->
                  <div class="mailbox-read-message">
                    <h4>Content :</h4>
                    {!! $data['content']->message !!}
                  </div><!-- /.mailbox-read-message -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                <a class="btn btn-primary" href="{!! URL::previous() !!}">Back</a>
                </div><!-- /.box-footer -->
                <div class="box-footer">
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
            </div><!-- /.col -->
    </div>
</section>
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection