@extends('layouts.app')

@section('content')

 
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Keiskei panel </h1> </small>
                      <marquee>  <h3 style="color:red">ATT: Wajib <bold>SIGN OUT </bold>Saat <bold>tidak di depan dekstop</bold>
                     </h3></marquee>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                
          

                <!-- Tabel TOP LIST -->

                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">5 TOP LIST Member Keiskei | Berdasarkan Total Transaksi</h3>
                                    <div class="box-tools">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover">
                                        <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nama</th>
                                            <th>Tanggal Aktif</th>
                                            <th>Tipe</th>
                                            <th>Total Harga</th>
                                            <th>Total PPN 10%</th>

                                        </tr>
                                        @foreach($data['top_user_transaction'] as $row)
                                    <tr>
                                            <td>{!! $row->user_id !!}</td>
                                            <td>{!! $row->user_name !!}</td>
                                            <td>{!! $row->tanggal_aktif !!}</td>
                                            <td><span class="label label-success">{!! $row->role_name !!}</span></td>
                                            <td>Rp. {!! $row->total_biaya !!},-</td>
                                            <td>Rp. {!! $row->total_biaya*10/100 !!},-</td>

                                        </tr>
                                        @endforeach
                                    </tbody></table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                <!-- /Tabel TOP LIST -->

           


<!-- kolom report 1 | report pembelian--->


                    <!-- Small boxes (Stat box) -->
                                   
                                
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="info-box bg-red">
                              <span class="info-box-icon"><i class="fa fa-barcode"></i></span>
                              <div class="info-box-content">
                                <span class="info-box-text">Items</span>
                                <span class="info-box-number">{!! $data['sale_total'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['sale_total'] !!}%"></div>
                                </div>
                                <span class="progress-description">
                                                                    TOTAL Pembelian
                                </span>
                              </div><!-- /.info-box-content -->



                                  </div><!-- /.info-box -->
                                            </div><!-- ./col -->
                                            <div class="col-lg-3 col-xs-6">
                                           <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-barcode"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Items</span>
                                      <span class="info-box-number">{!! $data['sale_today'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['sale_today'] !!}%"></div>
                                </div>
                                <span class="progress-description">
                            Pembelian Hari Ini
                                </span>
                              </div> </div>


                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                                      <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-barcode"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Items</span>
                                      <span class="info-box-number">{!! $data['sale_week'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width:{!! $data['sale_week'] !!}%"></div>
                                </div>
                                <span class="progress-description">
                            Pembelian Minggu Ini
                                </span>
                              </div></div>
                        </div><!-- ./col -->


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                                 <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-barcode"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Items</span>
                                      <span class="info-box-number">{!! $data['sale_month'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width:{!! $data['sale_month'] !!}%"></div>
                                </div>
                                <span class="progress-description">
                            Pembelian Minggu Ini
                                </span>
                              </div></div>
                        </div><!-- ./col -->


<!-- kolom report 2 | report BONUS--->




<!-- kolom report 3 | Member report--->


<div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="fa fa-user"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Member KEISKEI</span>
                                      <span class="info-box-number"> {!! $data['member_active'] !!}  </span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              TOTAL MEMBER AKTIF </span>
                              </div></div>


                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                                  <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="ion ion-person-add"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Member Baru</span>
                                      <span class="info-box-number"> {!! $data['member_today'] !!}  </span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_today'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Hari Ini</span>
                              </div></div>
                        </div><!-- ./col -->


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                                 <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="ion ion-person-add"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Member Baru</span>
                                      <span class="info-box-number"> {!! $data['member_week'] !!}  </span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_week'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Minggu Ini</span>
                              </div></div>
                        </div><!-- ./col -->


                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                           <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="ion ion-person-add"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Member Baru</span>
                                      <span class="info-box-number"> {!! $data['member_month'] !!}  </span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_month'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                               Bulan Ini</span>
                              </div></div>
                        </div><!-- ./col -->



                    <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-orange">
                                <span class="info-box-icon"><i class="fa fa-gift"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Perwakilan</span>
                                      <span class="info-box-number" style="font-size:16px">{!! $data['top_member_bonus'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Bonus Transaksi</span>
                              </div></div></div>

  <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-orange">
                                <span class="info-box-icon"><i class="fa fa-plus"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">TOTAL Bonus</span>
                                      <span class="info-box-number">Rp. &nbsp;{!! $data['total_bonus'] !!} </span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Bonus Transaksi</span>
                              </div></div></div>

<div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-orange">
                                <span class="info-box-icon"><i class="fa fa-minus-square"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">Bonus KREDIT</span>
                                      <span class="info-box-number">Rp. &nbsp;{!! $data['total_bonus_kredit'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Bonus Transaksi</span>
                              </div></div></div>


                              <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-orange">
                                <span class="info-box-icon"><i class="fa fa-check-square"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text">BONUS DIBAYARAKAN</span>
                                      <span class="info-box-number">Rp. &nbsp;{!! $data['total_bonus'] !!}</span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Bonus Transaksi</span>
                              </div></div></div>

                                  <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-blue">
                                <span class="info-box-icon"><i class="fa fa-spinner"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text"></span>
                                      <span class="info-box-number"><a href="{!! route('ki-admin.refresh-bonus') !!}" class="btn btn-default btn-block">SUBMIT</a></span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Refresh Bonus MEMBER</span>
                              </div></div></div>

           <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-green">
                                <span class="info-box-icon"><i class="fa fa-comment"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text"></span>
                                      <span class="info-box-number"><a href="{!! route('ki-admin.notif-sms.create') !!}" class="btn btn-default btn-block">Compose</a></span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                             KIRIM SMS</span>
                              </div></div></div>
           <div class="col-lg-4 col-xs-6">
                            <!-- small box -->
                              <!-- small box -->
                            <div class="info-box bg-default">
                                <span class="info-box-icon"><i class="fa fa-table"></i></span>
                              <div class="info-box-content">
                                      <span class="info-box-text"></span>
                                      <span class="info-box-number"><a href="{!! route('ki-admin.notif-message.customer.create') !!}" class="btn btn-default btn-block">Tulis</a></span>
                                <div class="progress">
                                  <div class="progress-bar" style="width: {!! $data['member_active'] !!}  %"></div>
                                </div>
                                <span class="progress-description">
                              Kirim MEMBER MEMO</span>
                              </div></div></div>

                    </div><!-- /.row -->


                       





                     
                </section><!-- /.content -->

@endsection


@section('custom-head')

@endsection

@section('custom-footer')

@endsection