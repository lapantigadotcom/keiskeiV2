@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @if(isset($data['content']))
                        {!! Form::model($data['content'],array('route' => ['ki-admin.general.update',$data['content']->id], 'method' => 'PUT','files' => 'true')); !!}
                        @else
                        {!! BootstrapForm::openStandard(array('route' => 'ki-admin.general.store', 'method' => 'POST')); !!}
                        @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                                            <li><a href="#tab_2" data-toggle="tab">Logo</a></li>
                                            <li><a href="#tab_3" data-toggle="tab">Token Admin</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">
                                                <div class="box-body">
                                                        {!! BootstrapForm::text('site_title') !!}
                                                        {!! BootstrapForm::text('news_roll') !!}
                                                        {!! BootstrapForm::text('site_tagline') !!}
                                                        {!! BootstrapForm::textarea('site_description','Site Description',null,array('class' => 'wysihtml5')) !!}
                                                        {!! BootstrapForm::text('email') !!}
                                                        {!! BootstrapForm::text('telephone') !!}
                                                        {!! BootstrapForm::text('mobile') !!}
                                                    <div class="form-group">
                                                        {!! Form::label('ms_city_id','City') !!}
                                                        {!! Form::select('ms_city_id',$data['city'],null, array('class' => 'form-control')) !!}
                                                        <span class="help-block">{{ $errors->first('ms_city_id') }}</span>
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('ms_country_id','Country') !!}
                                                        {!! Form::select('ms_country_id',$data['country'],null, array('class' => 'form-control')) !!}
                                                        <span class="help-block">{{ $errors->first('ms_country_id') }}</span>
                                                    </div>
                                                        {!! BootstrapForm::textarea('address','Address',null,array('class' => 'wysihtml5')) !!}

                                                    <div class="form-group">
                                                        {!! Form::label('active','Active') !!}
                                                        {!! Form::select('active',[ '0' => 'No', '1' => 'Yes'],null, array('class' => 'form-control')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                                    </div>
                                                </div>
                                            </div><!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <div class="box-body">
                                                        @if(File::exists('data/general/'.$data['content']->logo))
                                                    <div class="col-md-3">
                                                        <img src="{!! asset('data/general/'.$data['content']->logo) !!}" class="img-responsive img-thumbnail">
                                                    </div>
                                                        
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::label('logo','Logo') !!}
                                                        {!! Form::file('logo',array('class' => 'form-control')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                                    </div>
                                                </div>
                                            <div class="tab-pane" id="tab_3">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        {!! Form::label('token_admin','Token Admin') !!}
                                                        {!! Form::text('token_admin',null,array('class' => 'form-control')) !!}
                                                    </div>
                                                    <div class="form-group">
                                                        {!! Form::submit('Simpan', array('class' => 'btn btn-primary')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div><!-- /.tab-content -->
                                    </div><!-- nav-tabs-custom -->
                                </div><!-- /.col -->
                            </div> <!-- /.row -->
                        {!! BootstrapForm::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    {!! HTML::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
    <script type="text/javascript">
      $(function () {
        $(".wysihtml5").wysihtml5();
      });
    </script>
@endsection