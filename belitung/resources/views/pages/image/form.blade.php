<div class="box-body">
	<h3>Add image</h3>
	<div class="form-group">
		{!! Form::label('caption', 'Caption') !!}
		{!! Form::text('caption', null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('description', 'Description') !!}
		{!! Form::textarea('description', null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('new-image', 'Choose file') !!}
		{!! Form::file('new-image') !!}
	</div>
	<div class="form-group">
		{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
	</div>
</div>