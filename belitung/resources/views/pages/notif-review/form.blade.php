@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @if($data['content']->point()->count() > 0)
                        {!! Form::model($data['content']->point,array('route' => ['ki-admin.registrationfee.update',$data['content']->point->id], 'method' => 'PUT')) !!}
                        @else
                        {!! Form::open(array('route' => 'ki-admin.registrationfee.store', 'method' => 'POST')) !!}
                        @endif
                            <div class="box-body">
                                <div class="form-group">
                                    {!! Form::label('role','Role') !!}
                                    {!! Form::text('role',$data['content']->name,array('class'=>'form-control', 'disabled' => 'true')) !!}
                                </div>
                                {!! Form::hidden('ms_role_id',$data['content']->id) !!}
                                <div class="form-group {!! $errors->any()?($errors->first('registrationfee')?' has-error':''):'' !!}">
                                    {!! Form::label('registrationfee','Registration Fee') !!}
                                    <div class="input-group">
                                        <span class="input-group-addon">Rp.</span>
                                        {!! Form::text('registrationfee',null,array('class'=>'form-control')) !!}
                                    </div>
                                    <span class="help-block">{!! $errors->any()?($errors->first('registrationfee')?$errors->first('registrationfee'):''):'' !!}</span>
                                </div>
                                
                               
                                <div class="form-group">
                                {!! BootstrapForm::submit('Simpan') !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection