@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Notification
    <small>Mobile</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-12" >
      {!! Form::open(array('route' => 'ki-admin.notification.index', 'method' => 'POST', 'files' => true)) !!}
      @include('pages.notification.form',array('submit' => 'Send'))
      {!! Form::close() !!}
    </div>
  </div>
</section>

@endsection


@section('custom-head')
{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
<script type="text/javascript">
  $(document).ready(function() {
   $('#receiver').selectize();
   var changeReceiverSelectFunc = (function changeReceiverSelect(){

      var tipe = $('#group').val();
      changeReceiver(tipe);

      return changeReceiverSelect; //return the function itself to reference

  }()); 
   $('#group').change(function() {
    changeReceiverSelectFunc();
  });
 });
  $(function () {
    var tipe = $('#group').val();
    changeReceiver(tipe);
  });
  function changeReceiver(x)
  {
    $.get("{!! route('ki-admin.notif-email.getReceiverJson') !!}", {tipe : x}, function(data, status) {
      if(status == 'success') {
        var content = [];
        var temp = {'text' : "Semua", 'value' : 0};
        content.push(temp);
        $.each(data, function(key, value) {
          var temp = {'text' : value.name, 'value' : value.id};
          content.push(temp);
        });
        console.log(content);
        var selectize = $("#receiver")[0].selectize;
        selectize.clear();
        selectize.clearOptions();
        selectize.load(function(callback) {
          callback(content);
        });
      }
    });
  };
</script>
@endsection