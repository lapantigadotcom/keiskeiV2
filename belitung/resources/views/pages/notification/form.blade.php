<div class="col-md-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Compose New Message</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<div class="form-group">
				{!! Form::label('group', 'Tipe') !!}
				{!! Form::select('group', $data['group'], null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('receiver', 'To') !!}
				{!! Form::select('receiver', $data['receiver'], null, array('class' => 'form-control')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('title', 'Title') !!}
				{!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Subject:')) !!}
			</div>
			<div class="form-group">
				{!! Form::label('photo', 'Image') !!}
				{!! Form::file('photo') !!}
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Description') !!}
				{!! Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Description ...')) !!}
			</div>
		</div><!-- /.box-body -->
		<div class="box-footer">
			<div class="pull-right">
				<button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
			</div>
			<a href="{!! route('ki-admin.notification.index') !!}" class="btn btn-default"><i class="fa fa-times"></i> Discard</a>
		</div><!-- /.box-footer -->
	</div><!-- /. box -->
</div><!-- /.col -->