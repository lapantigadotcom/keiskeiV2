@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Notification
            <small>Mobile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
	<div class="row">
        <div class="col-md-3">
	<a href="{!! route('ki-admin.notification.create') !!}" class="btn btn-primary btn-block margin-bottom">Compose New Notification</a>
        </div>
        </div>
    <div class="row">
        <div class="col-md-12">
        

          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Sent</h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Title</th>
                    <th>To</th>
                    <th>Tipe</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                      <td></td>
                      <td><a href="{!! route('ki-admin.notification.show', [$row->id]) !!}">{!! $row->title !!}</a></td>
                      <td>{!! $row->ms_role_id == '0' ? 'Semua' : $row->recipient->name !!}</td>
                      <td>{!! $row->group == '0' ? 'Individual' : 'Group' !!}</td>
                      <td>{!! $row->created_at !!}</td>
                      <td>
                          <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.notification.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>&nbsp;</th>
                    <th>Subject</th>
                    <th>To</th>
                    <th>Tipe</th>
                    <th>Time</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection