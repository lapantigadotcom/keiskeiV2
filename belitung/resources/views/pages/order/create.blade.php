@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        {!! Form::open(array('route' => 'ki-admin.brand.store', 'method' => 'POST')) !!}
                            @include('pages.brand.form',array('submit' => 'Simpan'))
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection