@extends('layouts.app')

@section('content')
@include('theme.keiskei.scripts.price-product')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Penjualan | Proses Order Status</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        @include('errors.session')
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Harap Cek sebelum merubah status | Setiap Perubahan Status Transaksi Akan terkonfirmasi via SMS dan Email&nbsp;&nbsp;&nbsp;

                                    @if(Auth::user()->hasAccess('ki-admin.order.create'))
                                        <a href="{!! route('ki-admin.order.create') !!}" class="btn btn-primary">Tambah Data </a>
                                    @endif
                                    </h3>                                    
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Kode INV</th>
                                                <th>Surat Jalan</th>
                                                <th>TGL</th>
                                                <th>Kustomer</th>
                                                <th>Keterangan</th>
                                                <th>Total</th>
                                               
                                                <th style="width:143px">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            
                                            <tr>

                                                <td>
                                                <a href="{!! route('ki-admin.order.show',[$row->id]) !!}" target="_blank"   data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Order">
                                                    {!! $row->code !!}
                                                </a>
                                                </td>
                                               <td><a href="{!! route('ki-admin.order.show',[$row->id]) !!}" target="_blank"   data-toggle="tooltip" data-placement="bottom" title="Cetak Lembar Pengiriman">
                                                <button class="btn btn-warning" type="button"><i class="fa fa-truck"></i>&nbsp;|&nbsp;Cetak</button></a></td>
                                                 <td>
                                                
                                                    {!! $row->created_at !!}
                                                
                                                </td>
                                                <td>
                                                
                                                
                                                @if(Auth::user()->hasAccess('ki-admin.user.show'))<a href="{!! route('ki-admin.user.show',[$row->user->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail User">{{ $row->user->username }}</a>@else {{ $row->user->username }} @endif 
                                                </td>
                                                <td> 

                                                  

                                                    <i class="fa fa-shopping-cart"></i>  <b>{!! $row->statusOrder->name !!}</b>
                                                    @if($row->ms_status_order_id=='8')
                                                        ke : 
                                                        @if(Auth::user()->hasAccess('ki-admin.user.show'))
                                                        @if($row->reseller()->count())
                                                        	<a href="{!! route('ki-admin.user.show',[$row->reseller->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail User">{{ $row->reseller->username }}</a>
                                                        @endif
                                                        @else 
                                                        	{{ $row->reseller->username }} 
                                                        @endif
                                                    @endif
                                                    </b></small>
                                                    @foreach($row->transfers as $val)
                                                    <p>
                                                        <em>Konfirmasi pembayaran telah dilakukan: 
                                                            <em style="color:red">
                                                                 Rp.{!! $val->nominal !!}  
                                                            </em> 
                                                              <em style="color:green"><br>TGL:{!! date('j F Y',strtotime($val->transfer_date)) !!}</em>
                                                    </p>
                                                    @endforeach
                                                </td>
                                                <td class="text-right">
                                                @if($row->user()->count() > 0)
                                                <?php 
                                                    $total = 0; ?>
                                                    @if($row->detailOrders()->count() > 0)
                                                    @foreach($row->detailOrders as $val)
                                                    @if($val->product()->count() > 0)
                                                    @if($val->product->prices()->count() > 0)
                                                    <?php
                                                    // $total_t = 0;
                                                    // dd($row->user, $val->product, $val->product->prices);
                                                    $total_t = priceDisplayAdmin($row->user, $data['default_role'], $val->product->prices); 
                                                    if(!empty($val->product->discount))
                                                    {
                                                        $discount_t = ($total_t/100)*intval($val->product->discount);
                                                        $total_t = $total_t - $discount_t;
                                                    }
                                                    $total_t = $total_t*$val->total;
                                                    $total += $total_t;
                                                    ?>
                                                    @endif
                                                    @endif
                                                    @endforeach
                                                    @endif
                                                    <?php 
                                                    $total = $total + $row->shipping_price + ($total*10/100) ;
                                                    ?>
                                                {!! money_format('%(#10n', $total) !!}
                                                @else
                                                N.A.
                                                @endif
                                                </td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.order.change') || Auth::user()->hasAccess('ki-admin.order.delete'))
                                                  <div class="btn-group">
                                                    <button class="btn btn-danger" type="button">Ubah</button>
                                                    <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">
                                                      <span class="caret"></span>
                                                      <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul role="menu" class="dropdown-menu">
                                                    @if(Auth::user()->hasAccess('ki-admin.order.change'))
                                                      @foreach($data['statusorder'] as $key => $value)
                                                      <li><a href="{!! route('ki-admin.order.change',[$row->id]).'?state='.$key !!}">{{ $value }}</a></li>
                                                      @endforeach
                                                    @endif
                                                      <li class="divider"></li>
                                                      @if(Auth::user()->hasAccess('ki-admin.order.delete'))
                                                      <li><a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.order.delete',[$row->id]) !!}">Delete</a></li>
                                                      @endif
                                                    </ul>
                                                  </div>
                                                @endif
                                                </td>

                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Kode Invoice</th>    
                                                <th>Surat Jalan</th>
                                                <th>TGL</th>
                                                <th>Kustomer</th>
                                                <th>Keterangan</th>
                                                <th>Total</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            @include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable();
            });
    </script>
@endsection