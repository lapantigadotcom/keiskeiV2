@extends('layouts.app')

@section('content')
@include('theme.keiskei.scripts.price-product')
<section class="content-header">
        <h1>
            Order
            <small>Detail</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              @include('errors.session')
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Show Detail</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">

                  <div class="mailbox-read-info">
                    <h3>Username : {!! $data['content']->user->name !!}</h3>
                    <h4>Tipe User : {!! $data['content']->user->roles->first()->name !!} </h4>
                    <h5>Status : {!! $data['content']->user->approved=='1'?"<span class='label label-primary'>Approved</span>":"<span class='label label-danger'>Unnaproved</span>" !!} </h5>

                  </div><!-- /.mailbox-read-info -->
                  <div class="mailbox-read-message">
                    <div class="col-md-12">
                      <h4>Order Detail :</h4>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        Kode Invoice : <b> {!! $data['content']->code !!} </b><br>
                        Status : <b> {!! $data['content']->statusOrder->name !!} </b>
                      </div>
                      <div class="col-md-6">
                        <h5>
                          <strong>Alamat Tujuan:</strong>
                        </h5>
                        <p>
                          <b>{!! $data['content']->user->name !!}</b>
                          <br>
                          {!! $data['content']->user->address !!}<br>
                          {!! $data['content']->user->city->name !!}, {!! $data['content']->user->city->province->name !!}<br>
                          Telp. {!! $data['content']->user->telephone !!}
                        </p>
                      </div>
                    </div>
                    <div class="col-md-12">
                    <hr>
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Berat</th>
                            <th>Harga</th>
                            <th>Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          $total = 0;
                          $weight = 0;
                           ?>
                          @foreach($data['content']->detailOrders as $row)
                          <tr>
                            <td>
                              <h5>{!! $row->product->title !!}<h5>
                              @if($row->detailAttributeProductDetailOrders->count() > 0)
                              @foreach($row->detailAttributeProductDetailOrders as $val)
                              <span>
                              <b>{!! $val->detailAttributeProduct->detailAttribute->attribute->name !!}</b>
                                : 
                                <b>{!! $val->detailAttributeProduct->detailAttribute->name !!}</b>
                              </span>
                              @endforeach
                              @endif
                            </td>
                            <td>{!! $row->total !!}</td>
                            <td>{!! $row->product->weight !!} kg</td>
                            <td>
                              <?php 
                              $weight += $row->total*$row->product->weight;
                              if(Auth::user()->roles->first()->locked != '1')
                              $total_t = priceDisplay($data['default_role'], $row->product->prices); 
                              else
                              $total_t = priceDisplayAdmin($data['content']->user, $data['default_role'], $row->product->prices); 
                              ?>
                              <span>
                                {!! money_format('%(#10n', $total_t) !!}
                              </span>
                              <br>
                              <span class="small">
                                <del><?php $def = 0; if(Auth::user()->roles->first()->locked != '1') 
                                $def= defaultProduct($data['default_role'], $row->product->prices);
                                else
                                $def = defaultProductAdmin($data['content']->user, $data['default_role'], $row->product->prices);
                                ?>
                                {!! money_format('%(#10n', $def) !!}
                                </del>
                              </span>
                             <span style="color:red"> <?php 
                                if(!empty($row->product->discount))
                                {
                                  $discount_t = ($total_t/100)*intval($row->product->discount);
                                  echo "<br><b>Diskon ".$row->product->discount." %</b>";
                                  $total_t = $total_t - $discount_t;
                                }
                                $total_t = $total_t*$row->total;
                                $total += $total_t;
                              ?></span>
                            </td>
                            <td class="text-right">
                              {!! money_format('%(#10n', $total_t) !!}
                            </td>
                          </tr>
                          @endforeach
                          @if($data['content']->user->roles->first()->level!='5')
                          <tr>
                  

                   
                  <tr style="width:100%;text-align:right;background:#f4f4f4;color:green">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>

                    <td><h4><b>
                                      PPN (10%): {!! money_format('%(#10n', $total*10/100) !!}</b><h4>
                    
                  </td>
                </tr>



                          <tr>
                            <td colspan="2">
                              <h5>
                                {!! $data['content']->courier->name !!} -
                                {!! $data['content']->service_courier !!}
                              </h5>
                            </td>
                            <td>
                              {!! $weight !!} kg
                            </td>
                            <td colspan="2" class="text-right">
                              {!! money_format('%(#10n', $data['content']->shipping_price) !!}
                            </td>
                          </tr>
                          @endif
                          <tr>
                          <td colspan="4">
                            <h4>Total Pembayaran</h4>
                          </td>
                          <td class="text-right">
                            <h4>
                      {!! money_format('%(#10n', $total+intval($data['content']->shipping_price)+ (($total))*10/100)  !!}
                            </h4>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-md-12">
                      <h4>Daftar Kontirmasi Transfer</h4>
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Username</th>
                            <th>Nominal</th>
                            <th>No.Rek Tujuan</th>
                            <th>Keterangan</th>
                          </tr>
                        </thead>
                        @if($data['content']->transfers->count() > 0)
                          @foreach($data['content']->transfers as $row)
                            <tr>
                                <td>
                                    <a href="{!! route('ki-admin.transfer.show', [$row->id]) !!}">
                                        {{ $row->user->name }}
                                    </a>
                                </td>
                                <td>
                                  {!! money_format('%(#10n', $row->nominal) !!}
                                </td>
                                <td>
                                @if($row->payment()->count() > 0)
                                {!! $row->payment->name.' '.$row->payment->account_number.' - a/n '.$row->payment->account_name !!}
                                @else
                                N.A.
                                @endif
                                </td>
                                <td>
                                <b>{{ $row->account_name }}</b>
                                <br>
                                {{ $row->account_number }}
                                <br>
                                {!! date('j F Y', strtotime($row->transfer_date)) !!}
                                <br>
                                {!! !empty($row->status)?($row->status==1?"<span class='label label-primary'>Approved</span>":"<span class='label label-default'>Disapproved</span>"):"<span class='label label-warning'>Pending</span>" !!}
                                </td>
                            </tr>
                          @endforeach
                        @else
                          <tr>
                            <td colspan="4">
                              Data transfer kosong
                            </td>
                          </tr>
                        @endif
                      </table>
                    </div>
                  </div><!-- /.mailbox-read-message -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                  <div class="col-md-12">
                    @if(Auth::user()->hasAccess('ki-admin.order.change') || Auth::user()->hasAccess('ki-admin.order.delete'))
                      <div class="btn-group">
                        <button class="btn btn-danger" type="button">Change</button>
                        <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">
                          <span class="caret"></span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul role="menu" class="dropdown-menu">
                        @if(Auth::user()->hasAccess('ki-admin.order.change'))
                          @foreach($data['statusorder'] as $key => $value)
                          <li><a href="{!! route('ki-admin.order.change',[$data['content']->id]).'?state='.$key !!}">{{ $value }}</a></li>
                          @endforeach
                        @endif
                          <li class="divider"></li>
                          @if(Auth::user()->hasAccess('ki-admin.order.delete'))
                          <li><a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.order.delete',[$data['content']->id]) !!}">Delete</a></li>
                          @endif
                        </ul>
                      </div>
                    @endif
                    <a href="{!! route('user.invoice',[$data['content']->id]) !!}?invoice={!! $data['content']->code !!}" target="_blank" class="btn btn-primary pull-right" style="margin-right: 5px;">
                      <i class="fa fa-download"></i>
                      Generate Invoice
                    </a>
                    <a href="{!! route('user.jalan',[$data['content']->id]) !!}?jalan={!! $data['content']->code !!}" target="_blank" class="btn btn-warning pull-right" style="margin-right: 5px;">
                      <i class="fa fa-truck"></i>
                      Generate Surat Jalan
                    </a>
                  </div>
                </div><!-- /.box-footer -->
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

                <div class="box-footer">
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
            </div><!-- /.col -->
    </div>
</section>
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection