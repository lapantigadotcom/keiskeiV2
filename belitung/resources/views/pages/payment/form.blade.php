@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @if(isset($data['content']))
                        {!! Form::model($data['content'],array('route' => ['ki-admin.payment.update',$data['content']->id], 'method' => 'PUT')) !!}
                        @else
                        {!! Form::open(array('route' => 'ki-admin.payment.store', 'method' => 'POST')) !!}
                        @endif
                            <div class="box-body">
                                {!! BootstrapForm::text('name','Name') !!}
                                {!! BootstrapForm::text('account_name','Account Name') !!}
                                {!! BootstrapForm::text('account_number','Account Number') !!}
                                <div class="form-group">
                                {!! BootstrapForm::submit('Simpan') !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection