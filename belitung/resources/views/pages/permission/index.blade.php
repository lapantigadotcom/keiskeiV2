@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Permission Lists &nbsp;&nbsp;&nbsp;@if(Auth::user()->hasAccess('ki-admin.permission.create'))<a href="{!! route('ki-admin.permission.create') !!}" class="btn btn-primary">Tambah Data </a>@endif</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Route</th>
                                                <th>Role Pengakses</th>
                                                <th>Enabled</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{ $row->route }}</td>
                                                <td>
                                                <?php $tmp = array(); ?>
                                                    @foreach($row->roles as $v)
                                                        <?php array_push($tmp, $v->name);  ?>
                                                    @endforeach
                                                {!! implode(', ', $tmp) !!}
                                                </td>
                                                <td>{{ $row->enabled=='1'?'Ya':'Tidak' }}</td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.permission.edit'))
                                                    <a href="{!! route('ki-admin.permission.edit',[$row->id]) !!}" class="fa fa-pencil-square-o"></a>
                                                @endif
                                                @if(Auth::user()->hasAccess('ki-admin.permission.delete'))
                                                    <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.permission.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                                                @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Route</th>
                                                <th>Role Pengakses</th>
                                                <th>Enabled</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            @include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable();
            });
    </script>
@endsection