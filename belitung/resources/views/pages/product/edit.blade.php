@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
        Dashboard
        <small>EDIT PRODUK</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            @include('errors.session')
            {!! Form::model($data['content'],array('route' => ['ki-admin.product.update',$data['content']->id], 'method' => 'PUT', 'files' => 'true')) !!}
            <?php
                $submit = 'Simpan';
            ?>
            @include('pages.product.form')
            {!! Form::close() !!}
        </div>



    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
@endsection

@section('custom-footer')
    @include('scripts/jquery-number')
@endsection