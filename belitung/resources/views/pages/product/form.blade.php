

<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
				<li><a href="#tab_2" data-toggle="tab">Price</a></li>
				<li><a href="#tab_3" data-toggle="tab">Images</a></li>
				<li><a href="#tab_4" data-toggle="tab">Discount</a></li>
				<li><a href="#tab_5" data-toggle="tab">Attribute</a></li>
				<li><a href="#tab_6" data-toggle="tab">Stock</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<div class="box-body">
						<div class="form-group">
							{!! Form::label('title','Name') !!}
							{!! Form::text('title',null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('description','Deskripsi') !!}
							{!! Form::textarea('description',null, array('id' => 'editor1')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('specification','Indikasi / Kegunaan') !!}
							{!! Form::textarea('specification',null, array('id' => 'editor2')) !!}
						</div>

						<div class="form-group">
							{!! Form::label('bahan','Bahan Produk') !!}
							{!! Form::text('bahan',null, array('class' => 'form-control')) !!}
						</div>

						<div class="form-group">
							{!! Form::label('ms_brand_id','Brand') !!}
							{!! Form::select('ms_brand_id',$data['brand'],null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('ms_category_product_id','Category Product') !!}
							{!! Form::select('ms_category_product_id',$data['categoryProduct'],null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('ms_status_product_id','Status Product') !!}
							{!! Form::select('ms_status_product_id',$data['statusProduct'],null, array('class' => 'form-control')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('length','Length') !!}
							{!! Form::number('length',null, array('class' => 'form-control', 'step' => '0.1')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('weight','Weight') !!}
							{!! Form::number('weight',null, array('class' => 'form-control', 'step' => '0.1')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('help','FAQ Produk') !!}
							{!! Form::textarea('help',null, array('id' => 'editor3')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('files', 'File Brosur Produk') !!}
							@if(isset($data['content']))
							@if($data['content']->files != "" || $data['content']->files != null)
							<p>{!! explode("/", $data['content']->files)[2] !!}</p>
							<div class="form-group">
								<a href="{!! route('ki-admin.product.downloadfile', [$data['content']->id]) !!}" class="btn btn-success">Download File</a><a style="margin-left:20px;" href="{!! route('ki-admin.product.deletefile' , [$data['content']->id]) !!}" class="btn btn-danger"><i class="fa fa-close"></i>Delete file</a>
								</div>
							@endif
							@endif
							{!! Form::file('files'); !!}
						</div>

						<div class="form-group">
<button type="button" onclick="history.back();">Back</button>&nbsp;
|&nbsp;{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
						</div>

					</div>
				</div><!-- /.tab-pane -->
				<div class="tab-pane" id="tab_2">
					@if(isset($data['message']))
					<div class="alert alert-warning" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						{!! $data['message'] !!}
					</div>
					@else
					<div class="row">
						<div class="box-body">
							<div class="col-md-10 col-md-offset-1">
								@foreach($data['currency'] as $currencyRow)
									<div class="col-md-6">
										<h4>{!! $currencyRow->name !!}</h4>
									</div>
								@endforeach
								@foreach($data['role'] as $roleId => $roleName)
									@foreach($data['currency'] as $currencyRow)
									<div class="col-md-6">
										<div class="form-group">
											<?php
												$price = $data['content']->prices->filter(function($row) use($roleId, $currencyRow) {
													if(($row->ms_role_id == $roleId) && ($row->ms_currency_id == $currencyRow->id)) {
														return true;
													}
													else {
														return false;
													}
												})->first();
												if($price==null) $price = 0;
												else $price = $price = $price->price;
											?>
											{!! Form::label('price-'.$roleId.'-'.$currencyRow->id, $roleName) !!}
											<div class="input-group">
												<span class="input-group-addon">{!! $currencyRow->symbol !!}</span>
												{!! Form::text('price-'.$roleId.'-'.$currencyRow->id, $price, array('class' => 'form-control number')) !!}
											</div>
										</div>
									</div>
									@endforeach
								@endforeach
							</div>
						</div>
						<div class="form-group col-md-6">
							<div class="col-md-offset-1">
								{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
							</div>
						</div>
					</div>
					@endif
				</div>
				<div class="tab-pane" id="tab_3">
					@if(isset($data['message']))
					<div class="alert alert-warning" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						{!! $data['message'] !!}
					</div>
					@else
					<a href="{!! route('ki-admin.productimage.create', ['product' => $data['content']->id]) !!}" class="btn btn-primary">Add Image</a>
					<div class="box-body table-responsive">
						<table id="dataTable" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Image</th>
									<th>Caption</th>
									<th>Featured</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['content']->productImages as $row)
								<tr>
									<td>&nbsp;</td>
									<td>
										<img width="55px" height="55px" src="{!! asset($row->image) !!}" />
									</td>
									<td>{!! $row->caption !!}</td>
									<td>{!! $row->featured == 1 ? 'Yes' : 'No' !!}</td>
									<td>
										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.productimage.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>Image</th>
									<th>Caption</th>
									<th>Featured</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
					@endif
				</div>
				<div class="tab-pane" id="tab_4">
					@if(isset($data['message']))
					<div class="alert alert-warning" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						{!! $data['message'] !!}
					</div>
					@else
					<div class="form-group">
						{!! Form::label('discount', 'Discount') !!}
						<div class="input-group">
							{!! Form::number('discount', null, array('class' => 'form-control', 'min' => '0', 'max' => '100')) !!}
							<span class="input-group-addon">%</span>
						</div>
					</div>
					<div class="form-group">
						{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
					</div>
					@endif
				</div>
				<div class="tab-pane" id="tab_5">
					@if(isset($data['message']))
					<div class="alert alert-warning" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						{!! $data['message'] !!}
					</div>
					@else
						@foreach($data['attribute'] as $attribute)
							<div class="form-group">
								{!! Form::label($attribute->name, $attribute->name) !!}
							</div>
							<div class="form-group">
								@foreach($attribute->detailAttributes as $detailAttribute)
										<?php
											$detailAttributeProduct = $data['content']->detailAttributeProducts->filter(function($row) use($detailAttribute) {
												if($row->ms_detail_attribute_id == $detailAttribute->id) {
													return true;
												}
												else {
													return false;
												}
											})->first();
											if($detailAttributeProduct==null || $detailAttributeProduct->enabled=='0') $enabled = false;
											else $enabled = true;
										?>
										{!! Form::checkbox('attribute-'.$attribute->id.'-'.$detailAttribute->id, '1', $enabled) !!}	
										{!! Form::label('attribute-'.$attribute->id.'-'.$detailAttribute->id, $detailAttribute->name) !!}&nbsp;&nbsp;
								@endforeach
							</div>
						@endforeach
						<div class="form-group">
							{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
						</div>
					@endif
				</div>
				<div class="tab-pane" id="tab_6">
					@if(isset($data['message']))
					<div class="alert alert-warning" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						{!! $data['message'] !!}
					</div>
					@else
					<a href="{!! route('ki-admin.stock-product.create', ['product' => $data['content']->id]) !!}" class="btn btn-primary">Tambah data</a>
					<div class="box-body table-responsive">
						<table id="dataTable" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data['content']->stockProducts as $row)
								<tr>
									<td>&nbsp;</td>
									<td>{!! $row->date !!}</td>
									<td>{!! $row->amount !!}</td>
									<td>
										<a href="{!! route('ki-admin.stock-product.edit', [$row->id]) !!}" class="fa fa-pencil-square-o"></a>
										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.stock-product.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<th>&nbsp;</th>
									<th>Date</th>
									<th>Amount</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
					@endif
				</div>
			</div><!-- /.tab-content -->
		</div><!-- nav-tabs-custom -->
	</div><!-- /.col -->
</div> <!-- /.row -->
 <script type="text/javascript">
      CKEDITOR.replace( 'editor1' );
      CKEDITOR.add            
   </script>
    <script type="text/javascript">
      CKEDITOR.replace( 'editor2' );
      CKEDITOR.add            
   </script>
    <script type="text/javascript">
      CKEDITOR.replace( 'editor3' );
      CKEDITOR.add            
   </script>