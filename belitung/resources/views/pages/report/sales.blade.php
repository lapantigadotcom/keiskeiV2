@extends('layouts.app')

@section('content')
<section class="content-header">
  <h1>
    Report
    <small>Total Penjualan</small>
  </h1>
  <ol class="breadcrumb">
  </ol>
</section>
<section class="content">
  <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {!! $data['sale_total'] !!}<sup style="font-size: 20px">Items</sup>
                                    </h3>
                                    <p>
                                        TOTAL Pembelian
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    Transaksi TOTAL <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {!! $data['sale_today'] !!}<sup style="font-size: 20px">Items</sup>
                                    </h3>
                                    <p>
                                        Pembelian Hari ini
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    Transaksi HARIAN <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                     <h3>
                                        {!! $data['sale_week'] !!}<sup style="font-size: 20px">Items</sup>
                                    </h3>
                                    <p>
                                        Pembelian Minggu ini
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                   Transaksi MINGGUAN <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {!! $data['sale_month'] !!}<sup style="font-size: 20px">Items</sup>
                                    </h3>
                                    <p>
                                    <p>
                                        Pembelian Bulan ini
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-bag"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    Transaksi BULANAN  <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        
                                        <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Sales per product &nbsp;&nbsp;&nbsp;</h3>
                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable2" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Product Name</th>
                                                <th>Total Sales</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['sales_per_product'] as $row)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{ $row->title }}</td>
                                                <td>{{ $row->total }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>Product Name</th>
                                                <th>Total Sales</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
</section>
@endsection