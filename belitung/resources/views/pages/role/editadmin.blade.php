@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        {!! Form::model($data['content'],array('route' => ['ki-admin.role.admin.update',$data['content']->id], 'method' => 'PUT')) !!}
                            <div class="box-body">
                                {!! BootstrapForm::text('name') !!}
                            <div class="form-group">
                                {!! Form::label('enabled','Enabled') !!}
                                <div class="checkbox">
                                    <label>                     
                                        @if($data['content']->code=='SP')
                                        {!! Form::checkbox('enabled', '1', null,array('class' => 'flat-red','disabled' => 'true')) !!}
                                        Enabled
                                        @else
                                        {!! BootstrapForm::checkbox('enabled', ' Enabled', '1', null) !!}
                                        @endif
                                        
                                    </label>
                                
                                </div>
                            </div>
                            <div class="row text-center">
                                <h4><b>Admin Menu</b></h4>
                            </div>
                            <?php 
                            $no = 1;
                            $count_t = (intval(count($data['adminmenu'])/2));
                            ?>
                            <div class="row">
                                <div class="col-md-6">                                
                                @foreach($data['adminmenu'] as $key => $value)
                                <div class="form-group">
                                    <label>
                                      <input type="checkbox" name="adminmenu[]" value="{{ $key }}" {{ in_array($key, $data['adminmenu_t'])?'checked':'' }}  class="flat-red"/>
                                    </label>
                                    <label>
                                      {!! $value !!}
                                    </label>
                                  </div>
                                  @if($no++ == $count_t)
                                    </div>
                                    <div class="col-md-6">
                                    @endif
                                @endforeach
                                </div>
                            </div>
                            <div class="row text-center">
                                <h4><b>Permission</b></h4>
                            </div>
                            <?php 
                            $no = 1;
                            $count_t = (intval(count($data['permission_t'])/2));
                            ?>
                            <div class="row">
                                <div class="col-md-6">                                
                                @foreach($data['permission'] as $key => $value)
                                <div class="form-group">
                                    <label>
                                      <input type="checkbox" name="permission[]" value="{{ $key }}" {{ in_array($key, $data['permission_t'])?'checked':'' }}  class="flat-red"/>
                                    </label>
                                    <label>
                                      {!! $value !!}
                                    </label>
                                  </div>
                                  @if($no++ == $count_t)
                                    </div>
                                    <div class="col-md-6">
                                    @endif
                                @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    {!! HTML::style('plugins/iCheck/all.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/iCheck/icheck.min.js') !!}
    <script type="text/javascript">
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
    </script>
@endsection