@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Laporan Pengiriman</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                        @include('errors.session')
                        @if($errors->any())
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <ul class="alert alert-danger text-left" style="padding: 25px 0px 0px 35px;">
                                    @foreach($errors->all() as $rowErrors)
                                    <li>
                                    {!! $rowErrors !!}
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>      
                        @endif
                        {!! Form::open(array('route' => 'ki-admin.shippingoffice.store', 'method' => 'POST')) !!}
                            @include('pages.shippingoffice.form',array('submit' => 'Simpan'))
                        {!! Form::close() !!}
                        </div>
                    </div>
                </section>
@endsection


@section('custom-head')
    
@endsection

@section('custom-footer')
    
@endsection