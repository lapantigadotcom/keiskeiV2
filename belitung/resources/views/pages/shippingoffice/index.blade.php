@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Laporan Pengiriman &nbsp;&nbsp;&nbsp;
                                    @if(Auth::user()->hasAccess('ki-admin.shippingoffice.create'))
                                        <a href="{!! route('ki-admin.shippingoffice.create') !!}" class="btn btn-primary">Tambah Data </a>
                                    @endif
                                    </h3>                                    
                                </div>
                                <div class="box-body table-responsive">
                                    @include('errors.session')
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No Tracking</th>
                                                <th>Tujuan</th>
                                                <th>Ongkir</th>
                                                <th>Kurir</th>
                                               <th>Petugas</th>
                                                <th>Status</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $total = 0; ?>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>{{ $row->tracking_number }}</td>
                                                <td>{{ $row->tujuan }}</td>
                                                <td>
                                                    {{ intval($row->ongkir) }}
                                                    <?php $total += intval($row->ongkir); ?>
                                                </td>
                                                <td>{{ $row->courier->name }}</td>
                                                <td>{{ $row->petugas }}</td>

                                                <td>{!! $row->status=='1'?"<span class='label label-primary'>Selesai</span>":"<span class='label label-danger'>Proses</span>" !!}</td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.shippingoffice.change'))
                                                    <div class="btn-group">
                                                      <button class="btn btn-danger" type="button">Change</button>
                                                      <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                      </button>
                                                      <ul role="menu" class="dropdown-menu">
                                                      @if(Auth::user()->hasAccess('ki-admin.shippingoffice.change'))
                                                        <li><a href="{!! route('ki-admin.shippingoffice.change',[$row->id]).'?state=2' !!}">Proses</a></li>
                                                        <li><a href="{!! route('ki-admin.shippingoffice.change',[$row->id]).'?state=1' !!}">Selesai</a></li>
                                                      @endif
                                                      </ul>
                                                    </div>
                                                  @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No Tracking</th>
                                                <th>Tujuan</th>
                                                <th>Ongkir</th>
                                                <th>Kurir</th>
                                               <th>Petugas</th>

                                                <th>Status</th>
                                                <th>Opsi</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <div class="row">
                                        <h5 class="text-center">Total : {!! $total !!} </h5>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            @include('scripts.delete-modal')
@endsection


@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection