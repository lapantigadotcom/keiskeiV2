<div class="box-body">
	<h3>Add Stock</h3>
	@if(isset($data['ms_product_id']))
		{!! Form::hidden('ms_product_id', $data['ms_product_id'], array('class' => 'form-control')) !!}
	@endif
	<div class="form-group">
		{!! Form::label('amount', 'Amount') !!}
		{!! Form::number('amount', null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('date', 'Date') !!}
		<input type="date" id="date" name="date" class="form-control" value="@if(isset($data['content'])){!! $data['content']->date !!}@endif">
	</div>
	<div class="form-group">
		{!! Form::submit('Submit', array('class' => 'btn btn-primary')) !!}
	</div>
</div>