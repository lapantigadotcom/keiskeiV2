@extends('layouts.app')

@section('content')
            <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-xs-12">
                            @include('errors.session')
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">User List &nbsp;&nbsp;&nbsp;
                                    @if(Auth::user()->hasAccess('ki-admin.user.create'))
                                        <a href="{!! route('ki-admin.user.create') !!}" class="btn btn-primary">Tambah Data </a>
                                    @endif
                                    </h3>                                    

                                </div>
                                <div class="box-body table-responsive">
                                    <table id="dataTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Tgl Daftar</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Tipe Member</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data['content'] as $row)
                                            <tr>
                                                <td>{{ $row->created_at}}</td>
                                                <td>@if(Auth::user()->hasAccess('ki-admin.user.show'))<a href="{!! route('ki-admin.user.show',[$row->id]) !!}"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail User">{{ $row->username }}</a>@else {{ $row->username }} @endif </td>
                                                <td style="font-size:12px">{{ $row->email }}</td>
                                                <td>{{ $row->roles()->first()->name }}</td>
                                                <td style="font-size:12px">
                                                    {!! $row->enabled=='1'?"<span class='label label-primary'>Aktif</span>":"<span class='label label-danger'>Tdk Aktif</span>" !!}
                                                    {!! $row->approved=='1'?"<span class='label label-success'>Terverifikasi</span>":"<span class='label label-success'>Tdk Verifikasi</span>" !!}
                                                    {!! $row->safe=='1'?"<span class='label label-primary'>Banned</span>":"<span class='label label-danger'>NotBanned</span>" !!}
                                                </td>
                                                <td>
                                                @if(Auth::user()->hasAccess('ki-admin.user.edit'))
                                                    <a href="{!! route('ki-admin.user.edit',[$row->id]) !!}" class="fa fa-pencil-square-o"  data-toggle="tooltip" data-placement="bottom" title="Edit User"></a>&nbsp;&nbsp;
                                                @endif
                                                @if(Auth::user()->hasAccess('ki-admin.user.delete'))
                                                    <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.user.delete',[$row->id]) !!}" class="fa fa-trash-o"  data-toggle="tooltip" data-placement="bottom" title="Delete User"></a> &nbsp;&nbsp;
                                                @endif
                                                @if(Auth::user()->hasAccess('ki-admin.user.reset'))
                                                    <a href="javascript:void(0);" onclick="resetPasswordModal(this)" data-href="{!! route('ki-admin.user.reset',[$row->id]) !!}" data-email={!! $row->email !!} class="fa fa-refresh"  data-toggle="tooltip" data-placement="bottom" title="Reset Password"></a>
                                                @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Tgl Daftar</th>
                                                <th>Username</th>
                                                <th>Email</th>
                                                <th>Tipe Member</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
@include('scripts.delete-modal')
@include('scripts.reset-password-modal')
@endsection


@section('custom-head')
    {!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable();
            });
    </script>
@endsection