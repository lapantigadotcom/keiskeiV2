<ul class="nav nav-pills nav-stacked">
    <li><a href="{!! route('ki-admin.user.show',[$data['content']->id]) !!}"><i class="fa fa-user"></i> Detail</a></li>
    <li><a href="{!! route('ki-admin.user.show',[$data['content']->id]) !!}?type=message"><i class="fa fa-inbox"></i> Inbox</a></li>
    <li><a href="{!! route('ki-admin.user.show',[$data['content']->id]) !!}?type=logactivity"><i class="fa fa-info-circle"></i> Status</a></li>
    <li><a href="{!! route('ki-admin.user.show',[$data['content']->id]) !!}?type=report"><i class="fa fa-file-text-o"></i> Report</a></li>
    @if($data['content']->roles->first()->require_file=='1')
    <li><a href="{!! route('ki-admin.user.show',[$data['content']->id]) !!}?type=affiliate"><i class="fa fa-sitemap"></i> Affiliate</a></li>
    <li><a href="{!! route('ki-admin.user.show',[$data['content']->id]) !!}?type=withdraw"><i class="fa fa-plus"></i> Withdraw</a></li>
    @endif
</ul>