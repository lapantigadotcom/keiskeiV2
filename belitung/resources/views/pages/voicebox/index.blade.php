@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Kotak Suara
            <small>Mobile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Kotak Suara</h3>
              @if(Auth::user()->hasAccess('ki-admin.voicebox.index'))
              &nbsp;&nbsp;&nbsp;<a href="{!! route('ki-admin.voicebox.index') !!}" class="btn btn-primary btn-sm">Refresh</a>
              @endif
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Email</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data['content'] as $row)
                    <tr>
                      <td>{!! $row->name !!}</td>
                      <td><a href="{!! route('ki-admin.voicebox.show', [$row->id]) !!}">{!! $row->read_flag == '1'?$row->title:'<b>'.$row->title.'</b>' !!}</a></td>
                      <td>{!! $row->email !!}</td>
                      <td>{!! $row->created_at !!}</td>
                      <td>
                          <a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('ki-admin.voicebox.delete',[$row->id]) !!}" class="fa fa-trash-o"></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Title</th>
                    <th>Email</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div>
</section>
@include('scripts.delete-modal')
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection