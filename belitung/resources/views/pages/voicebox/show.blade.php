@extends('layouts.app')

@section('content')
<section class="content-header">
        <h1>
            Kotak Suara
            <small>mobile</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Detail Kotak Suara</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="mailbox-read-info">
                    <h3>Title : {!! $data['content']->title !!}</h3>
                    <h5>Name: {!! $data['content']->name !!} <span class="mailbox-read-time pull-right">{!! $data['content']->created_at !!}</span></h5>
                    <h5>Email: {!! $data['content']->email !!}</h5>
                    <h5>Telp: {!! $data['content']->telp !!}</h5>
                  </div><!-- /.mailbox-read-info -->
                  <div class="mailbox-read-message">
                    <h4>Pesan :</h4>
                    {!! $data['content']->description !!}
                  </div><!-- /.mailbox-read-message -->
                </div><!-- /.box-body -->
                <div class="box-footer">
                <div class="row">
                  <div class="col-md-4">
                    Image :
                  </div>
                  <div class="col-md-8">
                    @if(File::exists('ws/data/voicebox/'.$data['content']->photo))
                          <a href="{!! asset('ws/data/voicebox/'.$data['content']->photo) !!}" target="_blank" class="btn btn-primary btn-sm">Download</a>
                        @else
                          Not uploaded.
                        @endif
                  </div>
                </div>
                
                
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
            </div><!-- /.col -->
    </div>

    <div class="row">
        <div class="col-md-3">
          <a href="{!! route('ki-admin.voicebox.index') !!}" class="btn btn-warning btn-block margin-bottom">Back</a>
        </div>
    </div>
    </section>
@endsection


@section('custom-head')
{!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
{!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
<script type="text/javascript">
  $(function() {
    $('#dataTable').dataTable();
  });
</script>
@endsection