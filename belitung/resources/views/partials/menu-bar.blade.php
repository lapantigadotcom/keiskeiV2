        <header class="main-header">
        <!-- Logo -->
        <a href="{!! route('ki-admin.dashboard') !!}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>KeisKei</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>KeisKei</b>Panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>

                  <ul class="nav navbar-nav">
                  <li class="dropdown user user-menu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset('img/avatar5.png') }}" class="user-image" alt="User Image"/>
                                <span class="hidden-xs">{!! Auth::user()->name !!}</span>
                              </a>
                              <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                  <div class="pull-left">
                                    <a href="{!! route('ki-admin.getChangePassword') !!}" class="btn btn-default btn-flat">Ganti Password</a>
                                  </div>
                                  <div class="pull-right">
                                    <a href="{!! route('ki-admin.logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                                  </div>
                                </li>
                              </ul>
                   </li></ul>

<!--PESAN INBOX -->

   <ul class="nav navbar-nav hidden-xs hidden-sm">
               <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">                
                  <i class="fa fa-envelope" style="font-size:19px"></i>&nbsp; Inbox
                  <span class="label label-warning" style="font-size:12px">{!! $notificationblog !!} </span>|
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Ada  &nbsp;<b>{!! $notificationKontak !!} </b>&nbsp; Pesan inbox</li>
                  <li>
                                      <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="{!! route('ki-admin.contact-us.index') !!}">
                          <i class="fa fa-envelope text-aqua"></i> Cek inbox Pesan Contact Us
                        </a>
                      </li>
                      
                    </ul>
                  </li>
                </ul>
              </li>

                        <!--ORDER -->
                                       <ul class="nav navbar-nav ">
                                       <li class="dropdown notifications-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-success" style="font-size:12px">{!! $notificationOrder !!}</span>
             
                                          <i class="fa fa-shopping-cart" style="font-size:19px"></i>&nbsp;Order |

                                        </a>
                                          <ul class="dropdown-menu">
                                          <li class="header">  Ada &nbsp;<b>{!! $notificationOrder !!} order</span></b>&nbsp; </li>
                                          <li>
                                                              <!-- inner menu: contains the actual data -->
                                          <ul class="menu">
                                          <li>
                                          <a href="{!! route('ki-admin.contact-us.index') !!}">
                                          <i class="fa fa-gift text-green"></i> Cek order
                                          </a>
                                          </li>
                                              
                                          </ul>
                                          </li>
                                        </ul>
                                      </li>

                        <!-- BONUS -->
                           <ul class="nav navbar-nav">
                                       <li class="dropdown notifications-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">                
                                          <i class="fa fa-eye" style="font-size:19px"></i>&nbsp;PPN |
                                          
                                        </a>
                                        <ul class="dropdown-menu">
                                          <li class="header">Lihat  &nbsp;<b>PPN</b>&nbsp; Penjualan</li>
                                          <li>
                                                              <!-- inner menu: contains the actual data -->
                                            <ul class="menu">
                                              <li>
                                                <a href="{!! route('ki-admin.ppn.index') !!}">
                                                  <i class="fa fa-link text-orange"></i> Lihat Laporan PPN Bulanan
                                                </a>
                                              </li>
                                              
                                            </ul>
                                          </li>
                                        </ul>
                                      </li>

                                      <!-- Report BLOGS artikel -->

                                               <ul class="nav navbar-nav">
                                             <li class="dropdown notifications-menu">
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">                
                                                <i class="fa fa-file" style="font-size:19px"></i>&nbsp;Blogs |
                                                
                                              </a>
                                              <ul class="dropdown-menu">
                                                <li class="header">Manajemen &nbsp;<b>Artikel</b>&nbsp; Keiskei</li>
                                                <li>
                                                   <!-- inner menu: contains the actual data -->
                                                  <ul class="menu">
                                                    <li>
                                                      <a href="{!! route('ki-admin.article.index') !!}">
                                                        <i class="fa fa-list text-red"></i> Buka List
                                                      </a>
                                                    </li>
                                                    <li>
                                                      <a href="{!! route('ki-admin.article.create') !!}">
                                                        <i class="fa fa-plus text-green"></i> Tulis Artikel Baru
                                                      </a>
                                                    </li>
                                                    
                                                  </ul>
                                                </li>
                                              </ul>
                                            </li>


                                            <!-- Report Product -->

                                               <ul class="nav navbar-nav">
                                             <li class="dropdown notifications-menu">
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">                
                                                <i class="fa fa-list" style="font-size:19px"></i>&nbsp;Produk |
                                                
                                              </a>
                                              <ul class="dropdown-menu">
                                                <li class="header">Manajemen  &nbsp;<b>Produk</b>&nbsp; Keiskei</li>
                                                <li>
                                                   <!-- inner menu: contains the actual data -->
                                                  <ul class="menu">
                                                    <li>
                                                      <a href="{!! route('ki-admin.product.index') !!}">
                                                        <i class="fa fa-flag text-blue"></i> Lihat Produk KEISKEI
                                                      </a>
                                                    </li>
                                                    <li>
                                                      <a href="{!! route('ki-admin.product.create') !!}">
                                                        <i class="fa fa-plus text-green"></i> Tambahkan Produk KEISKEI
                                                      </a>
                                                    </li>
                                                    
                                                  </ul>
                                                </li>
                                              </ul>
                                            </li>

                                              <!-- Report shipping -->

                                               <ul class="nav navbar-nav">
                                             <li class="dropdown notifications-menu">
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">                
                                                <i class="fa fa-gear" style="font-size:19px"></i>&nbsp;Konfirmasi |
                                              <span class="label label-warning" style="font-size:12px">{!! $notificationTransfer !!} </span>
                                              </a>
                                              <ul class="dropdown-menu">
                                          <li class="header">
                        <a href="{!! route('ki-admin.transfer.index') !!}?status=pending&state=0">
                          <i class="fa fa-money text-green"></i> {!! $notificationTransfer !!} transfer belum disetujui
                        </a>
                      </li>
                                                   <!-- inner menu: contains the actual data -->
                                                  <ul class="menu">
                                                     
                                                    
                                                  </ul>
                                                </li>
                                              </ul>
                                            </li>



          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">                
                  <i class="fa fa-comment-o" style="font-size:19px"></i>Notifikasi
                  <span class="label label-danger" style="font-size:13px">{!! $notificationRegistration + $notificationTransfer + $notificationWithdraw + $notificationReview !!}</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Ada  &nbsp;<b>{!! $notificationRegistration + $notificationTransfer + $notificationWithdraw + $notificationReview !!}</b>&nbsp; Notifikasi Baru</li>
                  <li>
                                      <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="{!! route('ki-admin.user.index') !!}">
                          <i class="fa fa-users text-aqua"></i> {!! $notificationRegistration !!} user belum disetujui
                        </a>
                      </li>
                      
                      <li>
                        <a href="{!! route('ki-admin.notif-withdraw.index') !!}?status=pending&state=0">
                          <i class="fa fa-retweet text-red"></i> {!! $notificationWithdraw !!} withdrawal belum disetujui
                        </a>
                      </li>
                      <li>
                        <a href="{!! route('ki-admin.notif-registrationfee.index') !!}?status=pending&state=0">
                          <i class="fa fa-legal text-purple"></i> {!! $notificationRegistrationFee !!} registration fee belum disetujui
                        </a>
                      </li>
                      <li>
                        <a href="{!! route('ki-admin.notif-review.index') !!}?status=pending&state=0">
                          <i class="fa fa-star text-yellow"></i> {!! $notificationReview !!} Review Produk belum disetujui
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <!-- User Account: style can be found in dropdown.less -->

 <ul class="nav navbar-nav">
                                             <li class="dropdown notifications-menu">
                                              <a href="#" class="dropdown-toggle" onclick="history.back();" data-toggle="dropdown">                
                                                <i class="fa fa-backward text-red" style="font-size:16px"></i>&nbsp;
                                                <span class="text-red">back</span>
                                                
                                              </a>
                                             
                                                </li>
                                              </ul>
                                            </li>
       
       

              
            </ul>
          </div>
        </nav>
      </header>