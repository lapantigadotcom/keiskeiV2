                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ asset('img/avatar5.png') }}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>{!! Auth::user()->name !!}</p>

                            <a href="{!! route('ki-admin.getChangePassword') !!}"><i class="fa fa-circle text-orange"></i>Password</a>
                        <a href=" {!! route('ki-admin.logout') !!}"><i class="fa fa-circle text-red"></i> Log Out</a>

                        </div>
                    </div>
                    
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                    @foreach (Auth::user()->getEffectiveMenu() as $menu)
                        @if (count($menu->_child) > 0)
                        <li class="treeview">
                            <a href="{{ $menu->route==''?'':route($menu->route) }}">
                                <i class="fa fa-{{ $menu->icon }}"></i>
                                <span>{{ $menu->name }}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            @foreach ($menu->_child as $child)
                                <li><a href="{!! $child->route==''?'':route($child->route) !!}"><i class="fa fa-angle-double-right"></i> {{ $child->name }}</a></li>
                            @endforeach
                            </ul>
                        </li>
                        @else
                        <li class="">
                            <a href="{!! URL::to('admin/dashboard') !!}">
                                <i class="fa fa-{{ $menu->icon }}"></i> <span>{{ $menu->name }}</span>
                            </a>
                        </li>
                        @endif
                    @endforeach
                    </ul>
                    <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> NOTICE !</h4>
Ada <span><b>{!! $notificationRegistration !!}</b></span> USER BARU BELUM DI-AKTIF-KAN. Silahkan di tindak lanjuti.</div>
                </section>
                <!-- /.sidebar -->