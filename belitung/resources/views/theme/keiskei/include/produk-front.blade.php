<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<center><h2 class="title text-center">Produk Kami</h2>	
						<h5>Beauty | Health | Cure </h5></center>
						@foreach($data['latest'] as $row)

							<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
									@if($row->productImages()->count() > 0)
									@if(File::exists($row->productImages->first()->image))
									<img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
									@endif
									@endif
										<h2></h2>
										<p>{!! $row->title !!}</p>
											<a href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Lihat</a>
									</div>
								<!--	<div class="product-overlay">
										<div class="overlay-content">
 											<p>{!! $row->title !!}</p>s
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
									</div>-->
								</div> 
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>{!! ucwords($row->categoryproduct->name) !!}</a></li>
									</ul>
								</div>
							</div>
						</div>

												@endforeach

					
						
					</div><!--features_items-->
					
					 
					
				</div>