@extends('theme.keiskei.app')

@section('content')

  
	<title>
	KeisKei Indonesia | {!! $data['content']->title !!}

	</title>
	<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@keiskeiashitaba" />
<meta name="twitter:creator" content="@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="{!! $data['content']->meta_description !!}" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="{!! $data['content']->meta_description !!}" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="{!! $data['content']->meta_description !!}" />
<meta name="DC.subject" content="{!! $data['content']->meta_keyword !!}" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="{!! $data['content']->meta_description !!}">
<meta name="keywords" content="{!! $data['content']->meta_keyword !!}"/>
<meta name='robots' content='INDEX, FOLLOW' />
<br><br>
<section>
		<div class="container">
			<div class="row">
  				@include('theme.keiskei.include.sidebar-default')
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{!! $data['content']->title !!}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i>KEISKEI INDONESIA</li>
 									<li><i class="fa fa-calendar"></i> {!! date('l, j F Y') !!}</li>
								</ul>
								 
							</div>
							<a href="">
								<img src="images/blog/blog-one.jpg" alt="">
							</a>@include('errors.session')
							<p>{!! $data['content']->description !!}
								 </p>
							</div>
					</div><!--/blog-post-area-->

					<div class="rating-area">
						 
						<ul class="tag">
							<li>TAG:</li>
							<li><a class="color" href="">Pink <span>/</span></a></li>
							<li><a class="color" href="">T-Shirt <span>/</span></a></li>
							<li><a class="color" href="">Girls</a></li>
						</ul>
					</div><!--/rating-area-->

					<div class="socials-share">
						<a href=""><img src="images/blog/socials.png" alt=""></a>
					</div><!--/socials-share-->

					<div class="media commnets">
						<a class="pull-left" href="#">
							<img class="media-object" style="max-height:60px" src="{{ asset('theme/keiskei_V2/images/logo.png') }}" alt="">
						</a>
						<div class="media-body">
							<h4 class="media-heading">Ashitaba</h4>
							<p>Ashitaba berasal dari kata “Ashita” yang berarti “Esok” dan “Ba” yang berarti “Daun”, apabila digabungkan Ashitaba mempunyai arti “Daun Esok Hari”.   </p>
							<div class="blog-socials">
								<ul>
									<li><a href=""><i class="fa fa-facebook"></i></a></li>
									<li><a href=""><i class="fa fa-twitter"></i></a></li>
									<li><a href=""><i class="fa fa-dribbble"></i></a></li>
									<li><a href=""><i class="fa fa-google-plus"></i></a></li>
								</ul>
								<a class="btn btn-primary" href="">Other Posts</a>
							</div>
						</div>
					</div>
					 
				</div>	
			</div>
		</div>
	</section>	@include('theme.keiskei.partials.best-products')

		@endsection
		
	 
  