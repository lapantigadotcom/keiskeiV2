@extends('theme.keiskei.app')
<title>
	KeisKei Indonesia | Pendaftaran Anggota
	</title>
@section('content')

<style type="text/css">
.help-block {
	color: red;font-size: 12px;font-style: italic;font-weight: 300;
}
</style>
		 <br>
			<div class="row">
				<div class="form-head text-center">
					<h3>
					<b>Daftar anggota</b> <span style="color:green">Keiskei Indonesia</span>
					</h3>
					<p>
						Dapatkan banyak keuntungan dengan menjadi anggota <span style="color:green">Keiskei Indonesia</span>. 
					</p>
					@include('theme.'.$data['theme']->code.'.include.session')
					<hr>
				</div>
 			</div>
					
					<section id="form"><!--form-->
							<div class="container">
								<div class="row">
									<div class="col-sm-4 col-sm-offset-1">
										<div class="login-form"><!--login form-->
											<h2>Daftar Akun <span style="color:green"><b>Smart System</b></span></h2>
											{!! BootstrapForm::open(array('store' => 'register.store','id' => 'form-validate')) !!}
											@if(!empty($data['affiliate_token']))
											{!! Form::hidden('referraled_by',$data['affiliate_token']) !!}
											@endif

											{!! BootstrapForm::open(array('store' => 'register.store','id' => 'form-validate')) !!}
										@if(!empty($data['affiliate_token']))
										{!! Form::hidden('referraled_by',$data['affiliate_token']) !!}
										@endif
										
										{!! BootstrapForm::text('username') !!}
										{!! BootstrapForm::text('mobile','Nomor Handphone (Untuk Aktivasi)') !!}
										{!! BootstrapForm::text('email') !!}

										<div class="form-group">
											<label for="ms_country_id">
												Negara
											</label>
											{!! Form::select('ms_country_id',$data['country'],null,array('class' => 'form-control','id' => 'ms_country_id')) !!}
										</div>

										<div class="form-group" id="city-container">
											<label for="ms_city_id">
												Kota
											</label>
											{!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id')) !!}
										</div>

										<center>
											<div class="form-group">
											{!! Recaptcha::render() !!}
											<span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>
										</div></center>

 											{!! Form::submit('Bergabung',array('class' => 'btn btn-primary')) !!} 
					 					</div><!--/login form-->{!! BootstrapForm::close() !!}
									</div>
									<div class="col-sm-1">
					 				</div>
									<div class="col-sm-4">
										<div class="signup-form"><!--sign up form-->
										<h2>Catatan <span style="color:green"><b>Penting !?</b></span></h2>

											<p>
												1. Jangan Memberikan detail akun dan password <span style="color:green;font-weight: 500;">Keiskei Indonesia</span> Anda kepada siapapun.
											</p>
											<p>
												2. Pastikan URL yang Anda akses adalah <span style="font-weight: 500;color:green">https://www.keiskei.co.id</span>
											</p>
											<p>
												3. Setelah ter-verifikasi, pastikan Anda mengisi detail profil Anda di <span style="font-weight: 500;color:green">Beranda - edit profile</span>.
											</p>

											<p>
												4. Pihak <span style="font-weight: 500;color:green">Keiskei Indonesia</span> tidak pernah menanyakan akun dan password Anda dengan alasan apapun, harap berhati-hati.
											</p>
											<br>
												<center><script language="JavaScript" type="text/javascript">
				TrustLogo("https://www.keiskei.co.id/theme/keiskei/img/comodo-keiskei.gif", "CL1", "none");
				</script></center>

											@include('theme.'.$data['theme']->code.'.include.session')
											
										 
										</div><!--/sign up form-->

									</div>
								</div>
							</div>
					</section><!--/form-->

 
 
 @endsection

@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/validation/jquery.validate.min.js') !!}
	<script type="text/javascript">
		$(function () {
        	var value = $("#ms_country_id").val();
        	cityField(value);
      	});
		function cityField( value)
		{
			var general_country_id = {!! $data['general']->ms_country_id !!};
			if(value == general_country_id)
			{
				$('#city-container').show();
			}else{
				$('#city-container').hide();
			}
		}
	    $(document).ready(function() {

		   $('#ms_country_id').selectize();      
		   $('#ms_city_id').selectize();      
		   $('#ms_country_id').change(function()
		   {
		   		cityField($(this).val());
		   });
		   
		   $('#form-validate').validate({
		   		errorClass : "help-block error",
		   		rules: {
		   			username : {
		   				required: true,
		   				minlength: 3
		   			},
		   			mobile : {
		   				required: true,
		   				minlength: 3
		   			},
		   			email :{
		   				required: true,
		   				email : true
		   			}
		   		},
		   		messages: {
		   			username : {
		   				required : "Mohon masukkan username Anda",
		   				minlength : $.validator.format("Username Anda harus minimal {0} karakter")
		   			},
		   			mobile : {
		   				required : "Mohon masukkan nomor handphone Anda",
		   				minlength : $.validator.format("nomor handphone Anda harus minimal {0} karakter")
		   			},
		   			email : {
		   				required : "Mohon masukkan email Anda",
		   				email : "Mohon cek kembali alamat email Anda"
		   			}
		   		}
		   });
	    });
	</script>
@endsection