@extends('theme.keiskei.app')

@section('content')
@include('theme.keiskei.partials.meta-blog')

<br><br>
<section>
		<div class="container">
			<div class="row">
  				@include('theme.keiskei.include.sidebar-default')
				@include('errors.session')

<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">KEISKEI INDONESIA | The Real Ashitaba</h2>
						@foreach($data['content'] as $row)

						<div class="single-blog-post">
							<h3><a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><?php echo $row->title; ?></h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i>KEISKEI INDONESIA</li>
 									<li><i class="fa fa-calendar"></i> {!! date('l, j F Y') !!}</li>
								</ul>
								 
							</div>
							 
							<p><?php $desc = explode("<!-- pagebreak -->", $row->description);
								if(count($desc)>1)
									{ ?>
								<p>{!! $desc[0] !!}&nbsp;</p>      
								<?php }else{ ?>
								<p>{!! strip_tags(substr($row->description,0,400)).'...' !!}</p>
								<?php
							}
							?> 
							</p>
							<a  class="btn btn-primary" href="{!! route('articles',[$row->id.'-'.$row->title]) !!}">Read More</a>
						</div>@endforeach
						
					</div><div class="pagination-area">
							<ul class="pagination">
								<li>{!! $data['content']->render() !!}</li>
							</ul>
						</div>
				</div>
 						

			</div>
		</div>
	</section> 				
	@include('theme.keiskei.partials.best-products')

	@endsection
		@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
		$(document).ready(function() {
			$('.img-lazy').unveil();
			$("#category").selectize();
			$("#product-slider").owlCarousel({
				items : 3,
				lazyLoad : true,
				navigation : true,
				pagination : false,
				navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
			}); 
		});
	</script>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>

	@endsection
	@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
	@endsection