@extends('theme.jovem.layout.index')

@section('content')
@if($data['content']->portfolio()->count() > 0)
<div class="row">
        <div id="jovem-slider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          @for($i=0; $i<$data['content']->portfolio->detailPortfolio->count(); $i++)
          <li data-target="#jovem-slider" data-slide-to="{{ $i }}" @if($i==0) class="active" @endif></li>
          @endfor
        </ol>
        <div class="carousel-inner full-width-image" role="listbox">
          <?php $i=0; ?>
          @foreach($data['content']->portfolio->detailPortfolio as $row)
          <div class="item @if($i==0) active @endif">
            <img src="{{ asset('upload/media/'.$row->media->file) }}" alt="{{ $row->title!=''?$row->title:'' }}">
            <div class="carousel-caption">
              {{ $row->title!=''?$row->title:'' }}
            </div>
          </div>
          <?php $i++ ?>
          @endforeach
        </div>
      </div>
    
</div>
@endif
    <div class="row">
        <div class="col-md-9 breadcrumbs">
            <div class="col-md-offset-1 col-md-10">
                <a href="{!! URL::to('/') !!}">Home</a>  <b> > </b> 
                <a href="javascript:void(0);" class="current">{{ $data['content']->title }}</a>
            </div>
                
        </div>
        <div class="col-md-3 bg-jovem-dark mod-testimony text-center">
            <span class="glyphicon glyphicon-play icon-play">
            </span>
            <h5>Video Testimoni</h5>
          </div>
    </div>
<div class="row" style="min-height:70%;">
    <div class="col-md-12">
        <div class="col-md-offset-1 col-md-10">
            <h3>{{ $data['content']->title }}</h3>
            @if($data['content']->ms_media_id !='' && $data['content']->ms_media_id !='0')
            <div class="row">
                <div class="col-md-7" style="text-align:center;">
                    <img src="{{ asset('upload/media/'.$data['content']->mediaPost->file) }}" class="img-responsive">
                </div>
                <div class="col-md-5 text-left">
                <ul class="list-tag">
                    @foreach($data['content']->tag as $row)
                    <li>
                        {!! $row->title !!}
                    </li>
                    @endforeach
                </ul>
                    
                </div>
            </div>
            @else
            <div class="row">
                <div class="col-md-12" style="">
                <ul class="list-tag">
                    @foreach($data['content']->tag as $row)
                    <li>
                        {!! $row->title !!}
                    </li>
                    @endforeach
                </ul>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6 post-icon">
                    <i class="glyphicon glyphicon-calendar"></i> {{ date("d M Y" ,strtotime($data['content']->created_at)) }} &nbsp;&nbsp;&nbsp;
                    <i class="glyphicon glyphicon-user"></i> {{ $data['content']->user->name }} &nbsp;&nbsp;&nbsp;
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">


<!-- TARIF ONGKIR -->






<!-- /TARIF ONGKIR -->




                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_sharing_toolbox"></div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">

    </div>
    
</div>
 
@endsection

@section('custom-footer')

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-55cd97ec6f575082" async="async"></script>

@endsection