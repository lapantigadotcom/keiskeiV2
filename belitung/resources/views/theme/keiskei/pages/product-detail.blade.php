@extends('theme.keiskei.app')

@section('content')
<title>Keiskei Indonesia | {!! $data['content']->categoryproduct->name !!} | {!! $data['content']->title !!}</title>
<link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="{!! $data['content']->description !!} " />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta property="og:title" content="Keiskei Indonesia | {!! $data['content']->categoryproduct->name !!} | {!! $data['content']->title !!}" />
<meta property="og:description" content="{!! $data['content']->description !!}" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="{!! $data['content']->description !!}">
<meta name='robots' content='INDEX, FOLLOW' />

@include('theme.keiskei.scripts.price-product')
@include('theme.keiskei.include.review-content',['id' => 'review-content', 'reviews' => $data['content']->reviews ])

		@include('theme.keiskei.include.sidebar-default')

				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<ul id="etalage">
									@if($data['content']->productImages()->count() > 0)
									@foreach($data['content']->productImages as $row)
									@if(File::exists($row->image))
									<li>
										<!-- This is the large (zoomed) image source: -->
										<img class="etalage_source_image" src="{!! asset($row->image) !!}" title="© PT. Punyakunik Maju Jaya" />
										<!-- This is the thumb image source (if not provided, it will use the large image source and resize it): -->
										<img class="etalage_thumb_image" src="{!! asset($row->image) !!}" />
									</li>
									@endif

									@endforeach
									@endif
								</ul> 
							</div>

						</div>

						<div class="col-md-6 product-information">
						<div class="product-title">
							<h4>
								{!! $data['content']->title !!}<hr>
							</h4>
 								<div class="star">
									@for($i=1; $i<=intval($data['avg_review']); $i++ )
									<i class="fa fa-star fa-lg"></i>
									@endfor
									@for($i=intval($data['avg_review']); $i<=5; $i++ )
									<i class="fa fa-star-o fa-lg"></i>
									@endfor
									<span> {!! $data['content']->reviews()->count() !!} Review</span>
								</div>
								<span class="divider"></span><br>
 						</div>
						<div class="product-description">
							<p> Kategori : <strong>{!! $data['content']->categoryproduct->name !!}</strong> </p>
 							<p> Kode Produk : {!! $data['content']->code !!} </p>
							<h4>Kemasan</h4>
							<p> Dimensi :  {!! $data['content']->length !!} cm</p>
							<p> Berat :  {!! $data['content']->weight !!} Kg</p>
							<h4>Kandungan</h4>
							<p> {!! $data['content']->bahan !!}</p>
<span>
									<span><?php
								if(Auth::check())
								if(Auth::user()->roles->first()->level == '5'){
							      setlocale(LC_ALL, "en_US.UTF-8");
							    }else{
							      setlocale (LC_MONETARY, 'id_ID');
							    }
								?>
								Harga: {!! money_format('%(#10n', priceDisplay($data['default_role'], $data['content']->prices) + (   priceDisplay($data['default_role'], $row->product->prices))    )  !!}
							<hr></span>

							<span class="divider">&nbsp;</span>
						</div>
						{!! Form::open(array('route' => ['user.cart.store',$data['content']->id],'method' => 'post')) !!}
						<div class="product-attribute">
							<?php
							$id_att = -1;
							?>
							@foreach($data['content']->detailAttributeProducts as $row)
							@if($row->detailAttribute->ms_attribute_id != $id_att)

							<?php
							$id_att = $row->detailAttribute->ms_attribute_id;
							$d_attribute_t = $data['content']->detailAttributeProducts->filter(function($f) use($id_att) {
								if($f->detailAttribute->ms_attribute_id == $id_att && $f->enabled=='1')
								{
									return true;
								}
							});
							$arr_data = array();
							foreach ($d_attribute_t as $val) {

								$arr_data[$val->detailAttribute->id] = $val->detailAttribute->name;
							}
							?>
							<div class="form-group">
								{!! Form::label($row->detailAttribute->attribute->name, $row->detailAttribute->attribute->name, ['class' => 'form-label']) !!}
								@if($row->detailAttribute->attribute->typeattribute->code == 'SB')
								{!! Form::select('att'.$row->detailAttribute->attribute->id,$arr_data,null,['class' => 'form-control']) !!}
								@elseif($row->detailAttribute->attribute->typeattribute->code == 'CB')
								<div class="checkbox">
									@foreach($arr_data as $key => $val)
									<label>

										<input type="checkbox" name="{!! 'att'.$row->detailAttribute->attribute->id !!}[]" value="{!! $key !!}">
										{!! $val !!}&nbsp;&nbsp;
									</label>
									@endforeach
								</div>
								@elseif($row->detailAttribute->attribute->typeattribute->code == 'RB')
								<div class="radio">
									@foreach($arr_data as $key => $val)
									<label>
										{!! Form::radio('att'.$row->detailAttribute->attribute->id, $key) !!}
										{!! $val !!}
									</label>
									@endforeach
								</div>
								@endif
							</div>
							@endif
							@endforeach 
						</div>
						<div class="product-action">
							 
							

								</span>
							<br> 
							<div class="action-list">
								<button type="submit" class="btn btn-fefault"><i  title="Beli produk ini" class="fa fa-shopping-cart fa-lg"></i> Beli Produk</button>

								<a class="btn btn-default" title="Tulis Ulasan" data-toggle="modal" data-target="#review"><i class="fa fa-edit"></i></a>
								<a class="btn btn-default" title="Daftar Ulasan produk" data-toggle="modal" data-target="#review-content"><i class="fa fa-exchange"></i></a>
								<a class="btn btn-default"  title="FAQ produk" data-toggle="modal" data-target="#help"><i class="fa fa-question"></i></a>
								@if(isset($data['content']->files))
								@if(!empty($data['content']->files))
								@if(File::exists($data['content']->files))
								<a class="btn btn-default"  title="Download File Product" target='_blank' href="{!! asset($data['content']->files) !!}"><i class="fa fa-download"></i></a>
								@endif
								@endif
								@endif
 
							</div>
							<script type="text/javascript">

								buttons: [{
									text: "Ok",
									id : "btnModalDialog",
									Click : function () {                    
										var isOpen = $("#dialog-confirm").dialog("isOpen");
										if (isOpen == true) {
											okCallBack();
										}
										$("#dialog-confirm").dialog("close");
									}
								}]


							</script>
						{!! Form::close() !!}
						@include('theme.keiskei.include.help',[ 'help' => $data['content']->help])
						@include('theme.keiskei.include.review',[ 'idP' => $data['content']->id,'id' => 'review'])
																<h4>&nbsp;</h4>

									
								<a href=""><img src="images/product-details/share.png" class="share img-responsive" alt=""></a>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#detail" data-toggle="tab">Detail</a></li><li><a href="#ulasan" data-toggle="tab">Ulasan</a></li>
                            <li><a href="#spesifikasi" data-toggle="tab">Indikasi</a></li>
                            
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="detail">{!! $data['content']->description !!}</div>
                        <div class="tab-pane fade" id="spesifikasi">{!! $data['content']->specification !!}</div>
                        <div class="tab-pane fade" id="ulasan">
                        	 <h5>Ulasan Produk</h5><hr>
							@foreach($data['content']->reviews as $review)
							<div class="review-detail">
								<div class="review-item">
									<div class="review-item-head">
										<div class="row">
											<div class="col-md-8 text-left review-item-inline">
												<span>{!! $review->user->name !!}</span>
												<i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($review->created_at)) !!} <i class="fa fa-bell-o"></i> {!! date('l', strtotime($review->created_at)) !!}
												<i class="fa fa-calendar-o"></i>  {!! date('j F', strtotime($review->created_at)) !!}
											</div>
											<div class="col-md-4 text-right">
												<div class="rate">
													<div class="star">
														@for($i=0; $i<intval($review->rate); $i++ )
														<i class="fa fa-star fa-lg"></i>
														@endfor
														@for($i=intval($review->rate); $i<=5; $i++ )
														<i class="fa fa-star-o fa-lg"></i>
														@endfor
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="review-item-body">
										<p>
											{!! $review->description !!}
										</p>
									</div>
								</div>
							</div>

							@endforeach
						</div>
                        
                    </div>
                </div>
            </div><!--/category-tab-->
					
					 
		</div>			
				</div>
		
		<style type="text/css">
		.panel-heading {
			padding: 0;border-bottom: 0;
		}
		.shop-details-tab {
			border: 0;
		}


		</style>
@include('theme.keiskei.partials.best-products')


@endsection

@section('custom-footer')




<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>

{!! HTML::script('theme/keiskei/plugin/etalage/js/jquery.etalage.min.js') !!}

<script type="text/javascript">
	$(document).ready(function() {
		$('#etalage').etalage({
			thumb_image_width: 350,
			thumb_image_height: 350,
			source_image_width: 1200,
			source_image_height: 1600,
			zoom_area_width: 450,
		});
		$("#product-slider").owlCarousel({
			items : 5,
			lazyLoad : true,
			navigation : true,
			pagination : false,
			navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
		}); 
	});
</script>

@endsection

@section('custom-head')
{!! HTML::style('theme/keiskei/plugin/etalage/css/etalage.css') !!}


@endsection