@extends('theme.keiskei.app')



@section('content')
<title>
	KeisKei Indonesia | Beauty, Health, Cure </title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta name="DC.subject" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover">
<meta name='robots' content='INDEX, FOLLOW' />
<section id="advertisement" >
		<div class="container">
		<img src="{{ asset('theme/keiskei_V2/images/banner/Banner_K-Business.jpg') }}" alt="keiskei 17 Agustus">
  
		</div> 
</section>
 
<br><br>
				<!--sidebar-->
				<div class="col-sm-3">
					<div class="left-sidebar">
  						
					 
						<div class="shipping text-center"><!--shipping-->
							<img src="{!! asset('theme/keiskei_V2/images/register-banner.png') !!}" alt="">
						</div><!--/shipping-->

						<div class="shipping text-center">
						<p>  Selalu terkoneksi dengan informasi dan berita terbaru dari kami, juga berbagai fitur menarik lainnya? Silakan download aplikasinya di gadget Anda. 
						</p>	
							<img src="{!! asset('theme/keiskei_V2/images/playstore-ic.png') !!}" alt="">
						</div>
						
					</div>
				</div>	<!--/sidebar-->


<div class="col-sm-9 padding-right">
	<div class="features_items"><!--features_items-->

											@foreach($data['products'] as $row)

		<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
											<div class="productinfo text-center">
												@if($row->productImages()->count() > 0)
												@if(File::exists($row->productImages->first()->image))
												<img src="{!! asset($row->productImages->first()->image) !!}" alt="" />
												@endif
												@endif
												<h2></h2>
												<p>{!! $row->title !!}</p>
												<a href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Lihat Produk</a>
											</div>
									</div> 
									<div class="choose">
														<ul class="nav nav-pills nav-justified">
															<li><a   style="color:#0C3121;cursor: pointer; cursor: hand; " title="Tulis Ulasan" data-toggle="modal" data-target="#{{ 'review'.$row->id }}"><i class="fa fa-edit"></i></a></li>
															<li> <a  style="color:#0C3121;cursor: pointer; cursor: hand; " title="Daftar Ulasan produk" data-toggle="modal" data-target="#{{ 'review-content-'.$row->id }}"><i class="fa fa-exchange"></i></a></li>
															<li><a title="FAQ produk" data-toggle="modal" data-target="#help"  style="color:#0C3121;cursor: pointer; cursor: hand; " onclick="productHelp(this)" data-product="{!! $row->id !!}"><i class="fa fa-question"></i></a></li>

														</ul>
									</div>
								</div>@include('theme.keiskei.include.review-content',['id' => 'review-content-'.$row->id, 'reviews' => $row->reviews ])
			@include('theme.keiskei.include.review',[ 'idP' => $row->id,'id' => 'review'.$row->id])
		</div>@endforeach 
	</div>
							
</div>
					
					 
					
				 
		<div class="row">
			<div class="col-md-12">
				{!! $data['products']->render() !!}
			</div>
		</div>
	


 @include('theme.keiskei.include.help',[ 'help' => 'Bantuan'])  
		 @endsection

@section('custom-footer')


	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$('.img-lazy').unveil();
	    	$("#category").selectize();
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	    function changeCategory(x)
	    {
	    	if(x.value==0)
	    	{
	    		window.location.replace("{!! route('product') !!}");
	    	}else
	    	{
		    	window.location.replace("{!! route('product') !!}/" + x.value + "-" + $(x).text());	    	
	    	}
	    }
	    function productHelp(x)
	    {
	    	var product_id = x.getAttribute("data-product");
	    	$.ajax(
		   	{
		   		url:"{{ route('product.getjson') }}",
		   		type: "GET",
		   		data:{
			      product_id : product_id
			    }
			}).done(function(data,status){
				if(status=='success')
			    {	
			    	console.log(data);
			    	$('#help-text').empty();
			    	$('#help-text').append(data.help);
			    }
			});
	    }
	</script>



@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
		{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}

	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection