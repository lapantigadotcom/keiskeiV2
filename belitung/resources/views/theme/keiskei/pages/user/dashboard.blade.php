@extends('theme.keiskei.app')
<title>Keiskei Indonesia | User: {!! $data['content']->name !!}</title>

@section('content')
@include('theme.keiskei.scripts.price-product')


			<center>
				<div class="se-pre-con">
				  	<div style="left: 0px; margin: -100px auto auto; position: absolute; width: 100%; top: 40%;">
							 <h1><span style="color:green">KEISKEI SMART SYSTEM </span></h1>
				    <i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><br>
Membuka Halaman Profil Anda..Mohon ditunggu...
					</div>
				</div>
			</center>


		<section id="do_action" style="margin:30px">
			
			@include('errors.session')
			@if($data['content']->roles->first()->default!='1')
			@endif

			<div class="col-sm-12">
	<div class="heading">
		<h3><p>Selamat datang di Keiskei Smart System, <span style="color:green">{!! $data['content']->name !!}</span> </h3>
	</div>
							
		<div class="col-sm-4">
			<div class="total_area">
				<ul>
					@if(File::exists('data/user/photo/'.$data['content']->photo) and $data['content']->photo != '')
					<img src="{!! asset('data/user/photo/'.$data['content']->photo) !!}" class="img-responsive img-thumbnail" style="padding:10px;">
				@else
					<img src="{!! asset('theme/keiskei/img/user.png') !!}" class="img-responsive img-thumbnail">
				@endif
				</ul>
				<ul>
					<li>Status Keanggotaan @if($data['content']->approved=='1')
							<span class="unverivied" style="color:green">Terverifikasi</span>
						@else
							<span class="unverivied" style="color:red">Belum Terverifikasi
						@endif</span></li>
						<li>
								Tipe<span>{!! $data['content']->roles->first()->name !!}</span>
						</li>

						<li>@if($data['content']->roles->first()->require_file=='1')
											 Bonus Anda : <span class="unverivied" style="color:red">{!! money_format('%(#10n', $data['total_active']) !!}</span>
</li>											<a class="btn btn-default check_out" href="{!! route('user.withdraw.get') !!}">Withdraw</a>
@endif
							 </ul>
			</div>
		</div>
	 

					<!-- DATA DIRI -->
					<div class="col-sm-6">
									<div class="total_area">
										<ul>
											<li>Nama<span> {!! $data['content']->name !!}</span></li>
											@if($data['content']->roles->first()->default!='1')
											<li>Perusahaan <span>{!! $data['content']->company !!}</span></li>@endif
											<li>ID Anggota <span>{!! $data['content']->code!=''?$data['content']->code:'-' !!}</span></li>
											<li>Email <span>{!! $data['content']->email!=''?$data['content']->email:'-' !!}</span></li>
											<li>Telepon <span>{!! $data['content']->telephone!=''?$data['content']->telephone:'-' !!}</span></li>
											<li>No. HP <span>{!! $data['content']->mobile!=''?$data['content']->mobile:'-' !!} </span></li>
											<li>PIN BB <span>{!! $data['content']->pinbb!=''?$data['content']->pinbb:'-' !!}</span></li>
											<li>Alamat <span>{!! $data['content']->address!=''?$data['content']->address:'-' !!} </span></li>
											@if($data['content']->roles->first()->default!='1')
											@foreach($data['content']->accountBank as $row)
											<li>{!! $row->bank->name !!} <span>{!! $row->account_name !!}</span></li>
											@endforeach
										</ul>@endif
											<a class="btn btn-default check_out" href="javascript:void(0)" class="change-password"  data-toggle="modal" data-target="#change-password"><i class="fa fa-lock"></i> &nbsp;Ganti Password</a>

											<a class="btn btn-default check_out" href="{!! route('user.profile.edit') !!}"><i class="fa fa-user"></i>&nbsp;Edit Profile</a>
											@if($data['content']->roles->first()->require_file=='1' &&  $data['content']->approved!='1')
											@if($data['content']->registrationfee()->count() == 0)
											<a class="btn btn-default check_out" href="{!! route('user.registrationfee') !!}">Registration Fee</a>
											@endif
											@endif
											<a class="btn btn-default check_out" href="{!! route('user.report') !!}"><i class="fa fa-shopping-cart"></i>&nbsp;Histori Transaksi</a>
											
											 
					</div>
			</div>
</div>
			<!-- TAB USER -->
			<div class="container">
				<div class="col-sm-12">
					<div class="category-tab"><!--category-tab-->
							<ul class="nav nav-tabs">
								<li class="active"><a href="#inbox" data-toggle="tab">Inbox</a></li>
								<li><a href="#outbox" data-toggle="tab">Outbox</a></li>
								<li><a href="#report" data-toggle="tab">Histori Transaksi</a></li>
								<li><a href="#status" data-toggle="tab">Status</a></li>
							</ul>
							
						<div class="tab-content">
										<div class="tab-pane fade active in" id="inbox">
			 								@foreach($data['message_inbox'] as $row)
									  	<div class="review-detail">
									  		<div class="review-item">
									  			<div class="review-item-head">
									  				<div class="row">
									  					<div class="col-md-12 text-left review-item-inline" style="color:green">
									  						<span style="font-size:12px">Pengirim: &nbsp;</span><span>{!! $row->user->name !!} </span>
									  						<i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($row->created_at)) !!}  <i class="fa fa-bell-o"></i> {!! date('l', strtotime($row->created_at)) !!} 
									  						<i class="fa fa-calendar-o"></i> {!! date('j F', strtotime($row->created_at)) !!} 
									  					</div>
									  				</div>
									  				
									  			</div>
									  			<div class="review-item-body">
									  				<p>
									  					{!! $row->content !!}
									  				</p>
									  			</div>
									  		</div>
									  	</div>
									  	@endforeach
									  	@if($data['message_inbox']->count() == 0)
									  	<div class="review-detail">
									  		<h5>Tidak ada pesan masuk</h5>
									  	</div>
									  	@endif			
						</div>
															
						<div class="tab-pane fade" id="outbox">
										@foreach($data['message_outbox'] as $row)
								  	<div class="review-detail">
								  		<div class="review-item">
									  			<div class="review-item-head">
									  				<div class="row">
									  					<div class="col-md-12 text-left review-item-inline" style="color:green">
									  						<span style="font-size:12px">Tujuan: &nbsp;</span><span>{!! $row->recipient()->count()>0?$row->recipient->name:'N.A' !!} </span>
									  						<i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($row->created_at)) !!}  <i class="fa fa-bell-o"></i> {!! date('l', strtotime($row->created_at)) !!} 
									  						<i class="fa fa-calendar-o"></i> {!! date('j F', strtotime($row->created_at)) !!} 
									  					</div>
									  				</div>
									  				
									  			</div>
									  			<div class="review-item-body">
									  				<p>
									  					{!! $row->content !!}
									  				</p>
									  			</div>
								  		</div>
								  	</div>
										  	@endforeach
										  	@if($data['message_outbox']->count() == 0)
							<div class="review-detail">
								  		<h5>Tidak ada pesan keluar</h5>@endif	
							</div>
								  					
						</div>
												
							<div class="tab-pane fade" id="report">
								<table class="table">
										<thead>
											<tr>
												<th>Kode Invoice</th>
												<th>Waktu</th>
												<th>Keterangan</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody>
										<?php $no=1; ?>
										@foreach($data['report'] as $row)
										<tr>
											<td>
												<a href="{!! route('user.invoice',[$row->id]) !!}?invoice={!! $row->code !!}" target="_blank">
													<b>{!! $row->code !!}</b>
												</a>
											</td>
											<td>{!! date('j M Y',strtotime($row->created_at)) !!}</td>
											<td>
												<b>{!! $row->statusOrder->name !!}</b>
											</td>
											<td class="text-right">
											<?php 
												$total = 0; ?>
												@foreach($row->detailOrders as $val)
												<?php $total_t = priceDisplay($data['default_role'], $val->product->prices); 
												if(!empty($val->product->discount))
												{
													$discount_t = ($total_t/100)*intval($val->product->discount);
													$total_t = $total_t - $discount_t;
												}
												$total_t = $total_t*$val->total;
												$total += $total_t;
												?>
												@endforeach
												<?php 
												$total = $total + $row->shipping_price+ ($total*10/100);
												?>
												{!! money_format('%(#10n', $total) !!}
											</td>
										</tr>
										@endforeach
										</tbody>
								</table>					
							</div>
												
							<div class="tab-pane fade" id="status">
										@foreach($data['content']->logActivity as $row)
						  				<li> <em><b>{!! date('d M Y',strtotime($row->created_at)) !!}</b> &nbsp;|&nbsp; </em> {!! $row->description !!}</li>
						  				@endforeach
									 			
							</div>						
						</div>	

					</div>										
				</div>								
			</div>	<!-- TAB USER -->

		</section> 
  
					  
			
			
 

<div class="modal fade" id="withdrawPoint" tabindex="-1" role="dialog" aria-labelledby="withdrawPoint1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="withdrawPoint1">Withdraw Bonus</h4>
      </div>
      <div class="modal-body">
        <h4>Maaf, Withdraw hanya dapat dilakukan pada akhir bulan.</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>


 @include('theme.keiskei.include.password')
@endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 

	    });
	    function withdrawModal () {
		    $('#withdrawPoint').modal({ backdrop: 'static', keyboard: false });
		    
		  }
	</script>

	<script type="text/javascript">

		//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});

	</script>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
 	background-color: rgba(255, 255, 255, 0.9);
}
</style>

@endsection

@section('custom-head')
	
@endsection