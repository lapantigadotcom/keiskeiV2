@extends('theme.keiskei.app')
<title>Keiskei Indonesia | Edit Profil Anggota</title>

@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Beauty | Health | Cure </h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-1 col-md-10 form-container">
						<div class="form-head text-center">
							<h4>Edit Profil</h4>
							<p>Perbarui informasi akun anggota.</p>
							<hr>
						</div>
						<div class="form-body">
							<div class="col-md-12">
								<div class="row">
									@include('errors.session')
								</div>
								<div class="row">
									@if($errors->any())
									<ul class="alert alert-danger">
										@foreach($errors->all() as $row)
										<li>
											{!! $row !!}
										</li>
										@endforeach
									</ul>

									@endif
								</div>
								{!! Form::model($data['content'],array('route' => 'user.profile.update', 'method' => 'PUT','files' => 'true','id' => 'form-validate')); !!}
								<div class="form-group">
									<div class="col-md-6">
										<div class="well well-sm">
											@if(File::exists('data/user/photo/'.$data['content']->photo)  and $data['content']->photo != '')
											<img src="{!! asset('data/user/photo/'.$data['content']->photo) !!}" class="img-responsive img-thumbnail">
											@else
											<img src="{!! asset('theme/keiskei/img/user.png') !!}" class="img-responsive img-thumbnail">
											@endif
											{!! Form::file('photo', array('class' => 'form-control')) !!}	
											<small>Kosongkan jika tidak ingin diganti.</small>
										</div>
									</div>
									<div class="col-md-6">
										{!! Form::label('code', 'Kode', array('class' => 'form-label')) !!}
										{!! Form::text('code',null, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
										{!! Form::label('username', 'Username', array('class' => 'form-label')) !!}
										{!! Form::text('username',null, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
										@if($data['content']->roles->first()->require_file=='1')
										{!! Form::label('company', 'Nama Perusahaan / Outlet *', array('class' => 'form-label')) !!}
										{!! Form::text('company',null, array('class' => 'form-control')) !!}

										@endif
										{!! Form::label('name', 'Nama *', array('class' => 'form-label')) !!}
										{!! Form::text('name',null, array('class' => 'form-control')) !!}
										{!! Form::label('email', 'Email *', array('class' => 'form-label')) !!}
										{!! Form::text('email',null, array('class' => 'form-control')) !!}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6">
										{!! Form::label('telephone', 'Tlp. *', array('class' => 'form-label')) !!}
										{!! Form::text('telephone',null, array('class' => 'form-control')) !!}
									</div>
									<div class="col-md-6">
										{!! Form::label('mobile', 'No. HP (Untuk Verifikasi SMS)', array('class' => 'form-label')) !!}
										{!! Form::text('mobile',null, array('class' => 'form-control')) !!}
									</div>

									<div class="col-md-6">
										{!! Form::label('pinbb', 'PIN BB', array('class' => 'form-label')) !!}
										{!! Form::text('pinbb',null, array('class' => 'form-control')) !!}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										{!! Form::label('gender', 'Jenis Kelamin', array('class' => 'form-label')) !!}
										{!! Form::select('gender',['M' => 'Male', 'F' => 'Female'],null, array('class' => 'form-control')) !!}
									</div>
								</div>
								<div class="form-group">
									@if($data['content']->ms_country_id == $data['general']->ms_country_id)
									<div class="col-md-6">
										<label for="ms_country_id">
											Negara
										</label>
										{!! Form::select('ms_country_id',$data['country'],null,array('class' => 'form-control', 'disabled' => 'disabled')) !!}
									</div>
									<div class="col-md-6">

										<label for="ms_city_id">
											Kota
										</label>
										{!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id')) !!}

									</div>
									@else
									<div class="col-md-12">
										<label for="ms_country_id">
											Negara
										</label>
										{!! Form::select('ms_country_id',$data['country'],null,array('class' => 'form-control', 'disabled' => 'disabled')) !!}
									</div>
									@endif
								</div>
								<div class="form-group">
									<div class="col-md-12">
										{!! Form::label('address', 'Alamat *', array('class' => 'form-label')) !!}
										{!! Form::textarea('address',null, array('class' => 'form-control', 'rows' => '2')) !!}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										{!! Form::label('description', 'Deskripsi Diri', array('class' => 'form-label')) !!}
										{!! Form::textarea('description',null, array('class' => 'form-control', 'rows' => '2')) !!}
									</div>
								</div>
								<div class="row" style="padding-top:15px;">
									<div class="col-md-12">
										<h4 class="text-center">Akun Rekening</h4>
									</div>
									<div class="col-md-12">
									<div class="col-md-4">
										<div class="form-group {!! $errors->any()?($errors->first('ms_bank_id')?' has-error':''):'' !!}">
											<div class="col-md-4">
												{!! Form::label('ms_bank_id','Pilih Bank', array('class' => 'control-label')) !!}
											</div>
											<div class="col-md-8">
												{!! Form::select('ms_bank_id', $data['bank'], null, array('class' => 'form-control')) !!}
												<span class="help-block">{!! $errors->any()?($errors->first('ms_bank_id')?$errors->first('ms_bank_id'):''):'' !!}</span>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="col-md-3">
												{!! Form::label('account_name', 'Akun Bank *', array('class' => 'form-label')) !!}
											</div>
											<div class="col-md-9">
												{!! Form::text('account_name',null, array('class' => 'form-control')) !!}
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<a href="javascript:void(0)" class="btn btn-keiskei-orange" onclick="addRek()">Tambah Rekening</a>
									</div>
									<div class="col-md-12 text-center">
										<span class="help-block" style="color:red;" id="error-bank"><em>* Wajib diisi</em></span>
									</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">

										<table class="table">
											<thead>
												<tr>
													<th>Nama Bank</th>
													<th>Akun Bank</th>
													<th>Opsi</th>
												</tr>
											</thead>
											@foreach($data['content']->accountBank as $row)
											<tr>
												<td>{!! $row->bank->name !!}</td>
												<td>{!! $row->account_name !!}</td>
												<td>
													<a href="{!! route('user.account_bank.delete', [ $row->id ]) !!}" class="fa fa-trash"> Hapus</a>
												</td>
											</tr>
											@endforeach
											@if($data['content']->accountBank()->count() == 0)
											<tr>
												<td colspan="3">
													Akun Rekening Kosong
												</td>
											</tr>
											@endif
										</table>
									</div>
								</div>
							</div>
							<div class="form-group text-align form-action">
								{!! Form::submit('Simpan',array('class' => 'btn  btn-keiskei')) !!}
								<a href="{!! route('user.dashboard') !!}" class="btn btn-default">Kembali</a>
							</div>
							{!! BootstrapForm::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection

@section('custom-footer')
{!! HTML::script('theme/keiskei/plugin/validation/jquery.validate.min.js') !!}
{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}

<script type="text/javascript">
	$("#ms_city_id").selectize();
	$('#error-bank').hide();
	$('#form-validate').validate({
		errorClass : "help-block error",
		rules: {
			name : {
				required: true,
				minlength: 3
			},
			company : {
				required: true,
				minlength: 3
			},
			telephone : {
				required: true,
				minlength: 3
			},
			account_bank : {
				required: true,
				minlength: 3
			},
			email :{
				required: true,
				email : true
			}
		},
		messages: {
			name : {
				required : "Mohon masukkan username Anda",
				minlength : $.validator.format("Username Anda harus minimal {0} karakter")
			},
			company : {
				required : "Mohon masukkan perusahaan Anda",
				minlength : $.validator.format("Nama Perusahaan Anda harus minimal {0} karakter")
			},
			telephone : {
				required : "Mohon masukkan telepon Anda",
				minlength : $.validator.format("telepon Anda harus minimal {0} karakter")
			},
			account_bank : {
				required : "Mohon masukkan akun bank Anda",
				minlength : $.validator.format("No Rek Anda harus minimal {0} karakter")
			},

			email : {
				required : "Mohon masukkan email Anda",
				email : "Mohon cek kembali alamat email Anda"
			}
		}
	});
	function addRek()
	{
		var bank_id = $('#ms_bank_id').val();
		var account_name = $('#account_name').val();
		if(account_name == '')
		{
			$('#error-bank').show();
		}else{
			window.location.replace("{!! route('user.account_bank.store') !!}?bank_id=" + bank_id + "&account_name="+ account_name);
		}
	}
	</script>
	@endsection

	@section('custom-head')
	{!! HTML::style('theme/keiskei/css/jquery-ui.css') !!}
	{!! HTML::script('theme/keiskei/js/jquery-ui.js') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
	<script>
		$(function() {
			$( "#datepicker" ).datepicker({"dateFormat": "yy-mm-dd" });
		});
		
	</script>
	@endsection