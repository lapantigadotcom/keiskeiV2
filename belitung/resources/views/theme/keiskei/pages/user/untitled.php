					<section id="form"><!--form-->
							<div class="container">
								<div class="row">
									<div class="col-sm-4 col-sm-offset-1">
										<div class="login-form"><!--login form-->
											<h2>Daftar Akun <span style="color:green"><b>Smart System</b></span></h2>
											{!! BootstrapForm::open(array('store' => 'register.store','id' => 'form-validate')) !!}
											@if(!empty($data['affiliate_token']))
											{!! Form::hidden('referraled_by',$data['affiliate_token']) !!}
											@endif

											{!! BootstrapForm::open(array('store' => 'register.store','id' => 'form-validate')) !!}
										@if(!empty($data['affiliate_token']))
										{!! Form::hidden('referraled_by',$data['affiliate_token']) !!}
										@endif
										
										{!! BootstrapForm::text('username') !!}
										{!! BootstrapForm::text('mobile','Nomor Handphone (Untuk Aktivasi)') !!}
										{!! BootstrapForm::text('email') !!}
										<div class="register-req">
										<div class="col-sm-5 clearfix">
											<div class="bill-to">
												<p>Kurir</p>
													<div class="form-one">
										{!! Form::select('ms_country_id',$data['country'],null,array('class' => 'form-control','id' => 'ms_country_id')) !!}
													</div>
											</div>		
										</div></div>

										{!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id')) !!}
											<center>
											<div class="form-group">
											{!! Recaptcha::render() !!}
											<span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>
											</div></center>


											{!! Form::submit('Bergabung',array('class' => 'btn btn-primary')) !!}
					 					</div><!--/login form-->{!! BootstrapForm::close() !!}
									</div>
									<div class="col-sm-1">
					 				</div>
									<div class="col-sm-4">
										<div class="signup-form"><!--sign up form-->
										<img src="{{ asset('theme/keiskei_V2/images/banner/banner1.jpeg') }}" style="max-height:450px" class="img-lazy" alt="keiskei 17 Agustus">

										</div><!--/sign up form-->
									</div>
								</div>
							</div>
					</section><!--/form-->