@extends('theme.keiskei.app')

<title>Keiskei Indonesia | Verifikasi Keanggotaan </title>

@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Silahkan Pilih tipe Anggota yang Anda kehendaki</h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-offset-3 col-md-6 form-container">
						<div class="form-head text-center">
							<h4>Verifikasi Akun</h4>
							<p></p>
							<hr>
						</div>
						<div class="form-body">
						{!! Form::model($data['content'],array('route' => ['user.verified.update',$data['content']->id], 'method' => 'PUT','files' => 'true')); !!}

							<div class="form-group">
                                {!! Form::label('type','Tipe Keanggotaan') !!}
                                {!! Form::select('type',[ '1' => 'Customer', '2' => 'Partner' ],isset($data['content'])?$data['content']->roles->first()->id:null, array('class' => 'form-control','id' => 'type')) !!}
                            </div>
                            <div class="form-group" id="c_role_id">
                                {!! Form::label('role_id','Pilih Group Keanggotaan') !!}
                                {!! Form::select('role_id',$data['perusahaan'],isset($data['content'])?$data['content']->roles->first()->id:null, array('class' => 'form-control','id' => 'role_id')) !!}
                            </div>
                            <span class="help-block">{!! $errors->any()?($errors->first('ms_role_id')?$errors->first('ms_role_id'):''):'' !!}</span>
                            {!! Form::hidden('ms_role_id',null, array('id' => 'ms_role_id')) !!}
                            <div class="form-group">
                            	{!! Form::label('username','Username') !!}
                            	{!! Form::text('username',null,array('class' => 'form-control', 'disabled' => true)) !!}
                            </div>
                            <div class="form-group">
                            	{!! Form::label('email','Email') !!}
                            	{!! Form::text('email',null,array('class' => 'form-control', 'disabled' => true)) !!}
                            </div>
                        <div id="require_file" class="" style="margin-bottom:10px;">
                        	<h4 class="text-center">
                        		Kelengkapan Tambahan
                        	</h4>
                        	<hr>
                        	<div class="form-group {!! $errors->any()?($errors->first('company')?' has-error':''):'' !!}">
                        		{!! Form::label('company', 'Nama Perusahaan / Outlet / Partner', array('class' => 'form-label')) !!}
                        		{!! Form::text('company',null, array('class' => 'form-control')) !!}
                        		<span class="help-block" style="color:red;"><em>* Wajib</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('company')?$errors->first('company'):''):'' !!}</span>
                        	</div>
                        	<div class="form-group {!! $errors->any()?($errors->first('ms_bank_id')?' has-error':''):'' !!}">
			                    	{!! Form::label('ms_bank_id','Pilih Bank', array('class' => 'control-label')) !!}
			                    	{!! Form::select('ms_bank_id', $data['bank'], null, array('class' => 'form-control')) !!}
		                        <span class="help-block">{!! $errors->any()?($errors->first('ms_bank_id')?$errors->first('ms_bank_id'):''):'' !!}</span>
		                    </div>
		                    <div class="form-group {!! $errors->any()?($errors->first('account_bank')?' has-error':''):'' !!}">
			                    	{!! Form::label('account_bank','Akun Bank', array('class' => 'control-label')) !!}
			                    	{!! Form::text('account_bank',null, array('class' => 'form-control')) !!}
			                    <span class="help-block" style="color:red;"><em>* Wajib</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('account_bank')?$errors->first('account_bank'):''):'' !!}</span>
		                    </div>
	                        <div class="form-group {!! $errors->any()?($errors->first('ktp')?' has-error':''):'' !!}">
		                        {!! Form::label('ktp','KTP', array('class' => 'control-label')) !!}
		                        {!! Form::file('ktp', array('class' => 'form-control')) !!}
		                        @if($data['content']->userFiles()->count()>0)
		                        	{!! empty($data['content']->userFiles->ktp)?'':"<a href='".asset('data/user/'.$data['content']->userFiles->ktp)."'>download</a>" !!}
		                        @endif
		                        <span class="help-block" style="color:red;"><em>* Wajib</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('ktp')?$errors->first('ktp'):''):'' !!}</span>
		                    </div>
		                    
		                    <div class="form-group {!! $errors->any()?($errors->first('siup')?' has-error':''):'' !!}">
	                        		{!! Form::label('siup', 'SIUP', array('class' => 'control-label')) !!}
	                        		{!! Form::file('siup', array('class' => 'form-control')) !!}
	                        	@if($data['content']->userFiles()->count()>0)
		                        	{!! empty($data['content']->userFiles->siup)?'':"<a href='".asset('data/user/'.$data['content']->userFiles->siup)."'>download</a>" !!}
		                        @endif
		                        <span class="help-block" style="color:orange;"><em>* Wajib, jika Anda memiliki perusahaan.</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('siup')?$errors->first('siup'):''):'' !!}</span>
	                        </div>
	                        <div class="form-group {!! $errors->any()?($errors->first('tdp')?' has-error':''):'' !!}">
		                        	{!! Form::label('tdp','TDP', array('class' => 'control-label')) !!}
		                        	{!! Form::file('tdp', array('class' => 'form-control')) !!}
		                        @if($data['content']->userFiles()->count()>0)
		                        	{!! empty($data['content']->userFiles->tdp)?'':"<a href='".asset('data/user/'.$data['content']->userFiles->tdp)."'>download</a>" !!}
		                        @endif
		                        <span class="help-block" style="color:orange;"><em>* Wajib, jika Anda memiliki perusahaan.</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('tdp')?$errors->first('tdp'):''):'' !!}</span>
		                    </div>
		                    <div class="form-group {!! $errors->any()?($errors->first('npwp')?' has-error':''):'' !!}">
		                        	{!! Form::label('npwp','NPWP', array('class' => 'control-label')) !!}
		                        	{!! Form::file('npwp', array('class' => 'form-control')) !!}
		                        @if($data['content']->userFiles()->count()>0)
		                        	{!! empty($data['content']->userFiles->npwp)?'':"<a href='".asset('data/user/'.$data['content']->userFiles->npwp)."'>download</a>" !!}
		                        @endif
		                        <span class="help-block" style="color:orange;"><em>* Wajib, jika Anda memiliki perusahaan.</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('npwp')?$errors->first('siup'):''):'' !!}</span>
	                        </div>
	                        <div class="form-group {!! $errors->any()?($errors->first('akte')?' has-error':''):'' !!}">
	                        	{!! Form::label('akte','Akte', array('class' => 'control-label')) !!}
	                        	{!! Form::file('akte', array('class' => 'form-control')) !!}
	                        	@if($data['content']->userFiles()->count()>0)
		                        	{!! empty($data['content']->userFiles->akte)?'':"<a href='".asset('data/user/'.$data['content']->userFiles->akte)."'>download</a>" !!}
		                        @endif
		                        <span class="help-block" style="color:orange;"><em>* Wajib, jika Anda memiliki perusahaan.</em></span>
		                        <span class="help-block">{!! $errors->any()?($errors->first('akte')?$errors->first('akte'):''):'' !!}</span>
	                        </div>

                        </div>
						<div class="form-group text-align form-action" style="">
							{!! BootstrapForm::submit('Simpan',array('class' => 'btn btn-keiskei')) !!}
						</div>
						{!! BootstrapForm::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$("#type").selectize();
	    	$("#role_id").selectize();
	      	$("#role_id").change(function(){
	      		var role = $("#role_id").val();
	      		changeRole(role);
	      	});
	      	$("#type").change(function(){
	      		var type = $("#type").val();
	      		changeType(type);
	      	});
	      	$(':file').filestyle({size: "sm",buttonText: "Masukkan File"});
	    });
	    $(function () {
        	var role = $("#role_id").val();
        	changeRole(role);
        	$('#require_file').hide();
        	var type = $('#type').val();
        	changeType(type);
        	console.log(type);
      	});
      	function changeType(x)
      	{
      		switch(x){
        		case '1':
        			var role = $("#role_id").val();
        			changeRole(role);
	        		$("#c_role_id").hide();
	        		var default_t = {!! $data['perorangan'] !!};
	        		$('#ms_role_id').val(default_t);
	        		console.log($('#ms_role_id').val());
	        		break;
	        	case '2':
	        		var role = $("#role_id").val();
        			changeRole(role);
	        		$("#c_role_id").show();
	        		break;
        	}
      	}
      	function changeRole(x)
      	{
      		var type = $('#type').val();
      		if(type == 1)
      		{
      			$("#require_file").hide();
      			return;
      		}
      		$.ajax(
		   	{
		   		url:"{{ route('ki-admin.role.getjson') }}",
		   		type: "GET",
		   		data:{
			      id : x
			    }
			}).done(function(data,status){
				if(status=='success')
			    {	
			    	console.log(data);
			    	var require = data.require_file;
			    	if(require=='1')
			    	{
			    		$('#require_file').show();
			    	}else
			    	{
			    		$('#require_file').hide();
			    	}
			    	$('#ms_role_id').val(x);
	        		console.log($('#ms_role_id').val());
			    	
			    }
			});

      	}
	</script>
@endsection

