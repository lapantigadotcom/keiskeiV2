<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="col-md-12">
		@if(isset($hot_product))
			<div class="row">
				<div class="col-md-12 main-our-product">
						<h2 class="title text-center">Produk Keiskei Terpopuler</h2>
				</div>
			</div>
			<div class="row">
				 
				    <div class="owl-carousel">
				  @foreach($hot_product as $row)

								<div class="single-products" style="margin:10px">
									@if($row->productImages()->count() > 0)
									@if(File::exists($row->productImages->first()->image))
										<div class="productinfo text-center">

				      		<img class="lazyOwl img-responsive" src="{!! asset($row->productImages->first()->image) !!}"  title="{!! $row->title !!} | {!! ucwords($row->categoryproduct->name) !!}" > 
 						@endif
							@endif
											<h2>{!! ucwords($row->categoryproduct->name) !!}</h2>
											<p>{!! $row->title !!}</p>
											<a href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Lihat</a>
										</div>
										 
								</div>				    @endforeach
				    </div>
				 
			</div>
			@endif
		</div>
	</div>
</div>
