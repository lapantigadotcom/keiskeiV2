 

	<footer id="footer"><!--Footer-->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2><span>K</span>-Partners</h2>
							<p>Video Review dan Produk Keiskei Indonesia</p>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="https://www.youtube.com/watch?v=xuutXcgyThg" target="_blank">
									<div class="iframe-img">
										<img src="{{ asset('theme/keiskei_V2/images/video/thumbs1.jpg') }}" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Testimoni 1</p>
								<h2>Jun 8, 2015</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="https://www.youtube.com/watch?v=MfkaRB_TV3c" target="_blank">
									<div class="iframe-img">
										<img src="{{ asset('theme/keiskei_V2/images/video/thumbs6.jpg') }}" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Power Speaking</p>
								<h2>Oct 16, 2015</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="https://www.youtube.com/watch?v=trLk7zeM8kQ" target="_blank">
									<div class="iframe-img">
										<img src="{{ asset('theme/keiskei_V2/images/video/thumbs7.jpg') }}" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Promo Keiskei 17++</p>
								<h2>Nov 13, 2015</h2>
							</div>
						</div>
						
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="https://www.youtube.com/watch?v=84m8Ftc9I9s">
									<div class="iframe-img">
										<img src="{{ asset('theme/keiskei_V2/images/video/thumbs2.jpg') }}" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Premium Produk</p>
								<h2>Jun 15, 2015</h2>
							</div>
						</div>


					</div>
					<div class="col-sm-3">
							<div class="video-gallery text-center" >
								<a href="#">
									<div class="iframe-img" style="border:0">
										<img src="{{ asset('theme/keiskei_V2/images/plastore-get.png') }}" alt="" />
									</div>
									 
								</a>
								<p>Keiskei SmartSystem</p>
								<h2>Android App</h2>
							</div>
						</div>
				</div>
			</div>
		</div>
		
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Support</h2>
							<ul class="nav nav-pills nav-stacked">
								@if(isset($articles[4]['id']))<li><a href="{!! route('articles',[$articles[4]['id'].'-'.$articles[4]['title']]) !!}">Kebijakan Privasi</a></li>@endif
				@if(isset($articles[5]['id']))<li><a href="{!! route('articles',[$articles[5]['id'].'-'.$articles[5]['title']]) !!}">FAQ</a></li>@endif
				@if(isset($articles[6]['id']))<li><a href="{!! route('articles',[$articles[6]['id'].'-'.$articles[6]['title']]) !!}">Keanggotaan</a></li>@endif
			
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Tentang Kami</h2>
							<ul class="nav nav-pills nav-stacked">
								 @if(isset($articles[7]['id']))<li><a href="{!! route('articles',[$articles[7]['id'].'-'.$articles[7]['title']]) !!}">Peta Situs</a></li>@endif
				@if(isset($articles[3]['id']))<li><a href="{!! route('articles',[$articles[3]['id'].'-'.$articles[3]['title']]) !!}">Dukungan</a></li>@endif
				@if(isset($articles[8]['id']))<li><a href="{!! route('articles',[$articles[8]['id'].'-'.$articles[8]['title']]) !!}">Karir</a></li>@endif
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Panduan</h2>
							<ul class="nav nav-pills nav-stacked">
							@if(isset($articles[11]['id']))<li><a href="{!! route('articles',[$articles[11]['id'].'-'.$articles[11]['title']]) !!}">Pendaftaran</a></li>@endif
								@if(isset($articles[9]['id']))<li><a href="{!! route('articles',[$articles[9]['id'].'-'.$articles[9]['title']]) !!}">Pemesanan</a></li>@endif
								@if(isset($articles[10]['id']))<li><a href="{!! route('articles',[$articles[10]['id'].'-'.$articles[10]['title']]) !!}">Pembayaran</a></li>@endif
								@if(isset($articles[12]['id']))<li><a href="{!! route('articles',[$articles[12]['id'].'-'.$articles[12]['title']]) !!}">Konfirmasi</a></li>@endif
 							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Produk</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#">Hair Care</a></li>
								<li><a href="#">Health Care</a></li>
								<li><a href="#">Package</a></li>
								 
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							 							  <img src="{!! asset('theme/keiskei_V2/images/pom.png') !!}" alt="">
<script language="JavaScript" type="text/javascript">
				TrustLogo("{!! asset('theme/keiskei_V2/images/comodo-keiskei.gif') !!}", "CL1", "none");
				</script>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
 							  <img src="{!! asset('theme/keiskei_V2/images/logobank.png') !!}" alt="">
							  <img src="{!! asset('theme/keiskei_V2/images/logoshipping.png') !!}" alt="">


						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright &copy; PT. Punyakunik Maju Jaya. All Rights Reserved.  </p>
 									 	<span class="pull-right" style="color:#fff;float: right; font-weight: normal; max-width: 470px;"><marquee scrolldelay="90"><i class="fa fa-chatt"></i>&nbsp;{!! $general->news_roll !!}</marquee></span>

 				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	        {!! HTML::script('theme/keiskei_V2/js/jquery.js') !!}
{!! HTML::script('theme/keiskei_V2/js/owl/owl.carousel.js') !!}
	        {!! HTML::script('theme/keiskei_V2/js/owl/owl.carousel.min.js') !!}
            {!! HTML::script('theme/keiskei_V2/js/bootstrap.min.js') !!}
    {!! HTML::script('theme/keiskei_V2/js/jquery.scrollUp.min.js') !!}
 {!! HTML::script('theme/keiskei_V2/js/price-range.js') !!}
  {!! HTML::script('theme/keiskei_V2/js/jquery.prettyPhoto.js') !!}
   {!! HTML::script('theme/keiskei_V2/js/main.js') !!}

     <script type="text/javascript">
 $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})</script>

@yield('custom-footer')