{!! HTML::style('theme/keiskei_V2/css/bootstrap.min.css') !!}
{!! HTML::script('theme/keiskei_V2/css/price-range.css') !!}
{!! HTML::style('theme/keiskei_V2/css/animate.css') !!}
{!! HTML::style('theme/keiskei_V2/css/prettyPhoto.css') !!}
{!! HTML::style('theme/keiskei_V2/css/font-awesome.min.css') !!}
{!! HTML::style('theme/keiskei_V2/css/main.css') !!}
{!! HTML::style('theme/keiskei_V2/js/owl/owl.carousel.css') !!}
{!! HTML::style('theme/keiskei_V2/js/owl/owl.theme.css') !!}

@yield('custom-head')
 