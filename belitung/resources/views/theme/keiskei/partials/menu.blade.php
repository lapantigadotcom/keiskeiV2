<!-- COMODO SSL KEISKEI -->
<script type="text/javascript">
//<![CDATA[
var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "https://www.trustlogo.com/");
document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
//]]>
</script>
<style type="text/css">
	.goog-te-banner-frame.skiptranslate {
		display: none !important;
	} 
	body {
		top: 0px !important; 
	}

	.goog-te-gadget-simple
	{
		display: none !important;
	} 

	.goog-tooltip {
		display: none !important;
	}
	.goog-tooltip:hover {
		display: none !important;
	}
	.goog-text-highlight {
		background-color: transparent !important;
		border: none !important; 
		box-shadow: none !important;
	}

</style>

<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> {!! $general->telephone !!}
</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> {!! $general->email !!}</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
							<li><a href="https://www.facebook.com/KeiskeiIndonesia?ref=hl" target="_blank"><i  title="FB: keiskeiashitaba" class="fa fa-facebook-square fa-lg"  ></i></a></li>
							<li><a href="https://twitter.com/keiskeiashitaba" target="_blank"><i  title="@keiskeiashitaba" class="fa fa-twitter-square fa-lg"  ></i></a></li>
							<li><a href="https://plus.google.com/101701632456013203883/posts" target="_blank"><i  title="keiskeiashitaba" class="fa fa-google-plus fa-lg" ></i></a></li>
							<li><a href="https://id.linkedin.com/pub/keiskei-indonesia/b7/1/b90" target="_blank"><i  title="keiskei-indonesia" class="fa fa-linkedin fa-lg"  ></i></a></li>
							<li><a href="https://instagram.com/keiskeiindonesia/" target="_blank"><i title="keiskei_indonesia" class="fa fa-instagram fa-lg" ></i></a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
						<a href="{!! URL::to('/') !!}">
						<img style="max-height:60px" src="{!! asset('theme/keiskei_V2/images/logo.png') !!}" class="img-responsive hidden-xs main-logo">
					</a>						</div>
						<div class="btn-group pull-right">
							<div class="btn-group">
								 <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
<a href="#" class="notranslate" data-lang="Indonesian">
							<img src="{!! asset('theme/keiskei/img/id.png') !!}"></button>
								  <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
<a href="#" class="English" data-lang="English">
							<img src="{!! asset('theme/keiskei/img/en.png') !!}">
						</a></button>
											<!-- Code provided by Google -->
					<div id="google_translate_element"></div>
					<script type="text/javascript">
						function googleTranslateElementInit() {
							new google.translate.TranslateElement({pageLanguage: 'id', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
						}
					</script>
					<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit" type="text/javascript"></script>

					<!-- Flag click handler -->
					<script type="text/javascript">
						$('.translation-links a').click(function() {
							var lang = $(this).data('lang');
							var $frame = $('.goog-te-menu-frame:first');
							if (!$frame.size()) {
								alert("Error: Could not find Google translate frame.");
								return false;
							}
							$frame.contents().find('.goog-te-menu2-item span.text:contains('+lang+')').get(0).click();
							return false;
						});
					</script>			

							</div>
						  <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
<a href="#" class="Japanese" data-lang="Japanese">
							<img src="{!! asset('theme/keiskei/img/jp.png') !!}">
						</a></button>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">			
				 
								@if(Auth::check() == true)
								<li><a href="{!! route('user.dashboard') !!}"><i class="fa fa-dashboard fa-lg"></i> My Keiskei <i style="font-size:11px;color:red">(View Profile)</i></a></li>
								<li><a href="{!! route('user.logout') !!}"><i class="fa fa-power-off fa-lg"></i> Log Out</a></li>
								@else
								<li><a href="{!! route('login') !!}"><i class="fa fa-lock"></i> Login</a></li>
								<li><a href="{!! route('register') !!}"><i class="fa fa-user"></i> Pendaftaran</a></li>
								@endif
								<li><a href="{{ url('/password/email') }}"><i class="fa fa-key"></i> Reset Password</a></li>
								<li><a href="{!! route('contact-us') !!}"><i class="fa fa-envelope-o"></i> Hubungi Kami</a></li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="{!! URL::to('/') !!}" class="active">Home</a></li>
								@if(isset($articles[0]['id']))<li><a href="{!! route('articles',[$articles[0]['id'].'-'.$articles[0]['title']]) !!}">Tentang Kami</a></li> @endif
								<li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}">Produk</a></li>

								<li class="dropdown"><a href="#">Panduan<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
								@if(isset($articles[11]['id']))<li><a href="{!! route('register') !!}">Pendaftaran</a></li>@endif
								@if(isset($articles[9]['id']))<li><a href="{!! route('articles',[$articles[9]['id'].'-'.$articles[9]['title']]) !!}">Pemesanan</a></li>@endif
								@if(isset($articles[10]['id']))<li><a href="{!! route('articles',[$articles[10]['id'].'-'.$articles[10]['title']]) !!}">Pembayaran</a></li>@endif
								@if(isset($articles[12]['id']))<li><a href="{!! route('articles',[$articles[12]['id'].'-'.$articles[12]['title']]) !!}">Konfirmasi</a></li>@endif
								@if(isset($articles[13]['id']))<li><a href="{!! route('articles',[$articles[13]['id'].'-'.$articles[13]['title']]) !!}">Withdraw</a></li>@endif
    								</ul>
 										<li><a href="{!! route('blogs') !!}">Blog</a></li>
                                </li> 
                        		@if(isset($articles[2]['id']))<li><a href="{!! route('articles',[$articles[2]['id'].'-'.$articles[2]['title']]) !!}">Bisnis K-Partner</a></li>@endif

						<li><a href="{!! route('partner-area') !!}">Partner Area</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class=" pull-right">
							  <a href="{!! route('user.cart') !!}" style="color:#0C3121;font-size:16px"><i class="fa fa-shopping-cart"></i> Cart
							  	 </a> 

 						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->