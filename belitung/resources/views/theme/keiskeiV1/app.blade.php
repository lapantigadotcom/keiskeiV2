<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>
 
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,900,700italic,500italic' rel='stylesheet' type='text/css'>

 
  @include('theme.keiskei.partials.head')
  
</head>
<body>	 		

 		
			<!-- Container -->
			<div class="container">
			@include('theme.keiskei.partials.menu')
			@yield('content')
	 		@include('theme.keiskei.partials.footer')
	</div>
  @include('theme.keiskei.partials.script')		
</body>
</html>