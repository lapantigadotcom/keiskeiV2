<!-- News -->
			<div class="products-row row">
				
				<!-- Carousel Heading -->
				<div class="col-lg-12 col-md-12 col-sm-12">
					
					<div class="carousel-heading">
						<h4>Blog Artikel</h4>
						<div class="carousel-arrows">
							<i class="icons icon-left-dir"></i>
							<i class="icons icon-right-dir"></i>
						</div>
					</div>
					
				</div>
				<!-- /Carousel Heading -->
				

				<!-- Carousel -->
				<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
					
					<div class="owl-carousel" data-max-items="2">
							
						<!-- Slide -->
						<div>
							<!-- Carousel Item -->
				@if(!empty($data['latest_one']))			

							<article class="news">
								
								<div class="news-background">
								
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 news-thumbnail">
											<a href="#"><img src="img/news/sample1.jpg" alt="News1"></a>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 news-content">
											<h5><a href="blog_post.html">{!! strlen($data['latest_one']->title > 20)?substr($data['latest_one']->title,0,20).'...':$data['latest_one']->title !!}</a></h5>
											<span class="date"><i class="icons icon-clock-1"></i> {!! date('l, j F Y') !!} </span>
											<p>{!! strlen(strip_tags($data['latest_one']->description))>150?substr(strip_tags($data['latest_one']->description),0,150).'...':strip_tags($data['latest_one']->description) !!}</p>
										</div>
									</div>
									
								</div>
								@endif
							</article>
							<!-- /Carousel Item -->
						</div>
						<!-- /Slide -->
						 
					</div>
				
				</div>
				<!-- /Carousel -->
				
			</div>
			<!-- /News -->