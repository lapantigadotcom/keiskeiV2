<!-- New Collection -->
					<div class="products-row row">
						
						<!-- Carousel Heading -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="carousel-heading">
								<h4>Produk Baru</h4>
								<div class="carousel-arrows">
									<i class="icons icon-left-dir"></i>
									<i class="icons icon-right-dir"></i>
								</div>
							</div>
							
						</div>
						<!-- /Carousel Heading -->
						
						<!-- Carousel -->
						<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
							
							<div class="owl-carousel" data-max-items="3">
							<!-- Slide -->@foreach($data['latest'] as $row)
							@if($row->productImages()->count() > 0)
							@if(File::exists($row->productImages->first()->image))
									<div>
										 <!-- Carousel Item -->
							
							<div class="product">
								<div class="product-image">
												<span class="product-tag">{!! ucwords($row->categoryproduct->name) !!}</span>
												<img src="{!! asset($row->productImages->first()->image) !!}" alt="Lazy Owl Image">
												<a href="{!! route('user.product.detail',$row->code, $row->title) !!}" class="product-hover">
													<i class="icons icon-eye-1"></i> Quick View
												</a>
											</div>
												@endif
												@endif
											<div class="product-info">
												<h5><a href="products_page_v1.html">{!! $row->title !!}</a></h5>
												<span class="price">$281.00</span>
												<div class="rating readonly-rating" data-score="4"></div>
											</div>
											
											<div class="product-actions">
												<span class="add-to-cart">
													<span class="action-wrapper">
														<i class="icons icon-basket-2"></i>
														<span class="action-name">Add to cart</span>
													</span >
												</span>
												<span class="add-to-favorites">
													<span class="action-wrapper">
														<i class="icons icon-heart-empty"></i>
														<span class="action-name">Add to wishlist</span>
													</span>
												</span>
												<span class="add-to-compare">
													<span class="action-wrapper">
														<i class="icons icon-docs"></i>
														<span class="action-name">Add to Compare</span>
													</span>
												</span>
											</div>
											
										</div>
										<!-- /Carousel Item -->
									</div>
									<!-- /Slide -->@endforeach
									 
							</div>
						</div>
						<!-- /Carousel -->
						
					</div>
					<!-- /New Collection -->