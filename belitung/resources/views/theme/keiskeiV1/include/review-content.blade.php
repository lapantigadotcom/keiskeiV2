<!-- Modal -->
<div class="modal fade" id="{!! $id !!}" tabindex="-1" role="dialog" aria-labelledby="reviewLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="reviewLabel">Daftar Ulasan Produk</h4>
      </div>
      <div class="modal-body">
        @foreach($reviews as $review)
                <div class="review-detail">
                  <div class="review-item">
                    <div class="review-item-head">
                      <div class="row">
                        <div class="col-md-8 text-left review-item-inline">
                          <span>{!! $review->user->name !!}</span>
                          <i class="fa fa-clock-o"></i> {!! date('g:i a', strtotime($review->created_at)) !!} <i class="fa fa-bell-o"></i> {!! date('l', strtotime($review->created_at)) !!}
                          <i class="fa fa-calendar-o"></i>  {!! date('j F', strtotime($review->created_at)) !!} 
                        </div>
                        <div class="col-md-4 text-right">
                          <div class="rate">
                            <div class="star">
                            @for($i=1; $i<=intval($review->rate); $i++ )
                            <i class="fa fa-star fa-lg"></i>
                            @endfor
                            @for($i=intval($review->rate); $i<=5; $i++ )
                            <i class="fa fa-star-o fa-lg"></i>
                            @endfor
                          </div>
                          </div>
                        </div>
                      </div>
                      
                    </div>
                    <div class="review-item-body">
                      <p>
                        {!! $review->description !!}
                      </p>
                    </div>
                  </div>
                </div>
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>