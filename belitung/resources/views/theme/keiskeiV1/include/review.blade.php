<!-- Modal -->
<div class="modal fade" id="{!! $id !!}" tabindex="-1" role="dialog" aria-labelledby="reviewLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="reviewLabel">Tulis Ulasan Produk</h4>
      </div>
      <div class="modal-body">
      @if(Auth::check() && Auth::user()->roles->first()->locked != '1')
        
        @if(Auth::user()->arrProduct($idP))
        {!! Form::open(array('route' => ['user.review.store', $idP], 'method' => 'POST')) !!}
        <div class="form-group">
          {!! Form::label('rate','Rate', array('class' => 'form-label')) !!}
          {!! Form::selectRange('rate', 5, 1, null,array('class' => 'form-control') ) !!}
        </div>
        {!! BootstrapForm::textarea('description','Isi Review', null, array('rows' => '2', 'placeholder' => 'Tulis review anda untuk produk ini')) !!}
        <div class="form-group">
          {!! Form::submit('Tambahkan', array('class' => 'btn btn-keiskei')) !!}
        </div>
        {!! Form::close() !!}
        @else
        <p>
Untuk bisa memberikan ulasan, silahkan membeli produk terlebih dahulu        </p>
        @endif
      @else
      <p>
        Ulasan bisa ditambahan setelah Anda <a href="{!! route('login') !!}">login</a> terlebih dahulu.
      </p>
      @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>