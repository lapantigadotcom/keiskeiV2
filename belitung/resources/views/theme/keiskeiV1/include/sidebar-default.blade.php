<div class="sidebar hidden-xs hidden-sm">
	<div class="sidebar-head">
		<i class="fa fa-reorder"></i>&nbsp; | &nbsp; KEISKEI Main Menu
	</div>
	<div class="sidebar-body">
		<ul>
			<li class="{!! Request::is('/')?'active':'' !!}"><a href="{!! URL::to('/') !!}"><i class="fa fa-caret-right"></i>&nbsp;Home</a></li>
			<li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}"><i class="fa fa-caret-right"></i>&nbsp;Produk</a></li>
			<li><a href="{!! route('contact-us') !!}"><i class="fa fa-caret-right"></i>&nbsp;Hubungi Kami</a></li>
		</ul>
	</div>
</div>


<div class="sidebar hidden-xs hidden-sm">
	<div class="sidebar-head ">
		<i  class="fa fa-rss"></i>&nbsp; | &nbsp; BLOG TERBARU
	</div>
	<div class="sidebar-body">
		<ul>
		@if(isset($latest_article))
		@foreach($latest_article as $row)
			<li>
				<a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><i class="fa fa-caret-right"></i> &nbsp;<?php echo $row->title; ?></a>
			</li>
		@endforeach
		@endif
		</ul>
	</div>
</div>


<div class="sidebar hidden-xs hidden-sm">				
	<div class="sidebar-head">
		<i  title="FB: keiskeiashitaba" class="fa fa-facebook-square fa-lg"></i>&nbsp; | &nbsp; Keiskei Indonesia on FB</div>
		<div class="sidebar-body">
			<aside id="facebook-likebox-3" class="widget widget_facebook_likebox"><h3 class="widget-title"><a href="https://www.facebook.com/KeiskeiIndonesia"></a></h3><iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FKeiskeiIndonesia&amp;width=240&amp;height=230&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;show_border=true&amp;header=false&amp;force_wall=false" style="border: none; overflow: hidden; width: 240px;  height: 230px; background: #fff" frameborder="0" scrolling="no"></iframe></aside>					</div>

		</div>