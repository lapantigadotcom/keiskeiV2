<aside class="sidebar col-lg-3 col-md-3 col-sm-3  col-lg-pull-9 col-md-pull-9 col-sm-pull-9">
					
					<!-- Categories -->
					<div class="row sidebar-box purple">
						
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="sidebar-box-heading">
								<i class="icons icon-folder-open-empty"></i>
								<h4>Categories</h4>
							</div>
							
							<div class="sidebar-box-content">
								<ul>
									<li><a href="#">Cameras &amp; Photography <i class="icons icon-right-dir"></i></a></li>
									<li><a href="#">Computers &amp; Tablets <i class="icons icon-right-dir"></i></a></li>
									<li><a href="#">Cell Phones &amp; Accessories <i class="icons icon-right-dir"></i></a>
                                    	<ul class="sidebar-dropdown">
                                        	<li>
                                            	<ul>
                                                	<li><a href="#">Cell phones &amp; Smartphone</a></li>
                                                    <li><a href="#">Cell Phone Accessories</a></li>
                                                    <li><a href="#">Headsets</a></li>
                                                    <li><a href="#">Cases, Covers &amp; Skins</a></li>
                                                    <li><a href="#">Screen Protectors</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                            	<ul>
                                                	<li><a href="#">Chargers &amp; Cradles</a></li>
                                                    <li><a href="#">Batteries</a></li>
                                                    <li><a href="#">Cables &amp; Adapters</a></li>
                                                    <li><a href="#">All About Smartphones</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
									<li><a href="#">TV, Audio &amp; Surveillance <i class="icons icon-right-dir"></i></a></li>
									<li><a href="#">Video Games &amp; Consoles <i class="icons icon-right-dir"></i></a></li>
									<li><a href="#">Car Audio, Video &amp; GPS <i class="icons icon-right-dir"></i></a></li>
									<li><a href="#">Best Sellers <i class="icons icon-right-dir"></i></a></li>
									<li><a href="#">Shop by Brands <i class="icons icon-right-dir"></i></a></li>
									<li><a class="purple" href="#">All Categories</a></li>
								</ul>
							</div>
							
						</div>
							
					</div>
					<!-- /Categories -->
					
					
					<!-- Compare Products -->
					<div class="row sidebar-box blue">
						
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="sidebar-box-heading">
                            	<i class="icons icon-docs"></i>
								<h4>Compare Products</h4>
							</div>
							
							<div class="sidebar-box-content sidebar-padding-box">
								<p>You have no products to compare.</p>
							</div>
							
						</div>
						
					</div>
					<!-- /Compare Products -->
					
					
					<!-- Carousel -->
					<div class="row sidebar-box">
						
						<div class="col-lg-12 col-md-12 col-sm-12 sidebar-carousel">
							
							<!-- Slider -->
							<section class="sidebar-slider">
								<div class="sidebar-flexslider">
									
								<div style="overflow: hidden; position: relative;" class="flex-viewport"><ul style="width: 1000%; transition-duration: 0.6s; transform: translate3d(-540px, 0px, 0px);" class="slides"><li style="width: 270px; float: left; display: block;" class="clone">
											<a href="#"><img draggable="false" src="img/sidebar-slide3.jpg" alt="Slide1"></a>
										</li><li style="width: 270px; float: left; display: block;" aria-hidden="true" class="clone">
											<a href="#"><img draggable="false" src="img/sidebar-slide3.jpg" alt="Slide1"></a>
										</li>
										<li style="width: 270px; float: left; display: block;" class="">
											<a href="#"><img draggable="false" src="img/sidebar-slide1.jpg" alt="Slide1"></a>
										</li>
										<li class="flex-active-slide" style="width: 270px; float: left; display: block;">
											<a href="#"><img draggable="false" src="img/sidebar-slide2.jpg" alt="Slide1"></a>
										</li>
										<li class="" style="width: 270px; float: left; display: block;">
											<a href="#"><img draggable="false" src="img/sidebar-slide3.jpg" alt="Slide1"></a>
										</li>
									<li style="width: 270px; float: left; display: block;" aria-hidden="true" class="clone">
											<a href="#"><img draggable="false" src="img/sidebar-slide1.jpg" alt="Slide1"></a>
										</li><li style="width: 270px; float: left; display: block;" class="clone">
											<a href="#"><img draggable="false" src="img/sidebar-slide1.jpg" alt="Slide1"></a>
										</li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a class="">1</a></li><li><a class="flex-active">2</a></li><li><a class="">3</a></li></ol></div>
								<div class="slider-nav"></div>
							</section>
							<!-- /Slider -->
                            
						</div>
						
					</div>
					<!-- /Carousel -->
					
					
					<!-- Bestsellers -->
					<div class="row sidebar-box red">
						
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="sidebar-box-heading">
                            <i class="icons icon-award-2"></i>
								<h4>Bestsellers</h4>
							</div>
							
							<div class="sidebar-box-content">
								<table class="bestsellers-table">
									
									<tbody><tr>
										<td class="product-thumbnail"><a href="#"><img src="img/products/sample1.jpg" alt="Product1"></a></td>
										<td class="product-info">
											<p><a href="#">Lorem ipsum dolor sit amet</a></p>
											<span class="price">$550.00</span>
										</td>
									</tr>
									
									<tr>
										<td class="product-thumbnail"><a href="#"><img src="img/products/sample2.jpg" alt="Product1"></a></td>
										<td class="product-info">
											<p><a href="#">Lorem ipsum dolor sit amet</a></p>
											<span class="price">$550.00</span>
										</td>
									</tr>
									
									<tr>
										<td class="product-thumbnail"><a href="#"><img src="img/products/sample3.jpg" alt="Product1"></a></td>
										<td class="product-info">
											<p><a href="#">Lorem ipsum dolor sit amet</a></p>
                                            <div style="width: 100px;" title="good" class="rating readonly-rating" data-score="4"><img title="good" alt="1" src="js/img/star-on.png">&nbsp;<img title="good" alt="2" src="js/img/star-on.png">&nbsp;<img title="good" alt="3" src="js/img/star-on.png">&nbsp;<img title="good" alt="4" src="js/img/star-on.png">&nbsp;<img title="good" alt="5" src="js/img/star-off.png"><input readonly="readonly" value="4" name="score" type="hidden"></div>
											<span class="price"><del>$650.00</del> $550.00</span>
										</td>
									</tr>
									
								</tbody></table>
							</div>
							
						</div>
						
					</div>
					<!-- /Bestsellers -->
					
					
					<!-- Tag Cloud -->
					<div class="row sidebar-box green">
						
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<div class="sidebar-box-heading">
                            	<i class="icons icon-tag-6"></i>
								<h4>Tags Cloud</h4>
							</div>
							
							<div class="sidebar-box-content sidebar-padding-box">
								<a href="#" class="tag-item">digital camera</a>
								<a href="#" class="tag-item">lorem</a>
								<a href="#" class="tag-item">gps</a>
								<a href="#" class="tag-item">headphones</a>
								<a href="#" class="tag-item">ipsum</a>
								<a href="#" class="tag-item">laptop</a>
								<a href="#" class="tag-item">smartphone</a>
								<a href="#" class="tag-item">tv</a>
							</div>
								
						</div>
						
					</div>
					<!-- /Tag Cloud -->
					
					
					<!-- Specials -->
					<div class="row products-row sidebar-box orange">
						 
						<div class="col-lg-12 col-md-12 col-sm-12">
							
							<!-- Carousel Heading -->
							<div class="carousel-heading no-margin">
								
								<h4><i class="icons icon-magic"></i> Specials</h4>
								<div class="carousel-arrows">
									<i class="icons icon-left-dir"></i>
									<i class="icons icon-right-dir"></i>
								</div>
								
							</div>
							<!-- /Carousel Heading -->
							
						</div>
						
						<!-- Carousel -->
						<div class="carousel owl-carousel-wrap col-lg-12 col-md-12 col-sm-12">
							
							<div style="display: block; opacity: 1;" class="owl-carousel owl-theme" data-max-items="1">
									
								<!-- Slide -->
								<div class="owl-wrapper-outer"><div style="width: 1200px; left: 0px; display: block;" class="owl-wrapper"><div style="width: 300px;" class="owl-item"><div>
									<!-- Carousel Item -->
									<div class="product">
										
										<div class="product-image">
											<img src="img/products/sample1.jpg" alt="Product1">
											<a href="products_page_v1.html" class="product-hover">
												<i class="icons icon-eye-1"></i> Quick View
											</a>
										</div>
										
										<div class="product-info">
											<h5><a href="products_page_v1.html">Lorem ipsum dolor sit amet</a></h5>
											<span class="price">$281.00</span>
											<div style="width: 100px;" title="good" class="rating readonly-rating" data-score="4"><img title="good" alt="1" src="js/img/star-on.png">&nbsp;<img title="good" alt="2" src="js/img/star-on.png">&nbsp;<img title="good" alt="3" src="js/img/star-on.png">&nbsp;<img title="good" alt="4" src="js/img/star-on.png">&nbsp;<img title="good" alt="5" src="js/img/star-off.png"><input readonly="readonly" value="4" name="score" type="hidden"></div>
										</div>
										
										<div class="product-actions">
											<span class="add-to-cart current">
												<span class="action-wrapper">
													<i class="icons icon-basket-2"></i>
													<span class="action-name">Add to cart</span>
												</span>
											</span>
											<span class="add-to-favorites">
												<span class="action-wrapper">
													<i class="icons icon-heart-empty"></i>
													<span class="action-name">Add to wishlist</span>
												</span>
											</span>
											<span class="add-to-compare">
												<span class="action-wrapper">
													<i class="icons icon-docs"></i>
													<span class="action-name">Add to Compare</span>
												</span>
											</span>
										</div>
										
									</div>
									<!-- /Carousel Item -->
								</div></div><div style="width: 300px;" class="owl-item"><div>
									<!-- Carousel Item -->
									<div class="product">
										
										<div class="product-image">
											<img src="img/products/sample2.jpg" alt="Product1">
											<a href="products_page_v1.html" class="product-hover">
												<i class="icons icon-eye-1"></i> Quick View
											</a>
										</div>
										
										<div class="product-info">
											<h5><a href="products_page_v1.html">Lorem ipsum dolor sit amet</a></h5>
											<span class="price">$281.00</span>
											<div style="width: 100px;" title="good" class="rating readonly-rating" data-score="4"><img title="good" alt="1" src="js/img/star-on.png">&nbsp;<img title="good" alt="2" src="js/img/star-on.png">&nbsp;<img title="good" alt="3" src="js/img/star-on.png">&nbsp;<img title="good" alt="4" src="js/img/star-on.png">&nbsp;<img title="good" alt="5" src="js/img/star-off.png"><input readonly="readonly" value="4" name="score" type="hidden"></div>
										</div>
										
										<div class="product-actions">
											<span class="add-to-cart current">
												<span class="action-wrapper">
													<i class="icons icon-basket-2"></i>
													<span class="action-name">Add to cart</span>
												</span>
											</span>
											<span class="add-to-favorites">
												<span class="action-wrapper">
													<i class="icons icon-heart-empty"></i>
													<span class="action-name">Add to wishlist</span>
												</span>
											</span>
											<span class="add-to-compare">
												<span class="action-wrapper">
													<i class="icons icon-docs"></i>
													<span class="action-name">Add to Compare</span>
												</span>
											</span>
										</div>
										
									</div>
									<!-- /Carousel Item -->
								</div></div></div></div>
								<!-- /Slide -->
								
								
								<!-- Slide -->
								
								<!-- /Slide -->
								
							</div>
						
						</div>
						<!-- / Carousel -->
						
						
					</div>
					<!-- /Specials -->
					
					
				</aside>