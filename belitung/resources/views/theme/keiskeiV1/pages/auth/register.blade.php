@extends('theme.keiskei.app')
<title>
	KeisKei Indonesia | Pendaftaran Anggota
	</title>
@section('content')

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Selamat Datang</h2>
			<h5>Silahkan Isi Form berikut untuk Pendaftaran</h5>
		</div>
	</div>
</div>


<br><br><br>
<div class="row">
	<div class="col-md-offset-1 col-md-10 filter-product">
		<div class="row">

			<div class="col-md-offset-1 col-md-10 form-container">

				

				<div class="form-head text-center">
					<h3>
					<b>Daftar anggota</b> <span style="color:green">Keiskei Indonesia</span>
					</h3>
					<p>
						Dapatkan banyak keuntungan dengan menjadi anggota <span style="color:green">Keiskei Indonesia</span>. 
					</p>
					@include('theme.'.$data['theme']->code.'.include.session')
					<hr>
				</div>
				<div class="form-body">
					{!! BootstrapForm::open(array('store' => 'register.store','id' => 'form-validate')) !!}
					@if(!empty($data['affiliate_token']))
					{!! Form::hidden('referraled_by',$data['affiliate_token']) !!}
					@endif
					
					{!! BootstrapForm::text('username') !!}
					{!! BootstrapForm::text('mobile','Nomor Handphone (Untuk Aktivasi)') !!}
					{!! BootstrapForm::text('email') !!}

					<div class="form-group">
						<label for="ms_country_id">
							Negara
						</label>
						{!! Form::select('ms_country_id',$data['country'],null,array('class' => 'form-control','id' => 'ms_country_id')) !!}
					</div>
					<div class="form-group" id="city-container">
						<label for="ms_city_id">
							Kota
						</label>
						{!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id')) !!}
					</div>
					<center><div class="form-group">
						{!! Recaptcha::render() !!}
						<span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>
					</div></center>

<div class="form-group" style="font-size:12px;color:green">
<center>
	Dengan Menekan tombol Bergabung, Berarti Anda Menyetujui Aturan <span style="color:green;font-weight: 500;">Keanggotaan </span></a>dan <span style="color:green;font-weight: 500;">Kebijakan Privasi </span></a>KEISKEI INDONESIA
	</center></div>
<center>
					<div class="form-group text-align form-action">
						{!! Form::submit('Bergabung',array('class' => 'btn btn-keiskei')) !!}
					
					</div></center>

					<br>
					<center><script language="JavaScript" type="text/javascript">
				TrustLogo("https://www.keiskei.co.id/theme/keiskei/img/comodo-keiskei.gif", "CL1", "none");
				</script></center>
					
					{!! BootstrapForm::close() !!}
				</div>
			
					<div class="form-head  text-center"><div style="background:#FFEDED"><hr>
					<h3> <span style="color:red"><i class="fa fa-warning"></i> </span>
					<b>Catatan Penting</b>
					</h3><hr></div>
					<p>
						1. Jangan Memberikan detail akun dan password <span style="color:green;font-weight: 500;">Keiskei Indonesia</span> Anda kepada siapapun.
					</p>
					<p>
						2. Pastikan URL yang Anda akses adalah <span style="font-weight: 500;color:green">https://www.keiskei.co.id</span>
					</p>
					<p>
						3. Setelah ter-verifikasi, pastikan Anda mengisi detail profil Anda di <span style="font-weight: 500;color:green">Beranda - edit profile</span>.
					</p>

					<p>
						4. Pihak <span style="font-weight: 500;color:green">Keiskei Indonesia</span> tidak pernah menanyakan akun dan password Anda dengan alasan apapun, harap berhati-hati.
					</p>
					<br>

					@include('theme.'.$data['theme']->code.'.include.session')
					
				</div>

			</div>



		</div>
	</div>
</div>

@endsection

@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/validation/jquery.validate.min.js') !!}
	<script type="text/javascript">
		$(function () {
        	var value = $("#ms_country_id").val();
        	cityField(value);
      	});
		function cityField( value)
		{
			var general_country_id = {!! $data['general']->ms_country_id !!};
			if(value == general_country_id)
			{
				$('#city-container').show();
			}else{
				$('#city-container').hide();
			}
		}
	    $(document).ready(function() {

		   $('#ms_country_id').selectize();      
		   $('#ms_city_id').selectize();      
		   $('#ms_country_id').change(function()
		   {
		   		cityField($(this).val());
		   });
		   
		   $('#form-validate').validate({
		   		errorClass : "help-block error",
		   		rules: {
		   			username : {
		   				required: true,
		   				minlength: 3
		   			},
		   			mobile : {
		   				required: true,
		   				minlength: 3
		   			},
		   			email :{
		   				required: true,
		   				email : true
		   			}
		   		},
		   		messages: {
		   			username : {
		   				required : "Mohon masukkan username Anda",
		   				minlength : $.validator.format("Username Anda harus minimal {0} karakter")
		   			},
		   			mobile : {
		   				required : "Mohon masukkan nomor handphone Anda",
		   				minlength : $.validator.format("nomor handphone Anda harus minimal {0} karakter")
		   			},
		   			email : {
		   				required : "Mohon masukkan email Anda",
		   				email : "Mohon cek kembali alamat email Anda"
		   			}
		   		}
		   });
	    });
	</script>
@endsection