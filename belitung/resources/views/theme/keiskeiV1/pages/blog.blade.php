@extends('theme.keiskei.app')

@section('content')
@include('theme.keiskei.partials.meta-blog')

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>BLOGS</h2>
			<h5>KEISKEI INDONESIA | The Real Ashitaba</h5>
		</div>
	</div>
</div>
<div class="row">

	<div class="row product-detail-container">

		<div class="col-md-offset-1 col-md-10 filter-product">
			<div class="row">
				@include('errors.session')
			</div>
			<div class="col-md-4" style="max-width:24%;margin-right:20px">
				@include('theme.keiskei.include.sidebar-default')
			</div>
				<div class="col-md-8" >
					 
					@foreach($data['content'] as $row)
					<div class="row">
						<div class="col-md-12 blog-item">
							<div class="row">
								<div class="col-md-4">
									<i class="fa fa-calendar"></i>
									{!! date('l, j F Y') !!}
								</div>
								<div class="col-md-8">
									<i class="fa fa-newspaper-o"></i>
									{!! $row->categoryArticle->title !!}
								</div>
							</div>
							<h4><a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><?php echo $row->title; ?></a></h4>
							<p>
								<?php $desc = explode("<!-- pagebreak -->", $row->description);
								if(count($desc)>1)
									{ ?>
								<p>{!! $desc[0] !!}&nbsp;</p>      
								<?php }else{ ?>
								<p>{!! strip_tags(substr($row->description,0,150)).'...' !!}</p>
								<?php
							}
							?> <a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><b>...read more</b></a>
						</p>
					</div>
				</div>
				@endforeach
				<div class="row">
					<div class="col-md-12">
						{!! $data['content']->render() !!}
					</div>
				</div>
			</div>
		</div>


	</div>
	@include('theme.keiskei.partials.best-products')
	@endsection

	@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
		$(document).ready(function() {
			$('.img-lazy').unveil();
			$("#category").selectize();
			$("#product-slider").owlCarousel({
				items : 5,
				lazyLoad : true,
				navigation : true,
				pagination : false,
				navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
			}); 
		});
	</script>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>

	@endsection
	@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
	@endsection