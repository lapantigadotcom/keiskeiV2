@extends('theme.keiskei.app')





@section('content')

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db 
" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db
" />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KeisKei Indonesia | The Real Ashitaba. Alamat:  Ruko PPS , Jl.Mutiara 223 / Kav.M Gresik - Indonesia.Phone: +62.31.3957.299
Fax.+62-31-3951517.CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707.BBM: 55169066 / 53648437 / 2bee00db 
">
<meta name='robots' content='INDEX, FOLLOW' />


<title>Keiskei Indonesia | Hubungi Kami</title>
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>KEISKEI INDONESIA</h2>
			<h5>The Real Ashitaba</h5>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10 filter-product">
		<div class="row">
		@include('errors.session')
		
		</div>
		<div class="row">
			<div class="col-md-4">
				<h5>
					Kontak Kami :
				</h5>
				<p>
					<address>
					  <strong>{{ $data['general']->site_title }}</strong><br>
					  {!! $data['general']->address !!}<br>
					  <!--  {!! $data['general']->city->name !!}, {!! $data['general']->city->province->name !!}<br>  -->

					  <br title="Phone">P:</abbr> {!! $data['general']->telephone !!}
						<br>Fax.+62-31-99102111
					  <br>CALL / SMS / WA / Viber: 0811-330-9979 / 0811-335-0706 / 0811-335-0707
					  <br>BBM: 55169066 / 53648437 / 2bee00db
					</address>

					<address>
					  <strong>Email</strong><br>
					  <a href="mailto:{!! $data['general']->email !!}">{!! $data['general']->email !!}</a>
					</address>
				</p>
			</div>
			<div class="col-md-8">
				<h5>
					Formulir Kontak Kami
				</h5>
				@include('errors.session')
				{!! Form::open(array('route' => 'contact-us.store', 'method' => 'POST')) !!}
				{!! BootstrapForm::text('name','Nama', Auth::check()?Auth::user()->name:'') !!}
				{!! BootstrapForm::text('email','Email', Auth::check()?Auth::user()->email:'') !!}
				{!! BootstrapForm::text('subject','Judul') !!}
				{!! BootstrapForm::textarea('message','Pesan') !!}
				<div class="form-group">
					{!! Recaptcha::render() !!}
					<span class="help-block" style="color:red;">{!! $errors->any()?($errors->first('g-recaptcha-response')?$errors->first('g-recaptcha-response'):''):'' !!}</span>
				</div>
				<div class="form-group">
					{!! Form::submit('Kirim',array('class' => 'btn btn-keiskei')) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@include('theme.keiskei.partials.best-products')
@endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$('.img-lazy').unveil();
	    	$("#category").selectize();
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection