
@extends('theme.keiskei.app')
@section('content')
<title>
	KeisKei Indonesia | Branch Patner, Gold Patner, Silver Patner, Bronze Patner</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KEISKEI tipe patner : Branch Patner, Gold Patner, Silver Patner, Bronze Patner" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content=" KEISKEI tipe patner : Branch Patner, Gold Patner, Silver Patner, Bronze Patner" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Branch Patner, Gold Patner, Silver Patner, Bronze Patner,Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KEISKEI tipe patner : Branch Patner, Gold Patner, Silver Patner, Bronze Patner">
<meta name='robots' content='INDEX, FOLLOW' />

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>{!! $data['context'] !!}</h2>
			<h5>KEISKEI INDONESIA | The Real Ashitaba
			</h5>
		</div>
	</div>
</div>
<div class="row">

	<div class="row product-detail-container">

		<div class="col-md-offset-1 col-md-10 filter-product">
			<div class="row">
				@include('errors.session')
			</div>
			
<!---sidebar-->

<div class="col-md-4" style="max-width:24%;margin-right:20px">
				<div class="sidebar hidden-xs hidden-ms">
					<div class="sidebar-head">
						<i class="fa fa-reorder"></i>&nbsp; | &nbsp; KEISKEI Main Menu
					</div>
					<div class="sidebar-body">
						<ul>
			<li class="{!! Request::is('/')?'active':'' !!}"><a href="{!! URL::to('/') !!}"><i class="fa fa-caret-right"></i>&nbsp;Home</a></li>
	        <li class="{!! Request::is('/products')?'active':'' !!}"><a href="{!! URL::to('/products') !!}"><i class="fa fa-caret-right"></i>&nbsp;Produk</a></li>
	        <li><a href="{!! route('contact-us') !!}"><i class="fa fa-caret-right"></i>&nbsp;Hubungi Kami</a></li>
												</ul>
					</div>
				</div>
				

				
				<div class="sidebar hidden-xs hidden-ms">
					<div class="sidebar-head">
						<i  title="FB: keiskeiashitaba" class="fa fa-facebook-square fa-lg"></i>&nbsp; | &nbsp; Keiskei Indonesia on FB</div>
					<div class="sidebar-body">
						<aside id="facebook-likebox-3" class="widget widget_facebook_likebox"><h3 class="widget-title"><a href="https://www.facebook.com/KeiskeiIndonesia"></a></h3><iframe src="https://www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FKeiskeiIndonesia&amp;width=240&amp;height=300&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;show_border=true&amp;header=false&amp;force_wall=false" style="border: none; overflow: hidden; width: 240px;  height: 300px; background: #fff" frameborder="0" scrolling="no"></iframe></aside>					</div>

				</div>
<!--
				<div class="sidebar">				
					<div class="sidebar-head">
								<i  title="FB: keiskeiashitaba" class="fa fa-youtube"></i>&nbsp; | &nbsp; Keiskei Indonesia Video</div>
					<div class="sidebar-body">
					<iframe width="240" height="240" src="https://www.youtube.com/embed/cAYgWYqWPPQ" frameborder="0" allowfullscreen></iframe>
				</div></div>  -->


			</div>


			<div class="col-md-8" >
				<div class="row">
					<div class="col-md-12">
						{!! Form::open(array('route'=> 'partner-area', 'method' => 'GET', 'class' => 'form-inline')) !!}
						<div class="form-group">
							<label class="form-label" for="group_type">
								Pilih Tipe
							</label>
							<select name="group_type" class="form-control" id="group_type">
								<option value="ALL" @if(Session::get('group_type')=='ALL') selected="" @endif>Semua</option>
								@foreach($data['role'] as $key => $value)
								<option value="{!! $key !!}" @if(Session::get('group_type')==$key) selected="" @endif>{!! $value !!}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							{!! Form::submit('Tampilkan',array('class'=>'btn btn-keiskei')) !!}
						</div>
						{!! Form::close() !!}
						<hr>
					</div>
				</div>
				<div class="row">
					@foreach($data['content'] as $row)
					<div class="col-md-3 partner-boundary partner-item">
						<div class="col-md-12">
							<div class="img-responsive img gambar-produk hidden-xs hidden-ms" >
							@if(File::exists('data/user/photo/'.$row->photo) and $row->photo != '')
							<img src="{!! asset('data/user/photo/'.$row->photo) !!}" class="img-responsive img-thumbnail img-lazy" style="width:200px; height:150px;padding:10px;">
							@else
							<img src="{!! asset('theme/keiskei/img/user.png') !!}" class="img-responsive img-thumbnail img-lazy" style="width:200px; height:150px;">
							@endif
							<input style="border-radius:0;width:100%;height:100%;background: rgba(0, 88, 34, 0.3);" class="btn btn-widget" type="button" value="Lihat Profile" onclick="location.href='{!! route('partner-show',[$row->code]) !!}';"/></div>
							<h5>
							<a href="{!! route('partner-show',[$row->code]) !!}">{!! $row->name !!}</a>
							</h5>
							<p>
							<i title="Tipe Member" class="fa fa-user"></i>&nbsp;|{!! $row->roles->first()->name !!}<br>
							<i title="Alamat"class="fa fa-building"></i>&nbsp;|{!! $row->city->name !!}, {!! $row->city->province->name !!}
							</p>



						</div>
					</div>
					@endforeach
				</div>
				<div class="row">
					<div class="col-md-12">
						{!! $data['content']->render() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('theme.keiskei.partials.best-products')
@endsection

@section('custom-footer')
{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
<script type="text/javascript">
	$(document).ready(function() {
		$('.img-lazy').unveil();
		$("#category").selectize();
		$("#product-slider").owlCarousel({
			items : 5,
			lazyLoad : true,
			navigation : true,
			pagination : false,
			navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
		}); 
	});
</script>

<style type="text/css">
	div.gambar-produk {
	    position: relative;
	    float:left;
	    margin:5px;}

	div.gambar-produk:hover input
	  {
	  display: block;
	  }

	div.gambar-produk input {
	    position:absolute;
	   left: 0;
	   top: 0;
	    display:none;
	}
	</style>


@endsection
@section('custom-head')
{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection