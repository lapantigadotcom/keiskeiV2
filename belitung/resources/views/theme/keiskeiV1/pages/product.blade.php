@extends('theme.keiskei.app')



@section('content')
<title>
	KeisKei Indonesia | Beauty, Health, Cure </title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta name="DC.subject" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KEISKEI Ashitaba Product : keiskei treasure, keiskei hair vitamin and tonic, ashitaba angel leaf, ashitaba coffe lover">
<meta name='robots' content='INDEX, FOLLOW' />
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>KEISKEI INDONESIA</h2>
			<h5>The Real Ashitaba</h5>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10 filter-product">
		<div class="row">
			
			<div class="col-md-6 text-left">
				<div class="col-md-6">
					<?php 
					array_unshift($data['categoryproduct'], 'Semua');
					?>
					{!! Form::select('category',$data['categoryproduct'],Session::has('categorySelect')?Session::get('categorySelect'):null,['class' => 'form-control','id' => 'category','onchange' => 'changeCategory(this)']) !!}
				</div>
			</div>
			{!! Form::open(array('route' => 'search', 'method' => 'GET', 'class' => 'form-search')) !!}
			<div class="col-md-6 text-right"> 
				<div class="col-md-offset-6 col-md-6 text-right">
					<div class="input-group">
						{!! Form::text('search',null,['class' => 'form-control','placeholder' => 'Cari Produk...']) !!}
						<div class="input-group-addon">
							<button class="btn btn-keiskei-search btn-sm" type="submit"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10 filter-product">
		<div class="row">
		@include('errors.session')
		@foreach($data['products'] as $row)
			<div class="col-md-4 images">
				<div class="container-product text-center">
				@if($row->productImages()->count() > 0)
				    @if(File::exists($row->productImages->first()->image))
					<div class="img-container">
						<img class="img-responsive img-lazy" data-src="{!! asset($row->productImages->first()->image) !!}" src="{!! asset($row->productImages->first()->image) !!}" alt="">
					</div>
					@endif
				@endif
					<h5><span style="color:green">{!! $row->categoryproduct->name !!} </span>| {!! $row->title !!}</h5>
						<a class="btn btn-default" title="Tulis Ulasan" data-toggle="modal" data-target="#{{ 'review'.$row->id }}"><i class="fa fa-edit"></i></a>
						<a class="btn btn-default"  title="Daftar Ulasan produk" data-toggle="modal" data-target="#{{ 'review-content-'.$row->id }}"><i class="fa fa-exchange"></i></a>
						<a class="btn btn-default"  title="FAQ produk" data-toggle="modal" data-target="#help" onclick="productHelp(this)" data-product="{!! $row->id !!}"><i class="fa fa-question"></i></a>
					<a class="btn btn-widget"  title="lihat produk detail" style="width:70px" href="{!! route('user.product.detail',$row->code, $row->title) !!}">lihat </a>
				</div>
				
			</div>
			@include('theme.keiskei.include.review-content',['id' => 'review-content-'.$row->id, 'reviews' => $row->reviews ])
			@include('theme.keiskei.include.review',[ 'idP' => $row->id,'id' => 'review'.$row->id])
		@endforeach

		</div>
		<div class="row">
			<div class="col-md-12">
				{!! $data['products']->render() !!}
			</div>
		</div>
	</div>
</div>
@include('theme.keiskei.partials.best-products')
@include('theme.keiskei.include.help',[ 'help' => 'Bantuan'])
@endsection

@section('custom-footer')
	{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
	{!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
	{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
	<script type="text/javascript">
	    $(document).ready(function() {
	    	$('.img-lazy').unveil();
	    	$("#category").selectize();
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	    function changeCategory(x)
	    {
	    	if(x.value==0)
	    	{
	    		window.location.replace("{!! route('product') !!}");
	    	}else
	    	{
		    	window.location.replace("{!! route('product') !!}/" + x.value + "-" + $(x).text());	    	
	    	}
	    }
	    function productHelp(x)
	    {
	    	var product_id = x.getAttribute("data-product");
	    	$.ajax(
		   	{
		   		url:"{{ route('product.getjson') }}",
		   		type: "GET",
		   		data:{
			      product_id : product_id
			    }
			}).done(function(data,status){
				if(status=='success')
			    {	
			    	console.log(data);
			    	$('#help-text').empty();
			    	$('#help-text').append(data.help);
			    }
			});
	    }
	</script>



@endsection
@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
		{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}

	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
@endsection