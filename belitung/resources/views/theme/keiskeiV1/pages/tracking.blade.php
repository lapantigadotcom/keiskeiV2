@extends('theme.keiskei.app')

@section('content')
@include('theme.keiskei.partials.meta-blog')

<div class="row">
    <div class="col-md-offset-1 col-md-10">
        <div class="banner-product text-center">
            <h2>Tracking Pengiriman</h2>
            <h5>KEISKEI INDONESIA | The Real Ashitaba</h5>
        </div>
    </div>
</div>
<div class="row">

    <div class="row product-detail-container">

        <div class="col-md-offset-1 col-md-10 filter-product">
            <div class="row">
                @include('errors.session')
            </div>
            <div class="col-md-4" style="max-width:24%;margin-right:20px">
                @include('theme.keiskei.include.sidebar-default')
            </div>
                <div class="col-md-8" >
                     
                     <div class="row">
                        <div class="col-md-12 blog-item">
                            <div class="row">
                                <div class="col-md-8">
                                    <i class="fa fa-calendar"></i>
Silahkan Masukkan No. Tracking Anda di form berikut : 
<p> Pilihan Kurir :(POS INDONESIA,Tiki,JNE,PCP,RPX)                               </div>
                                
                            </div>



                             <p>
                                 
                        </p>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-12">
                 {!! Form::text('No. Tracking',null, array('class' => 'form-control', 'placeholder' => 'No. Tracking')) !!}
<p>
         
<?php 
                           
                   $data['service_courier'] = array();
                            
                            if($data['content']->service_courier != '')
                                array_push($data['service_courier'], $data['content']->service_courier);
                            else
                                array_push($data['service_courier'], 'Tidak ada');
                            ?>
                            {!! Form::select('service_courier',$data['service_courier'], null, array('class' => 'form-control','id' => 'service_courier','onchange' => 'changeService(this)')) !!}
                     </div>
                     <br></p>
                </div>
 <p>  
                <center>
                    <div class="form-group text-align form-action">
                        {!! Form::submit('Lacak Paket Anda',array('class' => 'btn btn-keiskei')) !!}
                    
                    </div></center>   </p>


            </div>
        </div>


    </div>
    @include('theme.keiskei.partials.best-products')
    @endsection

    @section('custom-footer')
    {!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
    {!! HTML::script('theme/keiskei/plugin/bootstrap-filestyle/bootstrap-filestyle.min.js') !!}
    {!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
    <script type="text/javascript">
        $(document).ready(function() {
            $('.img-lazy').unveil();
            $("#category").selectize();
            $("#product-slider").owlCarousel({
                items : 5,
                lazyLoad : true,
                navigation : true,
                pagination : false,
                navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
            }); 
        });
    </script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-556bcd205ea58bfc" async="async"></script>

    @endsection
    @section('custom-head')
    {!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
    {!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
    @endsection