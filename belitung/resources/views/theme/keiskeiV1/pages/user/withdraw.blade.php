@extends('theme.keiskei.app')
<title>Keiskei Indonesia | Withdraw Bonus Anggota </title>

@section('content')
@include('theme.keiskei.scripts.price-product')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Withdraw Bonus Anggota</h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						
						<table class="table table-hover">
							<thead>
								<tr>
									<th>No</th>
									<th>Tahun</th>
									<th>Bulan</th>
									<th>Bonus Afiliasi</th>
									<th>Bonus Bulanan</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							<?php $no=1; ?>
							@foreach($data['content'] as $row)
								<tr>
									<td>{!! $no++ !!}</td>
									<?php 
										$dateObj   = DateTime::createFromFormat('!m', $row->month);
										$monthName = $dateObj->format('F');
										unset($dateObj);
									?>
									<td>{!! $row->year !!}</td>
									<td>{!! $monthName !!}</td>
									<td class="text-right">{!! money_format('%(#10n',$row->referral_value) !!}</td>
									<td class="text-right">{!! money_format('%(#10n',intval($row->monthly_value)) !!}</td>
									<td>{!! $row->status=='1'?"<span class='label label-default'>Inactive</span><p>".date('d M Y',strtotime($row->withdraw_date))."</p>":$row->status=='2'?"<span class='label label-warning'>Confirmation</span>":"<span class='label label-primary'>Active</span>" !!}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						
						<div class="row">
							<div class="col-md-12">
								{!! $data['content']->render() !!}
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<table class="table">
									<tr>
										<td>Total yang dapat diambil</td>
										<td> : </td>
										<td class="text-right"> {!!  money_format('%(#10n',$data['total_active']) !!} </td>
									</tr>
									<tr>
										<td>Total yang telah diambil</td>
										<td> : </td>
										<td class="text-right"> {!!  money_format('%(#10n',$data['total_inactive']) !!} </td>
									</tr>
									<tr>
										<td>Total Semua</td>
										<td> : </td>
										<td class="text-right"> {!!  money_format('%(#10n',$data['total']) !!} </td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								@if(Auth::user()->approved == '1')
									<!-- @if(date('t') === date('j')) -->
									<a href="{!! route('user.withdraw.get') !!}" class="btn btn-keiskei-default">Withdraw Bonus</a>
									<!-- @else
									<a href="javascript:void(0)" class="btn btn-keiskei-default" onclick="withdrawModal()">Withdraw Bonus</a>
									@endif -->
									@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				@include('theme.keiskei.include.user-sidebar')
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="withdrawPoint" tabindex="-1" role="dialog" aria-labelledby="withdrawPoint1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="withdrawPoint1">Withdraw Point</h4>
      </div>
      <div class="modal-body">
        <h4>Maaf, Withdraw hanya dapat dilakukan pada akhir bulan.</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
@include('theme.keiskei.partials.best-products')
@endsection

@section('custom-footer')
	
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	    function withdrawModal () {
		    $('#withdrawPoint').modal({ backdrop: 'static', keyboard: false });
		    
		  }
	</script>
@endsection

@section('custom-head')
	
@endsection