<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon"/>
 
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	   <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
  @include('theme.keiskei.partials.head')
</head>
<body>
	@include('theme.keiskei.partials.menu')
	<div class="container-fluid">
		<div class="row">
			
		</div>
		<div id="page-content">
		@yield('content')
		</div>
		@include('theme.keiskei.partials.footer')
	</div>
  @include('theme.keiskei.partials.script')		
</body>
</html>