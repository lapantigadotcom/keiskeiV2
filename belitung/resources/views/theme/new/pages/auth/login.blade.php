@extends('theme.keiskei.app')

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang">

<title>Keiskei Indonesia | Login Anggota</title>

@section('content')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Silahkan Login terlebih dahulu. </h5>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-offset-1 col-md-10 filter-product">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 form-container">
				<div class="form-head text-center">
					<h5>Silahkan masukkan detail member Anda.</h5>
					<p></p>
					@include('theme.'.$data['theme']->code.'.include.session')
					<hr>
				</div>
				<div class="form-body">
				{!! BootstrapForm::open(array('store' => 'login.store')) !!}
				{!! BootstrapForm::text('username') !!}
				{!! BootstrapForm::password('password') !!}
				{!! BootstrapForm::checkbox('remember_me',' Ingat saya', true) !!}
				<div class="col-md-12 text-center form-action">
					<div class="col-md-6 text-left">
						{!! Form::submit('Masuk',array('class' => 'btn btn-keiskei')) !!}
					</div>
					<div class="col-md-6 text-right">
						<a href="{{ url('/password/email') }}"> Lupa Password?</a>
					</div>
				</div>
				<br>
				{!! BootstrapForm::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('custom-footer')
	<script type="text/javascript">
	    $(document).ready(function() {
	      $("#product-slider").owlCarousel({
	        items : 5,
	        lazyLoad : true,
	        navigation : true,
	        pagination : false,
	        navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
	      }); 
	    });
	</script>
@endsection