
@extends('theme.keiskei.app')
 
 

@section('content')
<title>Keiskei Indonesia | The Real Ashitaba</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@@keiskeiashitaba" />
<meta name="twitter:creator" content="@@keiskeiashitaba" />
<meta name="twitter:url" content="https://www.keiskei.co.id" />
<meta name="twitter:title" content="Penumbuh Rambut Keiskei" />
<meta name="twitter:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="og:image" content="https://www.keiskei.co.id/theme/keiskei/img/logo.png"/>
<meta property="og:type" content="website" />
<meta property="og:url" content="https://www.keiskei.co.id" />
<meta name="og:description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang" />
<meta property="fb:admins" content="https://www.facebook.com/KeiskeiIndonesia" />
<meta property="fb:app_id" content="1610113749225001" />
<meta name="DC.description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang " />
<meta name="DC.subject" content="Penumbuh Rambut, Kesehatan, obat Kanker, obat diabetes, obat anemia, obat kista, obat miom, obat rambut, anti dht" />
<meta name="DC.creator" content="Keiskei Indonesia" />
<meta name="DC.publisher" content="Keiskei Indonesia" />
<meta name="description" content="KEISKEI Merupakan produk Untuk Kesehatan Tubuh dan Rambut Yang Terbuat Dari Kandungan Utama Extract Ashitaba yang Di proses dengan metode tradisional Jepang">
<meta name="p:domain_verify" content="05e6dcbffcea28ff89468960b463c7bc"/>
<meta name='robots' content='INDEX, FOLLOW' />
<div class="row bg-leaf">
	<div class="col-md-offset-1 col-md-10">
		<div id="main-carousel" class="carousel slide" data-ride="carousel">


			<ol class="carousel-indicators hidden-xs hidden-sm ">
				<li data-target="#main-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#main-carousel" data-slide-to="1"></li>
			<li data-target="#main-carousel" data-slide-to="2"></li>
			</ol>


			<div class="carousel-inner hidden-xs hidden-sm " role="listbox">
				<div class="item active">
					<img src="{{ asset('theme/keiskei/img/slider/17agustus/17agustus-b.JPG') }}" class="img-lazy" alt="keiskei 17 Agustus">
					<div class="carousel-caption">	
					</div>
				</div>
				<div class="item">
					<img src="{{ asset('theme/keiskei/img/slider/17agustus/17agustus-a.JPG') }}" class="img-lazy" alt="keiskei 17 Agustus">
					<div class="carousel-caption">
					</div>
				</div>

				<div class="item">
					<img src="{{ asset('theme/keiskei/img/slider/17agustus/17agustus-c.JPG') }}" class="img-lazy" alt="keiskei 17 Agustus">
					<div class="carousel-caption">
					</div>
				</div>


			</div>
			<div class="banner-keiskei-front col-md-12" id="banner-keiskei">
				<div class="row">
					<div class="col-md-offset-7 col-md-4" style="text-align:center; ">
						<div class="over-carousel" style="position:relative" >
							<h4> 
								<center><img src="{!! asset('theme/keiskei/img/keiskei-white.png') !!}" 
  									class="img-responsive" width="53%"></center></h4>							
							<ul >

							@foreach($data['latest_article'] as $row)
								<a href="{!! route('articles',[$row->id.'-'.$row->title]) !!}"><li><i class="glyphicon glyphicon-chevron-right"></i> <b>{{ strlen($row->title) > 30?substr($row->title).'...':$row->title }}</li></a>
							@endforeach
							<li style="background:#fc0;color:#fff"><i class="fa fa-truck"></i><a href="http://lapantiga.com/kurir/ongkir" data-toggle="modal" data-target="#ongkir"><b>&nbsp; Cek Ongkir</b> </a><a data-toggle="modal"  href="http://lapantiga.com/kurir/tracking" data-target="#resi"> <i class="fa fa-barcode"></i>&nbsp; Lacak resi</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12 main-our-product">
					<h2>Produk Kami</h2>
					<h5>Beauty | Health | Cure </h5>

<!-- Modal resi-->
<div class="modal fade" id="resi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" style="color:#fff">Keiskei | Lacak Resi JNE</h4>

            </div>
            <div class="modal-body" ><div class="te" style="height:600px">
<iframe src="http://lapantiga.com/kurir/tracking" width="100%" height="100%"></iframe>
            </div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal ongkir-->
<div class="modal fade" id="ongkir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" >Keiskei | Cek Tarif Pengiriman</h4>

            </div>
            <div class="modal-body" ><div class="te" style="height:600px">
<iframe src="http://lapantiga.com/kurir/ongkir" width="100%" height="100%"></iframe>
            </div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



				</div>
			</div>


 

 
			<div class="row">
				<div class="col-md-12">
					<div id="product-slider" class="owl-carousel">
						@foreach($data['latest'] as $row)
						<div class="gambar-produk item">
							@if($row->productImages()->count() > 0)
							@if(File::exists($row->productImages->first()->image))
							<img title="{!! $row->title !!} | {!! ucwords($row->categoryproduct->name) !!}" class="lazyOwl img-responsive" data-src="{!! asset($row->productImages->first()->image) !!}" alt="Lazy Owl Image">
							<input style="border-radius:0;width:100%;height:100%;background: rgba(0, 88, 34, 0.3);" class="btn btn-widget" type="button" value="{!! $row->title !!}" onclick="location.href='{!! route('user.product.detail',$row->code, $row->title) !!}';"/>
							@endif
							@endif
							<h4>{!! $row->title !!}</h4>
							<h3>{!! ucwords($row->categoryproduct->name) !!}</h3>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row color-grey">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">





			<div class="col-md-4 widget hidden-xs hidden-sm">
				<h4><i class="fa fa-rss"></i> Blog Terbaru</h4>
				<div class="col-md-12 widget-description">
	<img src="{!! asset('theme/keiskei/img/bag-keiskei.png') !!}" atl="keiskei artikel" class="img-responsive hidden-xs hidden-sm">

				@if(!empty($data['latest_one']))			

					<h3>{!! strlen($data['latest_one']->title > 20)?substr($data['latest_one']->title,0,20).'...':$data['latest_one']->title !!}</h3>
					<p>
						{!! strlen(strip_tags($data['latest_one']->description))>150?substr(strip_tags($data['latest_one']->description),0,150).'...':strip_tags($data['latest_one']->description) !!}
					</p>
					<div class="col-md-12 text-center">
						<a href="{!! route('articles',[$data['latest_one']->id.'-'.$data['latest_one']->title]) !!}" class="btn btn-widget" style="width:120px">Selengkapnya</a>
					</div>
				@endif
				</div>
			</div>


			<div class="col-md-4 widget kontak">
				<h4><i class="fa fa-user"></i> Our Head Office :</h4>
				<div class="col-md-12 widget-description">
						<img src="{!! asset('theme/keiskei/img/keiskei-mobile-icon.png') !!}" class="img-responsive" width="250px"alt="keiskei mobile app">

<div class="col-md-12 text-center">

					<p><b>
	KeisKei Indonesia | The Real Ashitaba<br></b>
Ruko PPS ,  Jl.Mutiara 223 / Kav.M Gresik - Indonesia <br>
 Email
info@keiskei.co.id <p>
						<a href="#" class="btn btn-widget" style="width:120px">Kontak Kami</a>
					</div></p>						 </p>
					</div>
				</div>
			 




			<div class="col-md-4 widget hidden-xs hidden-sm">
				@if(Auth::check() == false)
				<h4><i class="fa fa-list-alt"></i> Pendaftaran</h4>
				@if($errors->any())
				<div class="row">
					<div class="col-md-12 text-center">
						<ul class="alert alert-danger text-left" style="padding: 25px 0px 0px 35px;">
							@foreach($errors->all() as $rowErrors)
							<li>
							{!! $rowErrors !!}
							</li>
							@endforeach
						</ul>
					</div>
				</div>		
				@endif
				
					
				


				<div class="col-md-12 widget-description">
					
					<p>
						Dapatkan banyak keuntungan dengan menjadi anggota <span style="color:green">Keiskei Indonesia</span>.
					</p>
					{!! BootstrapForm::open(array('route' => 'register.front.store', 'id' => 'form-validate', 'method' => 'post')) !!}
					<div class="form-group">
						{!! Form::text('username',null, array('class' => 'form-control', 'placeholder' => 'Username')) !!}
					</div>
					<div class="form-group">
						{!! Form::text('mobile',null, array('class' => 'form-control', 'placeholder' => 'No. HP (untuk aktivasi)')) !!}
					</div>
					<div class="form-group">
						{!! Form::text('email',null, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
					</div>
					<div class="form-group">
						{!! Form::select('ms_country_id',$data['country'], null, array('class' => 'form-control', 'placeholder' => 'Country','id' => 'ms_country_id')) !!}
					</div>
					<div class="form-group" id="city-container">
						{!! Form::select('ms_city_id',$data['city'],null,array('class' => 'form-control','id' => 'ms_city_id', 'placeholder' => 'City')) !!}
					</div>
					<div class="form-group text-center">
						{!! Form::submit('DAFTAR',array('class' => 'btn btn-widget')) !!}
					</div>
					{!! BootstrapForm::close() !!}
				</div>


			

			



				@else
				<h4><i class="fa fa-list-alt"></i> Profil Saya</h4>
				<div class="col-md-12 widget-description text-center">
					<h4>
						{!! Auth::user()->name !!}
					</h4>
					<p> Alamat : {!! Auth::user()->address!=''?Auth::user()->address:'-' !!} </p>
					<p> ID Anggota : {!! Auth::user()->code!=''?Auth::user()->code:'-' !!}</p>
					@if(Auth::user()->ms_bank_id != '')
					<p>
						Rek. Bank : {!! Auth::user()->bank->name !!} - {!! Auth::user()->account_bank !!}
					</p>
					@endif
					<h5>Status Keanggotaan</h5>
					<p class="member-status"> Status : 
						@if(Auth::user()->approved=='1')
						<span class="unverivied">Terverifikasi</span>
						@else
						<span class="unverivied">Belum Terverifikasi</span>
						@endif
					</p>
					<div class="col-md-12 text-center">
						<a href="{!! route('user.dashboard') !!}" class="btn btn-keiskei"> Dashboard</a>
					</div>
				</div>
				@endif
			</div>











			<div class="col-md-4 widget hidden-xs hidden-sm">
				<h4><i class="fa fa-mobile fa-5x"></i> Unduh Aplikasi</h4>
				<div class="col-md-12 widget-description">
					<p>
						Selalu terkoneksi dengan informasi dan berita terbaru dari kami? Silakan download aplikasinya di gadget Anda.
						 </p>
						<img src="{!! asset('theme/keiskei/img/keiskei-mobile-icon.png') !!}" class="img-responsive" alt="keiskei mobile app">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row color-white">
		<div class="col-md-offset-1 col-md-10 widget-payment hidden-xs hidden-sm">
			<div class="row">
				<div class="col-md-5 text-center" style="width:32.6667%">
					<h4 class="hidden-xs hidden-sm">Pembayaran</h4>
					<img src="{!! asset('theme/keiskei/img/pembayaran-keiskei-min.jpg') !!}" class="img-responsive">
				</div>
				<div class="col-md-5 text-center" style="width:32.6667%">
					<h4 class="hidden-xs hidden-sm">Pengiriman</h4>
					<img src="{!! asset('theme/keiskei/img/shipping-icon.jpg') !!}" class="img-responsive">
				</div>
				<div class="col-md-5 text-center" style="width:32.6667%">
				<br>
				<div style="float:left">
				<script language="JavaScript" type="text/javascript">
				TrustLogo("https://www.keiskei.co.id/theme/keiskei/img/comodo-keiskei.gif", "CL1", "none");
				</script></div>
				<img src="{!! asset('theme/keiskei/img/seal-footer-keiskei.jpg') !!}" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
	@endsection

	@section('custom-head')
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.css') !!}
	{!! HTML::style('theme/keiskei/plugin/selectize/css/selectize.bootstrap3.css') !!}
	<style type="text/css">
		div.gambar-produk {
			position: relative;
			float:left;
			margin:5px;}

			div.gambar-produk:hover input
			{
				display: block;
			}

			div.gambar-produk input {
				position:absolute;
				left: 0;
				top: 0;
				display:none;
			}
		</style>
		@endsection

		@section('custom-footer')
		{!! HTML::script('theme/keiskei/plugin/selectize/js/standalone/selectize.js') !!}
		{!! HTML::script('theme/keiskei/plugin/lazyload/jquery.unveil.js') !!}
		<script type="text/javascript">
			$(function () {
				var value = $("#ms_country_id").val();
				$('.img-lazy').unveil();
				cityField(value);
			});
			function cityField( value)
			{
				var general_country_id = {!! $data['general']->ms_country_id !!};
				if(value == general_country_id)
				{
					$('#city-container').show();
				}else{
					$('#city-container').hide();
				}
			}
			$(document).ready(function() {
				$('#ms_country_id').selectize();      
				$('#ms_city_id').selectize();      
				$('#ms_country_id').change(function()
				{
					cityField($(this).val());
				});
				$("#product-slider").owlCarousel({
					items : 5,
					lazyLoad : true,
					navigation : true,
					pagination : false,
					navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
				}); 
				$('#form-validate').validate({
					errorClass : "help-block error",
					rules: {
						username : {
							required: true,
							minlength: 3
						},
						email :{
							required: true,
							email : true
						}
					},
					messages: {
						username : {
							required : "Mohon masukkan username Anda",
							minlength : $.validator.format("Username Anda harus minimal {0} karakter")
						},
						email : {
							required : "Mohon masukkan email Anda",
							email : "Mohon cek kembali alamat email Anda"
						}
					}
				});
			});

		</script>
		@endsection

