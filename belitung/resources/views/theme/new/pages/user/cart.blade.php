@extends('theme.keiskei.app')
<title>Keiskei Indonesia | Keranjang Belanja</title>

@section('content')
@include('theme.keiskei.scripts.price-product')
<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<div class="banner-product text-center">
			<h2>Keiskei Indonesia</h2>
			<h5>Hallo... {!! Auth::user()->name !!}, Anda di Halaman Keranjang Belanja. Silahkan pilih Kurir pengiriman dan cara pembayaran.</h5>
		</div>
	</div>
</div>
<div class="row product-detail-container">
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-12">
				<div class="row">

					<center><div class="se-pre-con">
  <div style="left: 0px; margin: -100px auto auto; position: absolute; width: 100%; top: 40%;">
				<img src="{!! asset('theme/keiskei/img/logo.png') !!}" class="img-responsive">
    Mohon ditunggu...</div></div></center>

					<div class="col-md-12">
						@include('errors.session')
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Produk</th>
									<th>Harga</th>
									<th>Jumlah</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no=1;
								$total = 0;
								?>
								@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
								@if($data['content']->detailOrders->count() > 0)
								@foreach($data['content']->detailOrders as $row)
								<?php
								$total_t = priceDisplay($data['default_role'], $row->product->prices); 
								?>
								<tr>
									<td>
										<div class="pull-left">
											@if($row->product->productImages()->count() > 0)
											<img src="{!! asset($row->product->productImages->first()->image) !!}" width="100px" alt="{!! $row->product->productImages->first()->caption !!}">
											@endif
										</div>
										<div class="pull-left">
											<p>
												<a href="{!! route('user.product.detail',$row->product->code, $row->product->title) !!}">&nbsp;&nbsp;{!! $row->product->title !!}</a>
												<br>
												@if($row->detailAttributeProductDetailOrders()->count() > 0)
												@foreach($row->detailAttributeProductDetailOrders as $val)
												<span>
													{!! $val->detailAttributeProduct->detailAttribute->attribute->name !!}
													: 
													<b>{!! $val->detailAttributeProduct->detailAttribute->name !!}</b>
												</span>
												@endforeach
												@endif
											</p>
											<p>
												<span class="fa fa-check">&nbsp;&nbsp;Stok tersedia</span> &nbsp;&nbsp;
												<a href="{!! route('user.cart.delete',[$row->id]) !!}"><i class="fa fa-trash-o"></i> Hapus Item</a>
											</p>
										</div>
									</td>
									<td>
										<p>
											<span>
												{!! money_format('%(#10n', $total_t) + ($total_t*10/100)!!}

 
											</span>
											<br>

										</p>
									</td>
									<td>

										{!! Form::open(array('route' => ['user.cart.change-total', $row->id], 'method' => 'POST')) !!}
										{!! Form::selectRange('total', 1, 15, $row->total, ['class' => 'form-control','onchange' => 'this.form.submit()','data-detail-id' => $row->id]) !!}
										{!! Form::close() !!}
										<?php 
										$total_t = $total_t*$row->total;
										$total += $total_t;

										?>
									</td>
									<td class="text-right">
										{!! money_format('%(#10n', $total_t) + ($total_t*10/100) !!}

									</td>
									
								</tr>
								@endforeach
								@else
								<tr>
									<td colspan="4">
										Tidak ada data.
									</td>
								</tr>
								@endif
								@else
								<tr>
									<td colspan="4">
										Tidak ada data.
									</td>
								</tr>
								@endif
							</tbody>
						</table>					
					</div>
				</div>
			</div>
			@if(Auth::user()->roles->first()->level!='5')
			<div class="col-md-12">
				<div class="sidebar">
					<div class="sidebar-head">
						Alamat Tujuan
					</div>
					<div class="sidebar-body">
						@if(Auth::user()->roles->first()->level==5 && Auth::user()->roles->first()->locked != '1')
						<p>
							
						</p>
						@else
						<p>
							{!! Auth::user()->address !!}
							<br>
							{!! Auth::user()->city->name !!}
							<br>
							{!! Auth::user()->city->province->name !!}
							<br> Telp : {!! Auth::user()->telephone !!}
						</p>
						@endif
					</div>
				</div>
				@if(Auth::user()->roles->first()->level!='5')
				@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
				<div class="sidebar">
					<div class="sidebar-head">
						Pengiriman
					</div>
					<div class="sidebar-body">

						<div class="form-group">
							{!! Form::label('ms_courier_id','Kurir Pengiriman', array('class' => 'form-label')) !!}
							{!! Form::select('ms_courier_id',$data['courier'],!empty($data['content']->ms_courier_id)?$data['content']->ms_courier_id:null, array('class' => 'form-control', 'id' => 'ms_courier_id','onchange' => 'changeCourier(this)')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('service_courier','Paket Pengiriman', array('class' => 'form-label')) !!}
							<?php 
							$data['service_courier'] = array();
							
							if($data['content']->service_courier != '')
								array_push($data['service_courier'], $data['content']->service_courier);
							else
								array_push($data['service_courier'], 'Tidak ada');
							?>
							{!! Form::select('service_courier',$data['service_courier'], null, array('class' => 'form-control','id' => 'service_courier','onchange' => 'changeService(this)')) !!}
						</div>
						<div class="form-group">
							{!! Form::label('service_courier','Ongkos Pengiriman', array('class' => 'form-label')) !!}
							<br>
							<span class='shipping-price'>
								{!! money_format('%(#10n', $data['content']->shipping_price) !!}
								<p>
									<b>{!! $data['content']->courier->name !!}</b> - {!! $data['content']->service_courier !!}
								</p>
							</span>
						</div>
					</div>
				</div>
				@if(Auth::user()->roles->first()->default != '1' || (Auth::user()->roles->first()->default=='1' && $data['agen']->count() == 0))
				<div class="sidebar">
					<div class="sidebar-head">
						Pilih Pembayaran
					</div>
					<div class="sidebar-body">
						<div class="col-md-12">
							<div class="form-group">
								<?php 
								$arrPayment = array();
								foreach ($data['payment'] as $row) {
									$arrPayment[$row->id] = $row->name.' '.$row->account_number.' - '.$row->account_name;
								}
								?>
								{!! Form::label('ms_payment_id','Pilih Pembayaran', array('class' => 'form-label')) !!}
								{!! Form::select('ms_payment_id',$arrPayment, null, array('class' => 'form-control')) !!}
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="sidebar">
					<div class="sidebar-head">
						Pilih Perwakilan di Daerah
					</div>
					<div class="sidebar-body">
						<div class="col-md-12">
							<table class="table table-hover">
								@foreach($data['agen'] as $row)
								<tr>
									<td>
										<div class="form-group agen-select">
											<div class="radio">
												<input type="radio" name="ms_reseller_id" id="reseller{!! $row->id !!}" value="{{ $row->id }}" class="reseller" onchange="checkReseller(this)" data-order-id='{!! $data['content']->id !!}' @if($data['content']->ms_reseller_id == $row->id) checked='true' @endif>
											</div>
											<label class="control-label" for="reseller{!! $row->id !!}">
												<h4 >{!! $row->name !!}</h4>
												<p>
													{!! $row->address !!}<br>
													{!! $row->city->name !!}, {!! $row->city->province->name !!}
												</p>
											</label>
										</div>
									</td>
									<tr>
										@endforeach
									</table>
								</div>
							</div>
						</div>
						@endif
						@endif
						@endif
					</div>
					@endif
				</div>
				@if(Auth::user()->roles->first()->level!='5')
				@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
				@if(Auth::user()->roles->first()->default != '1' || (Auth::user()->roles->first()->default=='1' && $data['agen']->count() == 0))
				<div class="row">
					<div class="col-md-2">
						<a class="btn btn-keiskei-orange" href="{!! route('product') !!}">
							<i class="fa fa-chevron-left"></i>&nbsp;Lanjutkan Berbelanja 
						</a>
					</div>
					<div class="col-md-8 text-right">
						<div class="col-md-6">
							<b>Ongkos Pengiriman</b><br>
							<span class='shipping-price'>
								{!! money_format('%(#10n', $data['content']->shipping_price) !!}
								<p>
									<b>{!! $data['content']->courier->name !!}</b> - {!! $data['content']->service_courier !!}
								</p>
							</span>
						</div>
						<div class="col-md-6">
							<b>Total Pembayaran</b>
							<p id="total-pembayaran">
								{!! money_format('%(#10n', $total+$data['content']->shipping_price) + ($total_t*10/100)!!}
							</p>
						</div>

					</div>
					<div class="col-md-2 text-right">
						<a class="btn btn-keiskei" onclick="checkoutNormal()" href="javascript:void(0)">
							Checkout &nbsp; <i class="fa fa-chevron-right"></i>
						</a>
					</div>
				</div>
				@else
				<div class="row">
					<div class="col-md-2">
						<a class="btn btn-keiskei-orange" href="{!! route('product') !!}">
							<i class="fa fa-chevron-left"></i>&nbsp;Lanjutkan Berbelanja 
						</a>
					</div>
					<div class="col-md-8 text-right">
						<div class="col-md-6">
							<b>Ongkos Pengiriman</b>
							<span class='shipping-price'>
								{!! money_format('%(#10n', $data['content']->shipping_price) !!}
								<p>
									<b>{!! $data['content']->courier->name !!}</b> - {!! $data['content']->service_courier !!}
								</p>
							</span>
						</div>
						<div class="col-md-6">
							<b>Total Pembayaran</b>
							<p  id="total-pembayaran">
								{!! money_format('%(#10n', $total+$data['content']->shipping_price) !!}
							</p>
						</div>

					</div>
					<div class="col-md-2 text-right">
						<a class="btn btn-keiskei" href="javascript:void(0)" onclick="checkReseller2()">
							Checkout &nbsp; <i class="fa fa-chevron-right"></i>
						</a>
					</div>
				</div>
				@endif
				@endif
				@else
				@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
				<div class="row">
					<div class="col-md-2">
						<a class="btn btn-keiskei-orange" href="{!! route('product') !!}">
							<i class="fa fa-chevron-left"></i>&nbsp;Lanjutkan Berbelanja 
						</a>
					</div>
					<div class="col-md-8 text-right">
						<div class="col-md-12">
							<b>Total Pembayaran</b>
							<p  id="total-pembayaran">
								{!! money_format('%(#10n', $total) !!}
							</p>
						</div>

					</div>
					<div class="col-md-2 text-right">
						<a class="btn btn-keiskei" href="{!! route('user.cart.checkout.overseas') !!}">
							Checkout &nbsp; <i class="fa fa-chevron-right"></i>
						</a>
					</div>
				</div>
				@endif
				@endif
			</div>
		</div>
		@include('theme.keiskei.partials.best-products')
		@endsection

		@section('custom-footer')

		<script type="text/javascript">
			$(document).ready(function() {
				$("#product-slider").owlCarousel({
					items : 5,
					lazyLoad : true,
					navigation : true,
					pagination : false,
					navigationText : ["<i class='fa fa-angle-left fa-3x'></i>","<i class='fa fa-angle-right fa-3x'></i>"],
				});
			});

			function changeCourier(x)
			{
				var courier_id = $(x).val();
				$('#service_courier').empty();
				$.ajax(
				{
					url:"{{ route('user.shipping.courier') }}",
					type: "GET",
					data:{
						courier_id : courier_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						shippingPrice = [];
						var html = '<option selected disabled> Pilih Layanan </option>';
						$.each(data, function(key, value){
							html += "<option value='" + value[0] + "'>" + value[0] + "</option>";
							shippingPrice[value[0]] = value[1];
						});
						$('#service_courier').append(html);
					}
				});
			}

			@if(!empty($data['content']) && $data['content']->detailOrders->count() > 0)
			var shippingPrice = new Array();
			var total = {!! $total+$data['content']->shipping_price !!};
			var shippingPriceOld = {!! $data['content']->shipping_price !!};
			function checkReseller(x){
				var ms_reseller_id = $(x).val();
				var order_id = x.getAttribute("data-order-id");
				$.ajax(
				{
					url:"{{ route('user.cart.reseller') }}",
					type: "GET",
					data:{
						ms_reseller_id : ms_reseller_id,
						order_id : order_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						$('.reseller').prop("checked", false); 
						x.checked = true;
					}
				});
			}
			function checkReseller2(){
				var x =  $("input[name='ms_reseller_id']:checked").val();
				console.log(x);
				if(typeof x === "undefined")
				{
					alert('Pilih Perwakilan terlebih dahulu');
				}else{
					// alert(' dahulu');
					window.location.replace("{!! route('user.cart.checkout.agent') !!}");
				}
			}
			function changeService(x)
			{
				var service = $(x).val();
				var price = shippingPrice[service];
				var courier_id = $('#ms_courier_id').val();
				console.log(service);
				console.log(price);
				$.ajax(
				{
					url:"{{ route('user.shipping.service') }}",
					type: "GET",
					data:{
						service : service,
						price : price,
						courier_id : courier_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						var total_t = total-shippingPriceOld;
						var shippingPriceOld_t = price;
						total_t += price;
						var html = shippingPriceOld_t + "<p><b>" + data.courier.name + "</b> - " + data.service_courier + "</p>";
						$('#total-pembayaran').empty();
						$('#total-pembayaran').append(total_t)
						$('.shipping-price').empty();
						$('.shipping-price').append(html)
					}
				});	
			}
			function checkoutNormal()
			{
				var order_id = {!! $data['content']->id !!};
				var payment_id = $('#ms_payment_id').val();
				$.ajax(
				{
					url:"{{ route('user.checkout.api.normal') }}",
					type: "GET",
					data:{
						order_id : order_id,
						payment_id : payment_id
					}
				}).done(function(data,status){
					if(status=='success')
					{	
						window.location.replace("{!! route('user.checkout.normal') !!}");
					}else{
						alert('error');
						return false;
					}
				});	
			}
			@endif
		</script>
		<script type="text/javascript">

		//paste this code under head tag or in a seperate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});

	</script>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
	position: fixed;
	left: 0px;
	top: 0px;
	width: 100%;
	height: 100%;
	z-index: 9999;
	background: url(../img/preloader.gif) center no-repeat ;
	background-color: rgba(255, 255, 255, 0.7);
}
</style>

<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

		@endsection

		@section('custom-head')

		@endsection