<!DOCTYPE html>
<html>
 


	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>
		{!! $data['content']->code !!} | KeisKei Indonesia | INVOICE
	</title>

	{!! HTML::style('theme/keiskei/css/bootstrap-grid.min.css') !!}
{!! HTML::style('theme/keiskei/plugin/fontawesome/css/font-awesome.min.css') !!}

	{!! HTML::script('theme/keiskei/js/jquery-1.11.2.min.js') !!}
	<style type="text/css" media="print">
 		  @media print {
.body{color:green;background: #ccc}

    }
		


 		.bg{


			background-color: #FAF9F9;
			border-color: solid 2px #444;
		}
		b{
			color: green;
		}
	</style>

	<style type="text/css">

	.invoice{
			background-color:#444; color:#fff; border-right:1px solid #FAF9F9}
</style>
</head>
<body>
	@include('theme.keiskei.scripts.price-product')
	@if(Auth::user()->roles->first()->locked == '1')
	<?php 
	if($data['content']->user->roles->first()->level == '5')
		setlocale(LC_ALL, "en_US.UTF-8");
	?>
	@endif
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 bg">
			<div class="row">
				<div class="col-md-6">
					<h3 color="green"><b>KeisKei</b>Indonesia</h3>
				</div>
				<div class="col-md-6 pull-right text-right" style="padding-right:20px; padding-top:10px;">
					<h5>
						<!--<a href="{!! route('user.invoice',[$data['content']->id ]) !!}?invoice={!! $data['content']->code !!}&print=1" target="_blank" style="text-decoration:none;"> -->
							
							<a href="#"  onClick="window.print()"><b> <i class="fa fa-print"></i> Cetak</b>
						</a>
					</h5>

				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
				<div class="col-md-4 text-left">
					<b>
						Nomor Invoice :
					</b>
					<p>
						{!! $data['content']->code !!}
					</p>
					<br>
					<b>
						Tanggal Invoice :
					</b>
					<p>
						{!! date('j F Y', strtotime($data['content']->created_at)) !!}
					</p>
				</div>

				<div class="col-md-8 text-right">
					
					@if($data['content']->ms_status_order_id == '8')
					<br>
					<h4>
						<b>{!! $data['content']->statusOrder->name !!} : </b>
					</h4>

					@if($data['content']->reseller()->count() > 0)
					<div class="row">
						<div class="col-md-12">
						<b>{!! $data['content']->reseller->name !!}</b>
						<br>
						@if($data['content']->reseller->accountBank()->count())
						<h5 class="text-right">
							<b>
								Akun Rekening :
							</b>
						</h5>
						<ul>
							@foreach($data['content']->reseller->accountBank as $row)
							<li>
								{!! $row->bank->name !!} - {!! $row->account_name !!}
							</li>
							@endforeach
						</ul>
						<br>
						@endif
						{!! $data['content']->reseller->address !!}
						<br>
						{!! $data['content']->reseller->city->name !!}, {!! $data['content']->reseller->city->province->name !!}
						<br>
						Telp : {!! $data['content']->reseller->telephone !!}
						</div>
					</div>
						
					@endif
					@else
					<h4>
						<b>{!! $data['content']->statusOrder->name !!}</b>
					</h4>
					@endif
				</div>	
				</div>
				
			</div>
			@if(!empty($data['content']->ms_payment_id))
			<div class="row">
				<div class="col-md-12">
					<b>
						Pembayaran ke :
					</b>
					<p>
						{!! $data['content']->payment->name !!} {!! $data['content']->payment->account_number !!} - a.n. {!! $data['content']->payment->account_name !!}
					</p>
				</div>
				<div class="col-md-12">
					@if($data['content']->transfers->count() > 0)
					@foreach($data['content']->transfers as $val)
					<em style="color:red">Konfirmasi pembayaran telah dilakukan: {!! money_format('%(#10n', $val->nominal) !!} - {!! date('j F Y',strtotime($val->transfer_date)) !!}</em>
					@if($val->status == '1')
					<b> (Disetujui) </b>
					@elseif($val->status == '2')
					<b> (Tidak Disetujui) </b>
					@else
					<b> (Pending) </b>
					@endif
					<br>
					@endforeach
					@endif
				</div>
			</div>
			@endif
			<hr>
			<div class="row">
				<div class="col-md-12">
					<h4>
						Halo, {!! $data['content']->user->username !!}&nbsp;  | &nbsp; HP: {!! $data['content']->user->mobile !!}

					</h4>
					<p>Terima kasih, Anda telah melakukan pemesanan sebagai berikut:</p>
				</div>
				<div class="col-md-12">
					<table class="table" media="print">
						<thead>
							<tr>
								<th class="invoice">Nama Produk</th>
								<th style="background-color:#306d1f; color:#fff; border-right:1px solid #FAF9F9;">Jumlah</th>
								<th style="background-color:#306d1f; color:#fff; border-right:1px solid #FAF9F9;">Berat</th>
								<th style="background-color:#306d1f; color:#fff; border-right:1px solid #FAF9F9;">Harga</th>
								<th style="background-color:#306d1f; color:#fff;">Subtotal</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$total = 0;
							$weight = 0;
							?>
							@foreach($data['content']->detailOrders as $row)
							<tr>
								<td>
									<h5>{!! $row->product->title !!}</h5>
										@if($row->detailAttributeProductDetailOrders->count() > 0)
										@foreach($row->detailAttributeProductDetailOrders as $val)
										<span>

											: 
											<b>{!! $val->detailAttributeProduct->detailAttribute->name !!}</b>
										</span>
										@endforeach
										@endif
									</td>
									<td>{!! $row->total !!}</td>
									<td>{!! $row->product->weight !!} kg</td>
									<td>
										<?php 
										$weight += $row->total*$row->product->weight;
										if(Auth::user()->roles->first()->locked != '1')
											$total_t = priceDisplay($data['default_role'], $row->product->prices); 
										else
										{

											$total_t = priceDisplayAdmin($data['content']->user, $data['default_role'], $row->product->prices); 
										}

										?>
										<span>
											{!! money_format('%(#10n', $total_t) !!}
										</span>
										<br>
										<span class="small">
											<del><?php $def = 0; if(Auth::user()->roles->first()->locked != '1') 
												$def= defaultProduct($data['default_role'], $row->product->prices);
												else
													$def = defaultProductAdmin($data['content']->user, $data['default_role'], $row->product->prices);
												?>
												{!! money_format('%(#10n', $def) !!}
											</del>
										</span>
										<?php 
										if(!empty($row->product->discount))
										{
											$discount_t = ($total_t/100)*intval($row->product->discount);
											echo "<br><b>Diskon ".$row->product->discount." %</b>";
											$total_t = $total_t - $discount_t;
										}
										$total_t = $total_t*$row->total;
										$total += $total_t;
										?>
									</td>
									<td class="text-right">
										{!! money_format('%(#10n', $total_t) !!}
									</td>
								</tr>
									<tr  style="font-size:12px;width:100%;text-align:right;background:#f4f4f4;color:green">
									
										
										<td>&nbsp;</td>
         				    	       <td>&nbsp;</td>
           					         <td>&nbsp;</td>
           					         <td>&nbsp;</td>
									<td colspan="4" style>
										<h6>PPN (10%) :	{!! money_format('%(#10n', $total_t*10/100) !!}
										</h6>
									</td>
								</tr>
								<tr>
								@endforeach
								@if($data['content']->user->roles->first()->level!='5')
								<tr>
									<td colspan="2">
										<h5>
											{!! $data['content']->courier->name !!} -
											{!! $data['content']->service_courier !!}
										</h5>
									</td>
									<td>
										{!! $weight !!} kg
									</td>
									<td colspan="2" class="text-right">
										{!! money_format('%(#10n', $data['content']->shipping_price) !!}
									</td>
								</tr>
								@endif
								
								<tr>
									<td colspan="4" style=" background:#f4f4f4;color:green;border-bottom:2px solid green">
										<h4>Total Pembayaran</h4>
									</td>
									<td class="text-right" style=" background:#f4f4f4;color:green;border-bottom:2px solid green">
										<h4>
											{!! money_format('%(#10n', $total+intval($data['content']->shipping_price)+ (($total))*10/100)  !!}
										</h4>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-12">
						<div class="col-md-10 col-md-offset-2" style="background-color:#fff;margin-left:auto; margin-right:auto; margin-bottom:-10px;">
							<h5>
								<strong>Alamat Tujuan:</strong>
							</h5>
							<p>
								<b>{!! $data['content']->user->name !!}</b>
								<br>
								{!! $data['content']->user->address !!}<br>
								@if($data['content']->user->roles->first()->level!='5')
								{!! $data['content']->user->city->name !!}, {!! $data['content']->user->city->province->name !!}<br>
								@endif
								Telp. {!! $data['content']->user->telephone !!}
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</body>
	</html>