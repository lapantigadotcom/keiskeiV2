<div class="row color-white">
	<div class="col-md-offset-1 col-md-10">
		<div class="col-md-12">
		@if(isset($hot_product))
			<div class="row">
				<div class="col-md-12 main-our-product">
					<h3 class="text-left"> <i class="fa fa-money"></i> Produk Terpopuler</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
				    <div id="product-slider" class="owl-carousel">
				    @foreach($hot_product as $row)
				      <div class="gambar-produk item">
				      		@if($row->productImages()->count() > 0)
							@if(File::exists($row->productImages->first()->image))
				      		<img class="lazyOwl img-responsive" data-src="{!! asset($row->productImages->first()->image) !!}"  title="{!! $row->title !!} | {!! ucwords($row->categoryproduct->name) !!}" ><br><br><br>
				      		<input style="border-radius:0;width:100%;height:100%;background: rgba(0, 88, 34, 0.3);" class="btn btn-widget" type="button" value="{!! $row->title !!}" onclick="location.href='{!! route('user.product.detail',$row->code, $row->title) !!}';"/>
				      		@endif
							@endif
				      		<h4>{!! $row->title !!}</h4>
							<h3>{!! ucwords($row->categoryproduct->name) !!}</h3>
				      </div>
				    @endforeach
				    </div>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>


<div class="row color-white">
		<div class="col-md-offset-1 col-md-10 widget-payment">
			<div class="row">
				<div class="col-md-5 text-center hidden-xs hidden-sm" style="width:32.6667%">
					<h4 class="hidden-xs hidden-sm">Pembayaran</h4>
					<img src="{!! asset('theme/keiskei/img/pembayaran-keiskei-min.jpg') !!}" class="img-responsive">
				</div>
				<div class="col-md-5 text-center hidden-xs hidden-sm" style="width:32.6667%">
					<h4 class="hidden-xs hidden-sm">Pengiriman</h4>
					<img src="{!! asset('theme/keiskei/img/shipping-icon.jpg') !!}" class="img-responsive">
				</div>
				<div class="col-md-5 text-center" style="width:32.6667%">
				<br>
				<div style="float:left">
				<script language="JavaScript" type="text/javascript">
				TrustLogo("https://www.keiskei.co.id/theme/keiskei/img/comodo-keiskei.gif", "CL1", "none");
				</script></div>		
				<img src="{!! asset('theme/keiskei/img/seal-footer-keiskei.jpg') !!}" class="img-responsive">
				</div>


			</div>
		</div>			
</div>	

		<!-- SSL COMODO KEISKEI -->
		<script type="text/javascript">
		//<![CDATA[
		var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
		document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
		//]]>
		</script>
