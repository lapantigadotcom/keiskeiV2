	<div class="row color-grey">
		<div class="col-md-offset-1 col-md-10 bottom-menu hidden-xs hidden-sm">
			<ul id="footer-menu">
				@if(isset($articles[4]['id']))<li><a href="{!! route('articles',[$articles[4]['id'].'-'.$articles[4]['title']]) !!}">Kebijakan Privasi</a></li>@endif
				@if(isset($articles[5]['id']))<li><a href="{!! route('articles',[$articles[5]['id'].'-'.$articles[5]['title']]) !!}">FAQ</a></li>@endif
				@if(isset($articles[6]['id']))<li><a href="{!! route('articles',[$articles[6]['id'].'-'.$articles[6]['title']]) !!}">Keanggotaan</a></li>@endif
				@if(isset($articles[7]['id']))<li><a href="{!! route('articles',[$articles[7]['id'].'-'.$articles[7]['title']]) !!}">Peta Situs</a></li>@endif
				@if(isset($articles[3]['id']))<li><a href="{!! route('articles',[$articles[3]['id'].'-'.$articles[3]['title']]) !!}">Dukungan</a></li>@endif
				@if(isset($articles[8]['id']))<li><a href="{!! route('articles',[$articles[8]['id'].'-'.$articles[8]['title']]) !!}">Karir</a></li>@endif
				<li style="font-size: 19px; float: right;border:none"><i class="fa fa-comments" style="float:right;"></i></li>
				<li style="float: right; font-weight: normal; max-width: 470px;"><marquee scrolldelay="90">{!! $general->news_roll !!}</marquee></li>
			</ul>
		</div>
	</div>
	<div class="row color-keiskei-blur">
		<div class="col-md-offset-1 col-md-10 footer">
			<div class="row">
				<div class="col-md-6 text-left">
					<p>Copyright &copy; PT. Punyakunik Maju Jaya. All Rights Reserved. 



					</p>
				</div>
				<div class="col-md-6 text-right hidden-xs hidden-sm">
					<p>Head Office: {!! $general->address !!}</p>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function() {
			NProgress.start();
			NProgress.done();
			$('#e1').click(function() {
				NProgress.configure({ showSpinner: true });
				NProgress.set(0.2);
				NProgress.set(0.4);
				NProgress.set(0.6); 
				NProgress.set(0.8); 
				NProgress.set(1.0);				
			});
			$('#e2').click(function() {
				NProgress.configure({ showSpinner: true });
				NProgress.start();
				$('#nprogress .bar').css({'background': '#c0392b'});
				$('#nprogress .peg').css({'box-shadow': '0 0 10px #c0392b, 0 0 5px #c0392b'});
				$('#nprogress .spinner-icon').css({'border-top-color': '#c0392b', 'border-left-color': '#c0392b'});
				NProgress.done();
			});
			$('#e3').click(function() {
				NProgress.configure({ showSpinner: false });
				NProgress.start();
				$('#nprogress .bar').css({'background': '#16a085'});
				$('#nprogress .peg').css({'box-shadow': '0 0 10px #16a085, 0 0 5px #16a085'});
				$('#nprogress .spinner-icon').css({'border-top-color': '#16a085', 'border-left-color': '#16a085'});
				NProgress.done();
			});
			$('#login-trigger').click(function(){
				$(this).next('#login-content').slideToggle();
				$(this).toggleClass('active');          
				
				if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
				else $(this).find('span').html('&#x25BC;')
			});
		});
</script>
  {!! HTML::script('theme/pasargrosirsurabaya/js/vendor/jquery-1.11.3.min.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery.nivo.slider.pack.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery.meanmenu.min.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/wow.min.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery-price-slider.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery.simpleGallery.min.js) !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery.simpleLens.min.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery.scrollUp.min.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/jquery.collapse.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/plugins.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/js/main.js') !!}

{!! HTML::script('theme/pasargrosirsurabaya/js/bootstrap.min.js') !!}
{!! HTML::script('theme/pasargrosirsurabaya/plugin/owl/owl.carousel.min.js') !!}

<script type="text/javascript">
	$(document).ready(function () {

	if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
        $('#banner-keiskei').addClass( "banner-keiskei-front" );
    }
    if ($(window).width() <= 767) {
        $('#banner-keiskei').removeClass( "banner-keiskei-front" );
    }
    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));
  
    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));  

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '80%';
    var menuneg = '-100%';
    var slideneg = '-80%';


    $("#slide-nav").on("click", toggler, function (e) {
    	
    	$('#second-chart').toggle();
    	$('#second-login').toggle();
    	$('#second-register').toggle();
    	$('.footer-menu-in-head').toggle();
    	$('#footer-menu').toggle();
    	$('#chart').toggle();
        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });


        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');


        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');


    });


    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


    $(window).on("resize", function () {

        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
            $('#banner-keiskei').addClass( "banner-keiskei-front" );
        }
        if ($(window).width() <= 767) {
            $(selected).removeClass('slide-active');
            $('#banner-keiskei').removeClass( "banner-keiskei-front" );
        }

    });




});
</script>


<!-- 
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?39wiev9YKiInSGR18wzkvf6ppUtCOnuK";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
 End of Zopim Live Chat Script-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64053549-1', 'auto');
  ga('send', 'pageview');

</script>
@yield('custom-footer')