{!! HTML::style('theme/pasargrosirsurabaya/css/bootstrap.min.css') !!}

{!! HTML::script('theme/pasargrosirsurabaya/js/jquery-1.11.2.min.js') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/style.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/font-awesome.min.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/meanmenu.min.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/owl.carousel.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/owl.theme.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/owl.transitions.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/nivo-slider.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/jquery-ui-slider.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/jquery.simpleLens.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/jquery.simpleGallery.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/animate.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/normalize.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/main.css') !!}
{!! HTML::style('theme/pasargrosirsurabaya/css/responsive.css') !!}
 {!! HTML::script('theme/pasargrosirsurabaya/js/vendor/modernizr-2.8.3.min.js') !!}

@yield('custom-head')
