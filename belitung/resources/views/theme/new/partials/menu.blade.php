<div class="header-area">
			<!-- Header top bar -->
			<div class="header-top-bar">
				<div class="container">
					<div class="header-top-inner">
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<div class="header-top-left">
									<div class="phone">
										<label>Call us:</label> (+88) 01737803547
									</div>
									<div class="e-mail">
										<label>Email:</label> admin@bootexperts.com
									</div>
									<!-- Header Link Area -->
									<div class="header-link-area">
										<div class="header-link">
											<p class="hidden-xs">Language: </p>
											<ul>
												<li><a href="#">English <span class="caret"></span></a>
													<ul>
														<li><a href="#">English 1</a></li>
														<li><a href="#">English 2</a></li>
														<li><a href="#">English 3</a></li>
														<li><a href="#">English 4</a></li>
														<li><a href="#">English 5</a></li>
													</ul>
												</li>
											</ul>
										</div>
										<div class="header-link">
											<p class="hidden-md hidden-sm hidden-xs">Currency: </p>
											<ul>
												<li class="hidden-md hidden-sm hidden-xs"><a href="#">USD <span class="caret"></span></a>
													<ul>
														<li><a href="#">EUR</a></li>
														<li><a href="#">USD</a></li>
													</ul>
												</li>
												<li><a href="#">Account <span class="caret"></span></a>
													<ul>
														<li><a href="my-account.html">My Account</a></li>
														<li><a href="wishlist.html">My Wishlist</a></li>
														<li><a href="cart.html">My Cart</a></li>
														<li><a href="checkout.html">Checkout</a></li>
														<li><a href="blog.html">Blog</a></li>
														<li><a href="my-account.html">login</a></li>
													</ul>
												</li>
											</ul>
										</div>
									</div><!-- End Header Link Area -->
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="header-top-right">
									<!-- Header Social Icon -->
									<div class="header-social-icon">
										<ul>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- End Header Top bar -->
			<!-- Header bottom -->
			<div class="header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<!-- Header Search -->
							<div class="header-search">
								<form action="#">
									<input type="text" placeholder="SEARCH...">
									<button type="button" class="btn"><i class="fa fa-search"></i></button>
								</form>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<!-- Header logo -->
							<div class="header-logo">
								<a href="index.html"><img src="img/logo/logo.png" alt="logo"></a>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<!-- Header Cart Area-->
							<div class="header-cart-area">
								<div class="header-cart">
									<ul>
										<li>
											<a href="#">
												<i class="fa fa-shopping-cart"></i>
												<span class="my-cart">My cart</span>
												<span class="badge">2</span>
											</a>
											<ul>
												<li>
													<div class="cart-list">
														<div class="cart-list-item">
															<div class="cart-list-img">
																<a href="#"><img src="img/cart/c1.jpg" alt="cart" /></a>
															</div>
															<div class="cart-content">
																<a href="#">Etiam gravida</a>
																<p>1 x <span>$432.00</span></p>
															</div>
															<div class="cart-button">
																<a href="#"><i class="fa fa-pencil"></i></a>
																<a href="#"><i class="fa fa-times"></i></a>
															</div>
														</div>
													</div>
													<div class="cart-list cart-list-two">
														<div class="cart-list-item">
															<div class="cart-list-img">
																<a href="#"><img src="img/cart/c2.jpg" alt="cart" /></a>
															</div>
															<div class="cart-content">
																<a href="#">Etiam gravida</a>
																<p>1 x <span>$432.00</span></p>
															</div>
															<div class="cart-button">
																<a href="#"><i class="fa fa-pencil"></i></a>
																<a href="#"><i class="fa fa-times"></i></a>
															</div>
														</div>
													</div>
													<div class="cart-subtotal">
														<p>Subtotal: <span>$1,131.00</span></p>
													</div>
													<div class="cart-action">
														<button type="button" class="btn"><span>checkout</span> <i class="fa fa-long-arrow-right"></i></button>
													</div>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div><!-- End Header Cart Area-->
						</div>
					</div>
				</div>
			</div><!-- End Header bottom -->
		</div><!-- End Header Area -->