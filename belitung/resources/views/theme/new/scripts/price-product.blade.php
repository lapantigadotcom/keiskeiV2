<?php
  if(Auth::check())
  {
    if(Auth::user()->roles->first()->level == '5'){
//    setlocale(LC_ALL, "en_US.UTF-8");
	setlocale (LC_MONETARY, 'id_ID');
    }else{
      setlocale (LC_MONETARY, 'id_ID');
    }
  }else{
    setlocale (LC_MONETARY, 'id_ID');
  }
  
  
  function priceDisplay($data, $prices)
  {
    
    if(Auth::check())
    {
      if(Auth::user()->roles->first()->level != '5' ){
        $default= $prices->filter(function($v) use($data) {
        if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
          return true;
        }
      });
       if(Auth::user() && Auth::user()->roles->first()->default!='1' && Auth::user()->roles->first()->locked!='1')
       {
        $price_t= $prices->filter(function($v) use($data) {
          if($v->ms_role_id==Auth::user()->roles->first()->id && $v->ms_currency_id=='1'){
            return true;
          }
        });
        return $price_t->first()->price ;
      }
      else
        return $default->first()->price;
    }else{
      
        $default= $prices->filter(function($v) use($data) {
        if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
          return true;
        }
      });
        return $default->first()->price  ;
      
      
    }
    }else{
        $default= $prices->filter(function($v) use($data) {
        if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
          return true;
        }
      });
        return $default->first()->price;


      }
     
     
  }

  function defaultProduct($data, $prices)
  {
    if(Auth::user() && Auth::user()->roles->first()->default!='1')
    {
      $default= $prices->filter(function($v) use($data) {
        if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
          return true;
        }
      });
    
      return $default->first()->price;
    }
    return null;
  }

  function priceDisplayAdmin($user, $data, $prices)
  {
    if($user->roles->first()->level != '5'){
     $default= $prices->filter(function($v) use($data) {
      if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
        return true;
      }
    });
     if($user->roles->first()->default!='1')
     {
      $price_t= $prices->filter(function($v) use($user) {
        if($v->ms_role_id==$user->roles->first()->id && $v->ms_currency_id=='1'){
          return true;
        }
      });
      return $price_t->first()->price;
    }
    else
      return $default->first()->price;
    }else{
      $default= $prices->filter(function($v) use($data) {
      if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
        return true;
      }
    });
      return $default->first()->price;
    }
  }

  function defaultProductAdmin($user, $data, $prices)
  {
    if($user->roles->first()->level != '5'){
    if($user->roles->first()->default!='1')
    {
      $default= $prices->filter(function($v) use($data) {
        if($v->ms_role_id==$data->id && $v->ms_currency_id=='1'){
          return true;
        }
      });
    
      return $default->first()->price;
    }
    return null;
    }
  }
?>
